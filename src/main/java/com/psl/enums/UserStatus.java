package com.psl.enums;

public enum UserStatus {
	ACTIVE, SUSPENDED, BLACKLISTED
}
