package com.psl.enums;

public enum WalletTransactionGateway {
	INTERNAL, EXTERNAL
}
