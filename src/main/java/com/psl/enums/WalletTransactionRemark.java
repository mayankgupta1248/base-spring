package com.psl.enums;

public enum WalletTransactionRemark {
	MATCH_JOIN("Match Join"), ORDER_PLACED("Order Placed");
	private String remark;
	
	WalletTransactionRemark(String remark) {
		this.remark = remark;
	}

	public String getRemark() {
		return remark;
	}
}
