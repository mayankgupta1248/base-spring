package com.psl.enums;

public enum UserMatchWalletStatus {
	PENDING, ACTIVE, CLOSED
}
