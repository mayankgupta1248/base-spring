package com.psl.enums;

public enum WalletTransactionStatus {
    PENDING, SUCCESS, FAILED, REFUNDED;
}
