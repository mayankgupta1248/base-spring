package com.psl.enums;

public enum LoginRemark {

	WRONG_PASSWORD("Wrong Password"), WRONG_OTP("Wrong OTP"), LOGIN_SUCCESSFUL("Login Successful");

	private String remark;

	LoginRemark(String remark) {
		this.remark = remark;
	}

	public String getRemark() {
		return remark;
	}
}
