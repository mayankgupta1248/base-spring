package com.psl.enums;

public enum ErrorCodes {

	OTP101("OTP101","Country code is required"),
	OTP102("OTP102","No such user for given credentials"),
	OTP103("OTP103","This user is being suspended"),
	OTP104("OTP104","OTP Expired"),
	OTP105("OTP105","Invalid OTP"),	
	
	//Login codes
	LOGIN101("LOGIN101", "No such user exists."),
	LOGIN102("LOGIN102", "Account suspended."),
	LOGIN103("LOGIN103", "Invalid username or password."),
	LOGIN104("LOGIN104", "Account blacklisted");
	
	private final String code;
	private final String description;

	private ErrorCodes(String code, String description) {
		this.code = code;
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}
}
