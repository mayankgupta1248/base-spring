package com.psl.enums;

public enum WalletTransactionSource {
	REWARD, ORDER_PLACE, ORDER_CLOSE, MATCH_JOIN
}
