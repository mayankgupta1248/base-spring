package com.psl.enums;

public enum UserNotification {

	/*
	 * 0: Order Type (Buy, Sell) 1: Crypto Asset amount 2: Crypto Asset code 3: Fiat
	 * amount 4: Fiat code
	 */
	TRADE_SUCCESSFUL("Trade Successful", "Your {0} order of {1} {2} @ {3} {4} has been matched."),

	/*
	 * 0: Crypto Asset Name (Ethereum, bitcoin) 1: Crypto Asset amount 5, 2: Crypto
	 * Asset code 3: Timestamp 4: Crypto Asset balance
	 */
	DEPOSIT_SUCCESSFUL("Deposit Successful",
			"Your {0} wallet has been credited with {1} {2} on {3}. Available balance is {4} {5}"), DEPOSIT_FAILED(
					"Deposit Failed",
					"Your deposit request of {0} {1} has failed. Reason for failure - {2}"), CRYPTO_DEPOSIT_FAILED(
							"Deposit Failed", "Your deposit request of {0} {1} has failed."),

	/*
	 * 0: Crypto Asset Name (Ethereum, bitcoin) 1: Crypto Asset amount 5,2: Crypto
	 * Asset code 3: Timestamp 4: Crypto Asset balance
	 */
	WITHDRAWAL_SUCCESSFUL("Withdrawal Successful",
			"Your {0} wallet has been debited with {1} {2} on {3}. Available balance is {4} {5}"),

	/*
	 * 0: Crypto Asset amount 1: Crypto Asset code 2: Failure Reason
	 */
	WITHDRAWAL_FAILED("Withdrawal Failed",
			"Your withdrawal request of {0} {1} has failed. Reason for failure - {2}"), CRYPTO_WITHDRAWAL_FAILED(
					"Withdrawal Failed", "Your withdrawal request of {0} {1} has failed."),

	KYC_SUBMITTED("KYC Submitted",
			"Your KYC application has been submitted successfully. We will update you shortly on the application status."), KYC_APPROVED(
					"KYC Approved",
					"Congratulations! Your KYC details have been approved. Start placing your first order by depositing assets in your PSL wallet."),
	/*
	 * 0: Rejection reason
	 */
	KYC_REJECTED("KYC Rejected",
			"Your KYC application has been rejected. Please verify the submitted information and reinitiate the KYC application. Reason for rejection - {0}"),

	// Signup Reward
	// Type - 0. Amount, 1.Asset
	//Message - 0.Amount, 1.Asset, 2. Asset
	SIGNUP_REWARD("Congrats, we have credited {0} {1} tokens in your PSL account!",
			"Congrats, you have been rewarded with {0} {1} tokens for successful sign-up. Check it out in your PSL account. In order to redeem these {1} tokens, complete your KYC verification within next 15 days."),

	//Referee kyc reward
	//Type - 0.Amount, 1.Asset
	//Message - 0.Amount, 1.Asset
	REFEREE_KYC_REWARD("Congrats, we have credited {0} {1} tokens in your PSL account!",
			"Congrats, you have been rewarded with {0} {1} tokens on successfully completing the KYC verification. Start trading now and earn INR rewards on completion of 10000 INR trade amount."),
	
	//Referee trade reward
	//Type - 0.Amount, 1.Asset
	//Message - 0.Amount, 1.Asset
	REFEREE_TRADE_REWARD("Congrats, we have credited {0} {1} in your PSL account!","As promised, you have been rewarded with {0} {1} for completing a trade of 10000 INR on our platform. Check it out in your PSL account."),

	//Referrer kyc reward
	//Type - 0.Amount, 1.Asset
	//Message - 0.Amount, 1.Asset
	REFERRER_KYC_REWARD("Congrats, we have credited {0} {1} in your PSL account!","Congrats, we have credited {0} {1} tokens in your PSL account on successful KYC completion by one of your referred users. Keep referring and earning."),
	
	//Referrer trade reward
	//Type - 0.Amount, 1.Asset
	//Message - 0.Amount, 1.Asset
	REFERRER_TRADE_REWARD("Congrats, we have credited {0} {1} in your PSL account!","Congrats, we have credited {0} {1} in your PSL account on successful completion of 10000 INR trade by one of your referred users. Keep referring and earning.");
	
	private final String type;
	private final String message;

	private UserNotification(String type, String message) {
		this.type = type;
		this.message = message;
	}

	public String getType() {
		return type;
	}

	public String getMessage() {
		return message;
	}
}
