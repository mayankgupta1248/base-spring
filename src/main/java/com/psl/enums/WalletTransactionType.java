package com.psl.enums;

public enum WalletTransactionType {
    DEPOSIT, WITHDRAWAL
}
