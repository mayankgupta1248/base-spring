package com.psl.order.repository;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.psl.helper.AbstractBaseRepository;
import com.psl.order.domain.UserOrder;

@Repository
public class OrderRespository extends AbstractBaseRepository{

	public UserOrder getUserOpenOrderForPlayer(Long matchId, Long playerId, Long userId) {
		Criteria criteria = this.getCurrentSession().createCriteria(UserOrder.class);
		criteria.add(Restrictions.eq("matchId", matchId));
		criteria.add(Restrictions.eq("playerId", playerId));
		criteria.add(Restrictions.eq("userId", userId));
		criteria.add(Restrictions.eq("activeStatus", true));

		return this.getSingleResultOrNull(criteria, UserOrder.class);
	}

	public UserOrder getOrderbyOrderId(String orderId) {
		Criteria criteria = this.getCurrentSession().createCriteria(UserOrder.class);
		criteria.add(Restrictions.eq("orderId", orderId));

		return this.getSingleResultOrNull(criteria, UserOrder.class);
	}

}
