package com.psl.order.repository;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.psl.helper.AbstractBaseRepository;
import com.psl.order.domain.UserMatchTransaction;
import com.psl.order.domain.UserMatchWallet;

@Repository
public class UserMatchWalletRepository extends AbstractBaseRepository{

	public UserMatchTransaction getUserMatchWalletTransaction(Long userMatchWalletId, String referenceId, String type) {
		Criteria criteria = this.getCurrentSession().createCriteria(UserMatchTransaction.class);
		criteria.add(Restrictions.eq("userMatchWalletId", userMatchWalletId));
		criteria.add(Restrictions.eq("referenceId", referenceId));
		criteria.add(Restrictions.eq("transactionType", type));

		return this.getSingleResultOrNull(criteria, UserMatchTransaction.class);
	}

	public UserMatchWallet getUserMatchWallet(Long userId, Long matchId) {
		Criteria criteria = this.getCurrentSession().createCriteria(UserMatchWallet.class);
		criteria.add(Restrictions.eq("userId", userId));
		criteria.add(Restrictions.eq("matchId", matchId));

		return this.getSingleResultOrNull(criteria, UserMatchWallet.class);
	}

	public List<UserMatchWallet> getAllUserMatchWalletByMatchId(Long matchId) {
		Criteria criteria = this.getCurrentSession().createCriteria(UserMatchWallet.class);
		criteria.add(Restrictions.eq("matchId", matchId));
		return this.getResultList(criteria, UserMatchWallet.class);
	}

}
