package com.psl.order.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.psl.enums.UserRoles;
import com.psl.helper.BaseController;
import com.psl.helper.model.GenericResponse;
import com.psl.helper.utils.auth.PslSecured;
import com.psl.model.order.OrderRequestDto;
import com.psl.model.order.OrderResponseDto;
import com.psl.order.service.OrderService;

@RequestMapping(value = "/order")
@RestController
public class OrderController extends BaseController {

	@Autowired
	private OrderService orderService;
	
	@PslSecured(roles = UserRoles.USER)
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<GenericResponse<OrderResponseDto>> createOrder(
			@Valid @RequestBody final OrderRequestDto orderRequestDto) {
		return pslResponse(orderService.createOrder(orderRequestDto));
	}
	
	@PslSecured(roles = UserRoles.USER)
	@RequestMapping(value = "/{orderId}/close", method = RequestMethod.PUT)
	public ResponseEntity<GenericResponse<String>> closeOrder(
			@PathVariable final String orderId) {
		return pslResponse(orderService.closeOrder(orderId));
	}
	
}
