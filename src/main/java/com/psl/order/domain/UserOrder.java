package com.psl.order.domain;

import java.io.Serializable;
import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Version;


/**
 * The persistent class for the user_order database table.
 * 
 */
@Entity
@Table(name="user_order")
@NamedQuery(name="UserOrder.findAll", query="SELECT u FROM UserOrder u")
public class UserOrder implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	@Column(name="created_at")
	private Long createdAt;

	private Long fee;

	@Column(name="match_id")
	private Long matchId;

	@Column(name="order_id")
	private String orderId;

	@Column(name="player_id")
	private Long playerId;

	private Long price;
	
	private Long closingPrice;
	
	private String orderType;

	private String status;
	
	@Column(name="active_status")
	private Boolean activeStatus;

	@Column(name="updated_at")
	private Long updatedAt;

	@Column(name="user_id")
	private Long userId;

	@Version
	private int version;

	public UserOrder(Long fee, Long matchId, String orderId, Long playerId, Long price, Long closingPrice, String orderType, String status,
			Boolean activeStatus, Long userId) {
		super();
		this.fee = fee;
		this.matchId = matchId;
		this.orderId = orderId;
		this.playerId = playerId;
		this.price = price;
		this.closingPrice = closingPrice;
		this.orderType = orderType;
		this.status = status;
		this.activeStatus = activeStatus;
		this.userId = userId;
	}

	public UserOrder() {
	}
	
	@PrePersist
	protected void onCreate() {
		this.createdAt = Instant.now().toEpochMilli();
		this.updatedAt = Instant.now().toEpochMilli();
	}

	@PreUpdate
	protected void onUpdate() {
		this.updatedAt = Instant.now().toEpochMilli();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Long createdAt) {
		this.createdAt = createdAt;
	}

	public Long getFee() {
		return fee;
	}

	public void setFee(Long fee) {
		this.fee = fee;
	}

	public Long getMatchId() {
		return matchId;
	}

	public void setMatchId(Long matchId) {
		this.matchId = matchId;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public Long getPlayerId() {
		return playerId;
	}

	public void setPlayerId(Long playerId) {
		this.playerId = playerId;
	}

	public Long getPrice() {
		return price;
	}

	public void setPrice(Long price) {
		this.price = price;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Long updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public Boolean getActiveStatus() {
		return activeStatus;
	}

	public void setActiveStatus(Boolean activeStatus) {
		this.activeStatus = activeStatus;
	}

	public Long getClosingPrice() {
		return closingPrice;
	}

	public void setClosingPrice(Long closingPrice) {
		this.closingPrice = closingPrice;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
}