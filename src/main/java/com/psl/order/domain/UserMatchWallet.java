package com.psl.order.domain;

import java.io.Serializable;
import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Version;

/**
 * The persistent class for the user_match_wallet database table.
 * 
 */
@Entity
@Table(name = "user_match_wallet")
@NamedQuery(name = "UserMatchWallet.findAll", query = "SELECT u FROM UserMatchWallet u")
public class UserMatchWallet implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "created_at")
	private Long createdAt;

	@Column(name = "match_user_balance")
	private Long matchUserBalance;

	private int rank;

	@Column(name = "transaction_count")
	private int transactionCount;

	@Column(name = "updated_at")
	private Long updatedAt;

	@Column(name = "user_match_id")
	private String userMatchId;

	@Version
	private int version;

	@Column(name = "match_id")
	private Long matchId;

	@Column(name = "user_id")
	private Long userId;

	public UserMatchWallet() {
	}

	public UserMatchWallet(Long matchUserBalance, int rank, int transactionCount, String userMatchId, Long matchId,
			Long userId) {
		super();
		this.matchUserBalance = matchUserBalance;
		this.rank = rank;
		this.transactionCount = transactionCount;
		this.userMatchId = userMatchId;
		this.matchId = matchId;
		this.userId = userId;
	}

	@PrePersist
	protected void onCreate() {
		this.createdAt = Instant.now().toEpochMilli();
		this.updatedAt = Instant.now().toEpochMilli();
	}

	@PreUpdate
	protected void onUpdate() {
		this.updatedAt = Instant.now().toEpochMilli();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Long createdAt) {
		this.createdAt = createdAt;
	}

	public Long getMatchUserBalance() {
		return matchUserBalance;
	}

	public void setMatchUserBalance(Long matchUserBalance) {
		this.matchUserBalance = matchUserBalance;
	}

	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	public int getTransactionCount() {
		return transactionCount;
	}

	public void setTransactionCount(int transactionCount) {
		this.transactionCount = transactionCount;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public Long getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Long updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getUserMatchId() {
		return userMatchId;
	}

	public void setUserMatchId(String userMatchId) {
		this.userMatchId = userMatchId;
	}

	public Long getMatchId() {
		return matchId;
	}

	public void setMatchId(Long matchId) {
		this.matchId = matchId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}
}