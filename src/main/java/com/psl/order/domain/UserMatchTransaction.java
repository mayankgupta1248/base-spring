package com.psl.order.domain;

import java.io.Serializable;
import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Version;


/**
 * The persistent class for the user_match_transaction database table.
 * 
 */
@Entity
@Table(name="user_match_transaction")
@NamedQuery(name="UserMatchTransaction.findAll", query="SELECT u FROM UserMatchTransaction u")
public class UserMatchTransaction implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	@Column(name="reference_id")
	private String referenceId;
	
	private Long amount;

	@Column(name="created_at")
	private Long createdAt;

	private Long fee;
	
	private Long total;
	
	@Column(name="initial_wallet_balance")
	private Long initialWalletBalance;

	@Column(name="final_wallet_balance")
	private Long finalWalletBalance;

	private String status;

	@Column(name="transaction_source")
	private String transactionSource;

	@Column(name="transaction_type")
	private String transactionType;

	@Column(name="updated_at")
	private Long updatedAt;

	@Version
	private int version;

	@Column(name="user_match_wallet_id")
	private Long userMatchWalletId;

	public UserMatchTransaction() {
	}
	
	public UserMatchTransaction(String referenceId, Long amount, Long fee, Long total, Long initialWalletBalance,
			Long finalWalletBalance, String status, String transactionSource, String transactionType,
			Long userMatchWalletId) {
		super();
		this.referenceId = referenceId;
		this.amount = amount;
		this.fee = fee;
		this.total = total;
		this.initialWalletBalance = initialWalletBalance;
		this.finalWalletBalance = finalWalletBalance;
		this.status = status;
		this.transactionSource = transactionSource;
		this.transactionType = transactionType;
		this.userMatchWalletId = userMatchWalletId;
	}



	@PrePersist
	protected void onCreate() {
		this.createdAt = Instant.now().toEpochMilli();
		this.updatedAt = Instant.now().toEpochMilli();
	}

	@PreUpdate
	protected void onUpdate() {
		this.updatedAt = Instant.now().toEpochMilli();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getAmount() {
		return amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

	public Long getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Long createdAt) {
		this.createdAt = createdAt;
	}

	public Long getFee() {
		return fee;
	}

	public void setFee(Long fee) {
		this.fee = fee;
	}

	public Long getFinalWalletBalance() {
		return finalWalletBalance;
	}

	public void setFinalWalletBalance(Long finalWalletBalance) {
		this.finalWalletBalance = finalWalletBalance;
	}

	public Long getInitialWalletBalance() {
		return initialWalletBalance;
	}

	public void setInitialWalletBalance(Long initialWalletBalance) {
		this.initialWalletBalance = initialWalletBalance;
	}

	public String getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getTotal() {
		return total;
	}

	public void setTotal(Long total) {
		this.total = total;
	}

	public String getTransactionSource() {
		return transactionSource;
	}

	public void setTransactionSource(String transactionSource) {
		this.transactionSource = transactionSource;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public Long getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Long updatedAt) {
		this.updatedAt = updatedAt;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public Long getUserMatchWalletId() {
		return userMatchWalletId;
	}

	public void setUserMatchWalletId(Long userMatchWalletId) {
		this.userMatchWalletId = userMatchWalletId;
	}
}