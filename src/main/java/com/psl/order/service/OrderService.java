package com.psl.order.service;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.psl.enums.MatchStatus;
import com.psl.enums.OrderStatus;
import com.psl.enums.OrderType;
import com.psl.enums.WalletTransactionSource;
import com.psl.enums.WalletTransactionStatus;
import com.psl.enums.WalletTransactionType;
import com.psl.helper.exception.PslException;
import com.psl.helper.exception.PslForbidden;
import com.psl.helper.exception.PslUnprocessableEntity;
import com.psl.helper.logger.PslLogManager;
import com.psl.helper.logger.PslLogger;
import com.psl.helper.utils.PslMapper;
import com.psl.helper.utils.PslUtil;
import com.psl.helper.utils.request.PslRequestContext;
import com.psl.match.domain.MatchDetail;
import com.psl.match.domain.MatchTeamPlayer;
import com.psl.match.repository.MatchRepository;
import com.psl.model.order.OrderRequestDto;
import com.psl.model.order.OrderResponseDto;
import com.psl.models.user.AccessTokenDetails;
import com.psl.order.domain.UserMatchTransaction;
import com.psl.order.domain.UserMatchWallet;
import com.psl.order.domain.UserOrder;
import com.psl.order.repository.OrderRespository;
import com.psl.order.repository.UserMatchWalletRepository;

@Service
public class OrderService {

	@Autowired
	private MatchRepository matchRepository;

	@Autowired
	private OrderRespository orderRepository;

	@Autowired
	private UserMatchWalletRepository userMatchWalletRepository;

	private PslMapper pslMapper = new PslMapper();

	private static final PslLogger logger = PslLogManager.getLogger(OrderService.class);

	@Transactional
	public OrderResponseDto createOrder(OrderRequestDto orderRequestDto) {

		logger.info("Order create request " + PslUtil.objectToJson(orderRequestDto));

		Long userId = this.getAccessTokenDetails().getUserId();

		MatchDetail matchDetail = this.matchRepository.getMatchById(orderRequestDto.getMatchId());
		if (!matchDetail.getStatus().equalsIgnoreCase(MatchStatus.STARTED.name())) {
			throw new PslUnprocessableEntity("Can not place order, match is " + matchDetail.getStatus());
		}

		MatchTeamPlayer matchTeamPlayer = this.matchRepository.getMatchTeamPlayer(orderRequestDto.getMatchId(),
				orderRequestDto.getPlayerId());
		if (matchTeamPlayer == null || matchTeamPlayer.getStatus() == false) {
			throw new PslUnprocessableEntity("Invalid player");
		}

		UserOrder userOrder = this.orderRepository.getUserOpenOrderForPlayer(orderRequestDto.getMatchId(),
				orderRequestDto.getPlayerId(), userId);
		if (userOrder != null) {
			throw new PslUnprocessableEntity("You already have this player");
		}

		Long fee = null;
		if (orderRequestDto.getOrderType().equalsIgnoreCase(OrderType.BUY.name())) {
			fee = matchDetail.getBuyFee();
		} else if (orderRequestDto.getOrderType().equalsIgnoreCase(OrderType.SELL.name())) {
			fee = matchDetail.getSellFee();
		}
		String orderId = this.generateOrderId();
		userOrder = new UserOrder(fee, orderRequestDto.getMatchId(), orderId, orderRequestDto.getPlayerId(),
				matchTeamPlayer.getPrice(), null, orderRequestDto.getOrderType(), OrderStatus.OPEN.name(), true,
				userId);
		this.orderRepository.create(userOrder);

		UserMatchWallet userMatchWallet = this.userMatchWalletRepository.getUserMatchWallet(userId,
				orderRequestDto.getMatchId());
		Long total = userOrder.getPrice() + userOrder.getFee();
		Long finalWalletBalance = userMatchWallet.getMatchUserBalance() - total;
		UserMatchTransaction userMatchTransaction = new UserMatchTransaction(orderId, userOrder.getPrice(),
				userOrder.getFee(), total, userMatchWallet.getMatchUserBalance(), finalWalletBalance,
				WalletTransactionStatus.SUCCESS.name(), WalletTransactionSource.ORDER_CLOSE.name(),
				WalletTransactionType.WITHDRAWAL.name(), userMatchWallet.getId());
		userMatchWallet.setMatchUserBalance(finalWalletBalance);
		this.userMatchWalletRepository.create(userMatchTransaction);
		this.userMatchWalletRepository.update(userMatchWallet);

		return pslMapper.convertModel(userOrder, OrderResponseDto.class);
	}

	private String generateOrderId() {
		return "PO-" + UUID.randomUUID().toString();
	}

	public AccessTokenDetails getAccessTokenDetails() {
		AccessTokenDetails accessTokenDetails = PslRequestContext.getAccessTokenDetails();
		if (null == accessTokenDetails) {
			throw new PslForbidden("Resource is Forbidden");
		} else {
			return accessTokenDetails;
		}
	}

	@Transactional
	public String closeOrder(String orderId) {
		Long userId = this.getAccessTokenDetails().getUserId();
		logger.info("Close order request " + orderId);

		UserOrder userOrder = this.orderRepository.getOrderbyOrderId(orderId);
		if (userOrder == null) {
			throw new PslUnprocessableEntity("Invalid orderId");
		}
		
		if(!userId.equals(userOrder.getUserId())) {
			throw new PslForbidden("Invalid access to cancell order");
		}

		if (!OrderStatus.OPEN.name().equalsIgnoreCase(userOrder.getStatus())) {
			throw new PslUnprocessableEntity("Invalid order status");
		}

		MatchTeamPlayer matchTeamPlayer = this.matchRepository.getMatchTeamPlayer(userOrder.getMatchId(),
				userOrder.getPlayerId());
		if (!matchTeamPlayer.getStatus()) {
			throw new PslUnprocessableEntity("Player is disabled");
		}

		Long amountToCredit = null;
		UserMatchWallet userMatchWallet = this.userMatchWalletRepository.getUserMatchWallet(userId,
				userOrder.getMatchId());
		
		if(userOrder.getOrderType().equals(OrderType.BUY.name())) {
			amountToCredit = userOrder.getPrice() + matchTeamPlayer.getPrice() - userOrder.getPrice();
		}
		else if(userOrder.getOrderType().equals(OrderType.SELL.name())){
			amountToCredit = userOrder.getPrice() - matchTeamPlayer.getPrice() - userOrder.getPrice();
		}
		else {
			throw new PslException("Invalid order type founde");
		}
		
		Long finalWalletBalance = null;
		UserMatchTransaction userMatchTransaction = null;
		if(amountToCredit.intValue() > 0) {
			finalWalletBalance = userMatchWallet.getMatchUserBalance() + amountToCredit;
			userMatchTransaction = new UserMatchTransaction(orderId, amountToCredit,
						new Long("0"), amountToCredit, userMatchWallet.getMatchUserBalance(), finalWalletBalance,
						WalletTransactionStatus.SUCCESS.name(), WalletTransactionSource.ORDER_CLOSE.name(),
						WalletTransactionType.DEPOSIT.name(), userMatchWallet.getId());
		}
		else {
			finalWalletBalance = userMatchWallet.getMatchUserBalance() - amountToCredit;
			userMatchTransaction = new UserMatchTransaction(orderId, matchTeamPlayer.getPrice(),
					new Long("0"),amountToCredit, userMatchWallet.getMatchUserBalance(), finalWalletBalance,
					WalletTransactionStatus.SUCCESS.name(), WalletTransactionSource.ORDER_CLOSE.name(),
					WalletTransactionType.WITHDRAWAL.name(), userMatchWallet.getId());
		}
		
		
		userMatchWallet.setMatchUserBalance(finalWalletBalance);

		this.userMatchWalletRepository.create(userMatchTransaction);
		this.userMatchWalletRepository.update(userMatchWallet);

		userOrder.setStatus(OrderStatus.CLOSED.name());
		userOrder.setActiveStatus(null);
		userOrder.setClosingPrice(matchTeamPlayer.getPrice());

		this.orderRepository.update(userOrder);

		return "Success";
	}

}
