package com.psl.wallet.repository;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.psl.helper.AbstractBaseRepository;
import com.psl.wallet.domain.LoyaltyWallet;
import com.psl.wallet.domain.PointWallet;

@Repository
public class WalletRepository extends AbstractBaseRepository{

	public PointWallet getPointWalletByUserId(Long userId) {
		Criteria criteria = this.getCurrentSession().createCriteria(PointWallet.class);
		criteria.add(Restrictions.eq("userId", userId));
		return this.getSingleResultOrNull(criteria, PointWallet.class);
	}

	public LoyaltyWallet getLoyaltyWalletByUserId(Long userId) {
		Criteria criteria = this.getCurrentSession().createCriteria(LoyaltyWallet.class);
		criteria.add(Restrictions.eq("userId", userId));
		return this.getSingleResultOrNull(criteria, LoyaltyWallet.class);
	}

}
