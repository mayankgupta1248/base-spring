package com.psl.wallet.domain;

import java.io.Serializable;
import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;


/**
 * The persistent class for the point_wallet_transaction database table.
 * 
 */
@Entity
@Table(name="point_wallet_transaction")
@NamedQuery(name="PointWalletTransaction.findAll", query="SELECT p FROM PointWalletTransaction p")
public class PointWalletTransaction implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="reference_id")
	private String referenceId;

	private Long amount;

	@Column(name="created_at")
	private Long createdAt;

	private Long fees;
	
	@Column(name="total_amount")
	private Long totalAmount;
	
	@Column(name="initial_wallet_balance")
	private Long initialWalletBalance;

	@Column(name="final_wallet_balance")
	private Long finalWalletBalance;
	
	private String gateway;

	@Column(name="initiated_by")
	private String initiatedBy;

	private String remark;

	private String source;

	private String status;

	private String type;

	@Column(name="updated_at")
	private Long updatedAt;
	
	@Column(name="point_wallet_id")
	private Long pointWalletId;

	public PointWalletTransaction() {
	}
	
	public PointWalletTransaction(String referenceId, Long amount, Long fees, Long totalAmount, Long initialWalletBalance,
			Long finalWalletBalance, String gateway, String initiatedBy, String remark, String source, String status,
			String type, Long pointWalletId) {
		super();
		this.referenceId = referenceId;
		this.amount = amount;
		this.fees = fees;
		this.totalAmount = totalAmount;
		this.initialWalletBalance = initialWalletBalance;
		this.finalWalletBalance = finalWalletBalance;
		this.gateway = gateway;
		this.initiatedBy = initiatedBy;
		this.remark = remark;
		this.source = source;
		this.status = status;
		this.type = type;
		this.pointWalletId = pointWalletId;
	}


	@PrePersist
	protected void onCreate() {
		this.createdAt = Instant.now().toEpochMilli();
		this.updatedAt = Instant.now().toEpochMilli();
	}

	@PreUpdate
	protected void onUpdate() {
		this.updatedAt = Instant.now().toEpochMilli();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getAmount() {
		return amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

	public Long getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Long createdAt) {
		this.createdAt = createdAt;
	}

	public Long getFees() {
		return fees;
	}

	public void setFees(Long fees) {
		this.fees = fees;
	}

	public Long getFinalWalletBalance() {
		return finalWalletBalance;
	}

	public void setFinalWalletBalance(Long finalWalletBalance) {
		this.finalWalletBalance = finalWalletBalance;
	}

	public String getGateway() {
		return gateway;
	}

	public void setGateway(String gateway) {
		this.gateway = gateway;
	}

	public Long getInitialWalletBalance() {
		return initialWalletBalance;
	}

	public void setInitialWalletBalance(Long initialWalletBalance) {
		this.initialWalletBalance = initialWalletBalance;
	}

	public String getInitiatedBy() {
		return initiatedBy;
	}

	public void setInitiatedBy(String initiatedBy) {
		this.initiatedBy = initiatedBy;
	}

	public String getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Long totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Long getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Long updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Long getPointWalletId() {
		return pointWalletId;
	}

	public void setPointWalletId(Long pointWalletId) {
		this.pointWalletId = pointWalletId;
	}
}