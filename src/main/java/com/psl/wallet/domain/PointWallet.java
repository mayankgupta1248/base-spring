package com.psl.wallet.domain;

import java.io.Serializable;
import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Version;


/**
 * The persistent class for the point_wallet database table.
 * 
 */
@Entity
@Table(name="point_wallet")
@NamedQuery(name="PointWallet.findAll", query="SELECT p FROM PointWallet p")
public class PointWallet implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	private Long balance;

	@Column(name="created_at")
	private Long createdAt;

	private Boolean status;

	@Column(name="updated_at")
	private Long updatedAt;

	@Version
	private int version;
	
	@Column(name="user_id")
	private Long userId;

	public PointWallet() {
	}
	
	public PointWallet(Long balance, Boolean status, Long userId) {
		super();
		this.balance = balance;
		this.status = status;
		this.userId = userId;
	}

	@PrePersist
	protected void onCreate() {
		this.createdAt = Instant.now().toEpochMilli();
		this.updatedAt = Instant.now().toEpochMilli();
	}

	@PreUpdate
	protected void onUpdate() {
		this.updatedAt = Instant.now().toEpochMilli();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getBalance() {
		return balance;
	}

	public void setBalance(Long balance) {
		this.balance = balance;
	}

	public Long getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Long createdAt) {
		this.createdAt = createdAt;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public Long getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Long updatedAt) {
		this.updatedAt = updatedAt;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}
}