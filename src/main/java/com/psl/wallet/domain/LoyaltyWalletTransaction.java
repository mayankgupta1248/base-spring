package com.psl.wallet.domain;

import java.io.Serializable;
import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

/**
 * The persistent class for the loyalty_wallet_transaction database table.
 * 
 */
@Entity
@Table(name = "loyalty_wallet_transaction")
@NamedQuery(name = "LoyaltyWalletTransaction.findAll", query = "SELECT l FROM LoyaltyWalletTransaction l")
public class LoyaltyWalletTransaction implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private Long amount;

	@Column(name = "created_at")
	private Long createdAt;

	private Long fees;

	@Column(name = "final_wallet_balance")
	private Long finalWalletBalance;

	private String gateway;

	@Column(name = "initial_wallet_balance")
	private Long initialWalletBalance;

	@Column(name = "initiated_by")
	private String initiatedBy;

	@Column(name = "reference_id")
	private String referenceId;

	@Lob
	private String remark;

	private String source;

	private String status;

	@Column(name = "total_amount")
	private Long totalAmount;

	private String type;

	@Column(name = "updated_at")
	private Long updatedAt;

	@Column(name = "loyalty_wallet_id")
	private Long loyaltyWalletId;

	public LoyaltyWalletTransaction() {
	}

	@PrePersist
	protected void onCreate() {
		this.createdAt = Instant.now().toEpochMilli();
		this.updatedAt = Instant.now().toEpochMilli();
	}

	@PreUpdate
	protected void onUpdate() {
		this.updatedAt = Instant.now().toEpochMilli();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getAmount() {
		return amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

	public Long getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Long createdAt) {
		this.createdAt = createdAt;
	}

	public Long getFees() {
		return fees;
	}

	public void setFees(Long fees) {
		this.fees = fees;
	}

	public Long getFinalWalletBalance() {
		return finalWalletBalance;
	}

	public void setFinalWalletBalance(Long finalWalletBalance) {
		this.finalWalletBalance = finalWalletBalance;
	}

	public String getGateway() {
		return gateway;
	}

	public void setGateway(String gateway) {
		this.gateway = gateway;
	}

	public Long getInitialWalletBalance() {
		return initialWalletBalance;
	}

	public void setInitialWalletBalance(Long initialWalletBalance) {
		this.initialWalletBalance = initialWalletBalance;
	}

	public String getInitiatedBy() {
		return initiatedBy;
	}

	public void setInitiatedBy(String initiatedBy) {
		this.initiatedBy = initiatedBy;
	}

	public String getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Long totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Long getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Long updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Long getLoyaltyWalletId() {
		return loyaltyWalletId;
	}

	public void setLoyaltyWalletId(Long loyaltyWalletId) {
		this.loyaltyWalletId = loyaltyWalletId;
	}

}