package com.psl.wallet.sqs;

import javax.jms.Message;
import javax.jms.TextMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import com.psl.helper.logger.PslLogManager;
import com.psl.helper.logger.PslLogger;
import com.psl.helper.sqs.JmsConstant;
import com.psl.wallet.service.WalletService;

@Service
public class WalletSqsListner {

	@Autowired
	private WalletService walletService;

	@Autowired
	@Qualifier(JmsConstant.SQS_JMS_TEMPLATE)
	private JmsTemplate jmsTemplate;

	private static final PslLogger LOGGER = PslLogManager.getLogger(WalletSqsListner.class);

	@JmsListener(destination = "${new_user_wallet_queue}", containerFactory = JmsConstant.SQS_JMS_CONTAINER_FACTORY)
	public void onNewUserRegister(Message message) throws Exception {
		TextMessage textMessage = (TextMessage) message;
		LOGGER.info("New user create wallet request arrived " + textMessage.getText());
		Long userId = Long.parseLong(textMessage.getText());
		walletService.createUserWallet(userId);
		message.acknowledge();
	}

//	@JmsListener(destination = "${wallet_transaction_queue}", containerFactory = JmsConstant.SQS_JMS_CONTAINER_FACTORY)
//	public void onNewWalletTransaction(Message message) throws JMSException {
//		TextMessage textMessage = (TextMessage) message;
//		LOGGER.info("New wallet transaction arrived " + textMessage.getText());
////		WalletInternalTransactionDto walletInternalTransactionDto = PslUtil.jsonToObject(textMessage.getText(),
////				WalletInternalTransactionDto.class);
////		walletService.processWalletTransactionFromSQS(walletInternalTransactionDto);
//		message.acknowledge();
//	}
}
