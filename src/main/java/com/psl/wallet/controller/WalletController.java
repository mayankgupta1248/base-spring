package com.psl.wallet.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.psl.enums.UserRoles;
import com.psl.helper.BaseController;
import com.psl.helper.model.GenericResponse;
import com.psl.helper.utils.auth.PslSecured;
import com.psl.models.wallet.PointWalletInternalTransactionDto;
import com.psl.wallet.service.WalletService;

@RequestMapping(value = "/wallet")
@RestController
public class WalletController extends BaseController{
	
	@Autowired
	private WalletService walletService;
	
	@PslSecured(roles = UserRoles.USER)
	@RequestMapping(value = "/internal-transaction", method = RequestMethod.POST)
	public ResponseEntity<GenericResponse<Boolean>> walletInternalTransaction(
			@RequestBody PointWalletInternalTransactionDto pointWalletInternalTransactionDto) {
		return pslResponse(walletService.createWalletTransaction(pointWalletInternalTransactionDto));
	}
	
}
