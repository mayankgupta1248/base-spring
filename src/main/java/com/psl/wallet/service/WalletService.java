package com.psl.wallet.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.psl.EnvironmentService;
import com.psl.enums.SystemConfig;
import com.psl.enums.WalletTransactionType;
import com.psl.helper.exception.PslBaseException;
import com.psl.helper.exception.PslForbidden;
import com.psl.helper.exception.PslUnprocessableEntity;
import com.psl.helper.logger.PslLogManager;
import com.psl.helper.logger.PslLogger;
import com.psl.helper.utils.PslUtil;
import com.psl.helper.utils.request.PslRequest;
import com.psl.helper.utils.request.PslRequestContext;
import com.psl.model.systemconfig.SystemConfigDto;
import com.psl.models.user.AccessTokenDetails;
import com.psl.models.wallet.PointWalletInternalTransactionDto;
import com.psl.wallet.domain.LoyaltyWallet;
import com.psl.wallet.domain.PointWallet;
import com.psl.wallet.domain.PointWalletTransaction;
import com.psl.wallet.repository.WalletRepository;

@Service
public class WalletService {

	@Autowired
	private EnvironmentService environmentService;

	@Autowired
	private WalletRepository walletRepository;

	@Autowired
	private PslRequest pslRequest;

	private static final PslLogger logger = PslLogManager.getLogger(WalletService.class);

	@Transactional
	public void createUserWallet(Long userId) {
		PointWallet pointWallet = this.walletRepository.getPointWalletByUserId(userId);
		if (pointWallet == null) {

			Map<String, Object> paramMap = new HashMap<>();
			paramMap.put("key", SystemConfig.SIGNUP_REWARD.name());
			String url = PslUtil.generateUrlUsingParams(environmentService.getSystemConfigUrl(), paramMap);
			SystemConfigDto systemConfigDto = pslRequest.request(url, HttpMethod.GET, SystemConfigDto.class);
			if (systemConfigDto == null) {
				throw new PslBaseException("System config not found " + SystemConfig.SIGNUP_REWARD.name());
			}
			pointWallet = new PointWallet(Long.parseLong(systemConfigDto.getValue()), true, userId);
			this.walletRepository.create(pointWallet);
		}

		LoyaltyWallet loyaltyWallet = this.walletRepository.getLoyaltyWalletByUserId(userId);
		if (loyaltyWallet == null) {
			loyaltyWallet = new LoyaltyWallet(new Long("0"), true, userId);
			this.walletRepository.create(loyaltyWallet);
		}
	}

	@Transactional
	public Boolean createWalletTransaction(PointWalletInternalTransactionDto pointWalletInternalTransactionDto) {
		// UserId will be taken from access token in future
		Long userId = this.getAccessTokenDetails().getUserId();
		logger.info("Wallet transaction request " + PslUtil.objectToJson(pointWalletInternalTransactionDto));
		if (!userId.equals(pointWalletInternalTransactionDto.getUserId())) {
			throw new PslForbidden("Invalid access for wallet");
		}
		PointWallet pointWallet = this.walletRepository.getPointWalletByUserId(userId);
		if (WalletTransactionType.WITHDRAWAL.name().equalsIgnoreCase(pointWalletInternalTransactionDto.getType())) {
			isTransactionAmountValid(pointWallet, pointWalletInternalTransactionDto.getTotalAmount());

			// Update point wallet and create its transaction
			Long finalBalance = pointWallet.getBalance() - pointWalletInternalTransactionDto.getTotalAmount();
			PointWalletTransaction pointWalletTransaction = new PointWalletTransaction(pointWalletInternalTransactionDto.getReferenceId(),
					pointWalletInternalTransactionDto.getAmount(),
					pointWalletInternalTransactionDto.getFees(), pointWalletInternalTransactionDto.getTotalAmount(), pointWallet.getBalance(), finalBalance,
					pointWalletInternalTransactionDto.getGateway(),pointWalletInternalTransactionDto.getInitiatedBy(),
					pointWalletInternalTransactionDto.getRemark(), pointWalletInternalTransactionDto.getSource(),
					pointWalletInternalTransactionDto.getStatus(), pointWalletInternalTransactionDto.getType(),
					pointWallet.getId());
			pointWallet.setBalance(finalBalance);
			this.walletRepository.create(pointWalletTransaction);
			this.walletRepository.update(pointWallet);
		}
		//Write code for deposit

		return true;
	}

	private void isTransactionAmountValid(PointWallet pointWallet, Long debitAmount) {
		int val = pointWallet.getBalance().compareTo(debitAmount);
		boolean isValid = (val >= 0);
		if (isValid) {
			return;
		} else {
			throw new PslUnprocessableEntity("Insufficient balance");
		}
	}

	public AccessTokenDetails getAccessTokenDetails() {
		AccessTokenDetails accessTokenDetails = PslRequestContext.getAccessTokenDetails();
		if (null == accessTokenDetails) {
			throw new PslForbidden("Resource is Forbidden");
		} else {
			return accessTokenDetails;
		}
	}
}
