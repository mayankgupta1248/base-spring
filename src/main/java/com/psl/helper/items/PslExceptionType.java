package com.psl.helper.items;

public enum PslExceptionType {
    PslDataFormatException,
    PslDataIntegrityViolationException,
    PslDomainDTOEngineException,
    PslJsonParseException,
    PslMessageNotReadable,
    PslResourceNotFoundException,
    PslEception,
    Exception,
    NoHandlerFoundException,
    HttpRequestMethodNotSupportedException,
    HttpMediaTypeNotSupportedException,
    MethodArgumentNotValidException,
    MissingServletRequestParameterException,
    ConstraintViolationException,
    MethodArgumentTypeMismatchException,
    HttpMessageNotReadableException,
    PslResourceCreateException,
    PslSQLGrammarException,
    PslInvalidDataAccessResourceUsageException,
    PslUnprocessableEntity,
    PslUnauthorized, PslForbidden,
    PslDaoException,
    PslAlreadyProcessedException,
    PslRedisException,
    PslConstraintViolationException
}
