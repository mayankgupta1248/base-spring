package com.psl.helper.items;

public enum PslRequestType {
    Get,
    Post,
    Put,
    Patch,
    DELETE
}
