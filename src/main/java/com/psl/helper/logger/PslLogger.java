package com.psl.helper.logger;

public interface PslLogger {
	void info(String msg);

	void info(String msg, Throwable t);
	
	void debug(String msg);

	void debug(String msg, Throwable t);
	
	void warn(String msg);

	void warn(String msg, Throwable t);
	
	void error(String msg);

	void error(String msg, Throwable t);
}
