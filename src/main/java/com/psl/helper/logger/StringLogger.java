
package com.psl.helper.logger;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.psl.helper.utils.Const;
import com.psl.helper.utils.request.PslRequestContext;

class StringLogger implements PslLogger {

	private Logger logger;

	public StringLogger(final Class<?> clazz) {
		logger = LogManager.getLogger(clazz);
	}

	public void info(String msg) {
		logger.info(addRequestId(msg));
	}

	public void info(String msg, Throwable t) {
		logger.info(addRequestId(msg), t);
	}

	public void debug(String msg) {
		logger.debug(addRequestId(msg));
	}

	public void debug(String msg, Throwable t) {
		logger.debug(addRequestId(msg), t);
	}

	public void warn(String msg) {
		logger.warn(addRequestId(msg));
	}

	public void warn(String msg, Throwable t) {
		logger.warn(addRequestId(msg), t);
	}

	public void error(String msg) {
		logger.error(addRequestId(msg));
	}

	public void error(String msg, Throwable t) {
		logger.error(addRequestId(msg), t);
	}

	private static String addRequestId(String msg) {
		String id = PslRequestContext.getRequestContextValueByName(Const.xRequestId);
		if (msg != null && id != null) {
			msg = id + " " + msg;
		}

		return msg;
	}
}
