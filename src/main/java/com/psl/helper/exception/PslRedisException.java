package com.psl.helper.exception;

public class PslRedisException extends PslBaseException {
    /**
	 * 
	 */
	private static final long serialVersionUID = 5859357669924247011L;
	public PslRedisException() {
        super();
    }

    public PslRedisException(String message, Throwable cause) {
        super(message, cause);
    }

    public PslRedisException(String message) {
        super(message);
    }

    public PslRedisException(Throwable cause) {
        super(cause);
    }
	public PslRedisException(String message, String developerCode, Throwable cause) {
		super(message, developerCode, cause);
	}
}