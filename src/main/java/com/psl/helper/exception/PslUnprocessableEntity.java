package com.psl.helper.exception;

public final class PslUnprocessableEntity extends PslBaseException {
    /**
	 * 
	 */
	private static final long serialVersionUID = -2203597902251653043L;

	public PslUnprocessableEntity() {
        super();
    }

    public PslUnprocessableEntity(String message, Throwable cause) {
        super(message, cause);
    }

    public PslUnprocessableEntity(String message) {
        super(message);
    }

    public PslUnprocessableEntity(Throwable cause) {
        super(cause);
    }
    
    public PslUnprocessableEntity(String message, String developerCode, Throwable cause) {
    	super(message, developerCode, cause);
    }
}
