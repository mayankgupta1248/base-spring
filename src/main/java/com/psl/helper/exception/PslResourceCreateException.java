package com.psl.helper.exception;

public class PslResourceCreateException extends PslBaseException {
    /**
	 * 
	 */
	private static final long serialVersionUID = -2215856943080850871L;

	public PslResourceCreateException() {
		super();
    }

    public PslResourceCreateException(String message) {
        super(message);
    }

    public PslResourceCreateException(String message, Throwable throwable) {
        super(message, throwable);
    }

    public PslResourceCreateException(Throwable throwable) {
        super(throwable);
    }

	public PslResourceCreateException(String message, String developerCode, Throwable cause) {
		super(message, developerCode, cause);
	}
}
