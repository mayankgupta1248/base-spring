package com.psl.helper.exception;

public final class MethodArgumentNotValidException extends PslBaseException {
    /**
	 * 
	 */
	private static final long serialVersionUID = 7618678138583870737L;

	public MethodArgumentNotValidException() {
        super();
    }

    public MethodArgumentNotValidException(String message, Throwable cause) {
        super(message, cause);
    }

    public MethodArgumentNotValidException(String message) {
        super(message);
    }

    public MethodArgumentNotValidException(Throwable cause) {
        super(cause);
    }
    
	public MethodArgumentNotValidException(String message, String developerCode, Throwable cause) {
		super(message, developerCode, cause);
	}
}
