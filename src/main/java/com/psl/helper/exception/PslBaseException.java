package com.psl.helper.exception;

public class PslBaseException extends RuntimeException{

	private static final long serialVersionUID = 787489956373137590L;
	
	private String developerCode;

	public PslBaseException(String message, Throwable cause) {
		super(message, cause);
	}

	public PslBaseException() {
		super();
	}

	public PslBaseException(String message) {
		super(message);
	}

	public PslBaseException(Throwable cause) {
		super(cause);
	}

	public PslBaseException(String message, String developerCode, Throwable cause) {
		super(message, cause);
		this.developerCode = developerCode;
	}

	public String getDeveloperCode() {
		return developerCode;
	}

	public void setDeveloperCode(String developerCode) {
		this.developerCode = developerCode;
	}
}
