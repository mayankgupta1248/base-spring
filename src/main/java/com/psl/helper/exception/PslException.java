package com.psl.helper.exception;

public final class PslException extends PslBaseException {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1514000551762554212L;

	public PslException() {
		super();
	}

	public PslException(String message, Throwable cause) {
		super(message, cause);
	}

	public PslException(String message) {
		super(message);
	}

	public PslException(Throwable cause) {
		super(cause);
	}

	public PslException(String message, String developerCode, Throwable cause) {
		super(message, developerCode, cause);
	}
}
