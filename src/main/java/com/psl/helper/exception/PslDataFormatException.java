package com.psl.helper.exception;

public final class PslDataFormatException extends PslBaseException {
    /**
	 * 
	 */
	private static final long serialVersionUID = -4695804289068152625L;

	public PslDataFormatException() {
        super();
    }

    public PslDataFormatException(String message, Throwable cause) {
        super(message, cause);
    }

    public PslDataFormatException(String message) {
        super(message);
    }

    public PslDataFormatException(Throwable cause) {
        super(cause);
    }
    
    public PslDataFormatException(String message, String developerCode, Throwable cause) {
    	super(message, developerCode, cause);
    }
}
