package com.psl.helper.exception;

public final class PslHttpMediaTypeNotSupportedException extends PslBaseException {
    /**
	 * 
	 */
	private static final long serialVersionUID = -6744258831694740488L;

	public PslHttpMediaTypeNotSupportedException() {
        super();
    }

    public PslHttpMediaTypeNotSupportedException(String message, Throwable cause) {
        super(message, cause);
    }

    public PslHttpMediaTypeNotSupportedException(String message) {
        super(message);
    }

    public PslHttpMediaTypeNotSupportedException(Throwable cause) {
        super(cause);
    }
    
	public PslHttpMediaTypeNotSupportedException(String message, String developerCode, Throwable cause) {
		super(message, developerCode, cause);
	}
}
