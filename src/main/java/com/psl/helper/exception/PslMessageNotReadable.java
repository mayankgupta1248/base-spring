package com.psl.helper.exception;

public final class PslMessageNotReadable extends PslBaseException {
    /**
	 * 
	 */
	private static final long serialVersionUID = 2062945887928867607L;
	public PslMessageNotReadable() {
        super();
    }

    public PslMessageNotReadable(String message, Throwable cause) {
        super(message, cause);
    }

    public PslMessageNotReadable(String message) {
        super(message);
    }

    public PslMessageNotReadable(Throwable cause) {
        super(cause);
    }
	public PslMessageNotReadable(String message, String developerCode, Throwable cause) {
		super(message, developerCode, cause);
	}
}
