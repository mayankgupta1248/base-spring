package com.psl.helper.exception;

public class PslDomainDTOEngineException extends PslBaseException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6291497025079268421L;

	public PslDomainDTOEngineException() {
		super();
	}

	public PslDomainDTOEngineException(String s) {
		super(s);
	}

	public PslDomainDTOEngineException(String s, Throwable throwable) {
		super(s, throwable);
	}

	public PslDomainDTOEngineException(Throwable throwable) {
		super(throwable);
	}

	public PslDomainDTOEngineException(String message, String developerCode, Throwable cause) {
		super(message, developerCode, cause);
	}
}
