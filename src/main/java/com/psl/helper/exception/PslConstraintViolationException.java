package com.psl.helper.exception;

public final class PslConstraintViolationException extends PslBaseException {
    /**
	 * 
	 */
	private static final long serialVersionUID = 7394615923756149899L;

	public PslConstraintViolationException() {
        super();
    }

    public PslConstraintViolationException(String message, Throwable cause) {
        super(message, cause);
    }

    public PslConstraintViolationException(String message) {
        super(message);
    }

    public PslConstraintViolationException(Throwable cause) {
        super(cause);
    }
    
    public PslConstraintViolationException(String message, String developerCode, Throwable cause) {
    	super(message, developerCode, cause);
    }
}
