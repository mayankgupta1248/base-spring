package com.psl.helper.exception;

public final class PslMethodArgumentTypeMismatchException extends PslBaseException {
    /**
	 * 
	 */
	private static final long serialVersionUID = 387514300452044744L;
	public PslMethodArgumentTypeMismatchException() {
        super();
    }

    public PslMethodArgumentTypeMismatchException(String message, Throwable cause) {
        super(message, cause);
    }

    public PslMethodArgumentTypeMismatchException(String message) {
        super(message);
    }

    public PslMethodArgumentTypeMismatchException(Throwable cause) {
        super(cause);
    }
	public PslMethodArgumentTypeMismatchException(String message, String developerCode, Throwable cause) {
		super(message, developerCode, cause);
	}
}
