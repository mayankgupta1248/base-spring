package com.psl.helper.exception;

public final class PslHttpMessageNotReadableException extends PslBaseException {
    /**
	 * 
	 */
	private static final long serialVersionUID = -1562559299386714274L;
	public PslHttpMessageNotReadableException() {
        super();
    }

    public PslHttpMessageNotReadableException(String message, Throwable cause) {
        super(message, cause);
    }

    public PslHttpMessageNotReadableException(String message) {
        super(message);
    }

    public PslHttpMessageNotReadableException(Throwable cause) {
        super(cause);
    }
	public PslHttpMessageNotReadableException(String message, String developerCode, Throwable cause) {
		super(message, developerCode, cause);
	}
}
