package com.psl.helper.exception;

public class PslInvalidDataAccessResourceUsageException extends PslBaseException {

    /**
	 * 
	 */
	private static final long serialVersionUID = -2461652881705137587L;
	public PslInvalidDataAccessResourceUsageException(String message, Throwable cause) {
        super(message, cause);
    }

    public PslInvalidDataAccessResourceUsageException(String message) {
        super(message);
    }

    public PslInvalidDataAccessResourceUsageException(Throwable cause) {
        super(cause);
    }
	public PslInvalidDataAccessResourceUsageException(String message, String developerCode, Throwable cause) {
		super(message, developerCode, cause);
	}
}

