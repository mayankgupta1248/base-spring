package com.psl.helper.exception;

public class PslResourceUpdateException extends PslBaseException {

    /**
	 * 
	 */
	private static final long serialVersionUID = 7411005386068252438L;

	public PslResourceUpdateException() {
		super();
    }

    public PslResourceUpdateException(String s) {
        super(s);
    }

    public PslResourceUpdateException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public PslResourceUpdateException(Throwable throwable) {
        super(throwable);
    }

	public PslResourceUpdateException(String message, String developerCode, Throwable cause) {
		super(message, developerCode, cause);
	}
}
