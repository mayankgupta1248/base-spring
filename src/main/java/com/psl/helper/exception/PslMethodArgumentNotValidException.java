package com.psl.helper.exception;

public final class PslMethodArgumentNotValidException extends PslBaseException {
    /**
	 * 
	 */
	private static final long serialVersionUID = -6915021830647892956L;
	public PslMethodArgumentNotValidException() {
        super();
    }

    public PslMethodArgumentNotValidException(String message, Throwable cause) {
        super(message, cause);
    }

    public PslMethodArgumentNotValidException(String message) {
        super(message);
    }

    public PslMethodArgumentNotValidException(Throwable cause) {
        super(cause);
    }
	public PslMethodArgumentNotValidException(String message, String developerCode, Throwable cause) {
		super(message, developerCode, cause);
	}
}
