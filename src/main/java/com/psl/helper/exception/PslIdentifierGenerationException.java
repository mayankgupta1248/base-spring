package com.psl.helper.exception;

public final class PslIdentifierGenerationException extends PslBaseException {
    /**
	 * 
	 */
	private static final long serialVersionUID = 8267548414436801714L;
	public PslIdentifierGenerationException() {
        super();
    }

    public PslIdentifierGenerationException(String message, Throwable cause) {
        super(message, cause);
    }

    public PslIdentifierGenerationException(String message) {
        super(message);
    }

    public PslIdentifierGenerationException(Throwable cause) {
        super(cause);
    }
	public PslIdentifierGenerationException(String message, String developerCode, Throwable cause) {
		super(message, developerCode, cause);
	}
}
