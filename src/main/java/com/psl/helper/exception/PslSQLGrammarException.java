package com.psl.helper.exception;

public class PslSQLGrammarException extends PslBaseException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6115192028775912753L;

	public PslSQLGrammarException() {
		super();
	}

	public PslSQLGrammarException(String message) {
		super(message);
	}

	public PslSQLGrammarException(String message, Throwable cause) {
		super(message, cause);
	}

	public PslSQLGrammarException(Throwable cause) {
		super(cause);
	}

	public PslSQLGrammarException(String message, String developerCode, Throwable cause) {
		super(message, developerCode, cause);
	}
}
