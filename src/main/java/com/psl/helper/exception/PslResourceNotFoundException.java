package com.psl.helper.exception;

public class PslResourceNotFoundException extends PslBaseException {
    /**
	 * 
	 */
	private static final long serialVersionUID = 6322945054533611332L;
	public PslResourceNotFoundException() {
        super();
    }

    public PslResourceNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public PslResourceNotFoundException(String message) {
        super(message);
    }

    public PslResourceNotFoundException(Throwable cause) {
        super(cause);
    }
	public PslResourceNotFoundException(String message, String developerCode, Throwable cause) {
		super(message, developerCode, cause);
	}
}