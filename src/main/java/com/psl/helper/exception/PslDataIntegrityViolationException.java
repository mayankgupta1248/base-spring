package com.psl.helper.exception;

public final class PslDataIntegrityViolationException extends PslBaseException {
    /**
	 * 
	 */
	private static final long serialVersionUID = -3899221256474554768L;

	public PslDataIntegrityViolationException() {
        super();
    }

    public PslDataIntegrityViolationException(String message, Throwable cause) {
        super(message, cause);
    }

    public PslDataIntegrityViolationException(String message) {
        super(message);
    }

    public PslDataIntegrityViolationException(Throwable cause) {
        super(cause);
    }
    
    public PslDataIntegrityViolationException(String message, String developerCode, Throwable cause) {
    	super(message, developerCode, cause);
    }
}
