package com.psl.helper.exception;

public final class PslMissingServletRequestParameterException extends PslBaseException {
    /**
	 * 
	 */
	private static final long serialVersionUID = 7674814600103035378L;
	public PslMissingServletRequestParameterException() {
        super();
    }

    public PslMissingServletRequestParameterException(String message, Throwable cause) {
        super(message, cause);
    }

    public PslMissingServletRequestParameterException(String message) {
        super(message);
    }

    public PslMissingServletRequestParameterException(Throwable cause) {
        super(cause);
    }
	public PslMissingServletRequestParameterException(String message, String developerCode, Throwable cause) {
		super(message, developerCode, cause);
	}
}
