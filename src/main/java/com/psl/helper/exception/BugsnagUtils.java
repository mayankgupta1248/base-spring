package com.psl.helper.exception;


import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bugsnag.Bugsnag;

@Component
public class BugsnagUtils {

	@Autowired
	Bugsnag bugsnag;

	/**
	 * Notify with some Data
	 *
	 * @param e
	 *            Exception
	 * @param tabName
	 *            TabName for Extra Info
	 * @param info
	 *            Map<String, String> Key Value pair of information
	 * @return boolean
	 */
	public boolean notify(Exception e, String tabName, Map<String, String> info) {
		return bugsnag.notify(e, (report -> {
			if (info != null) {
				info.forEach((key, value) -> {
					report.addToTab(tabName, key, value);
				});
			}
		}));
	}

	/**
	 * Notify with some Data
	 *
	 * @param e
	 *            Exception
	 * @param info
	 *            Map<String, String> Key Value pair of information
	 * @return boolean
	 */
	public boolean notify(Exception e, Map<String, String> info) {
		return notify(e, "Data", info);
	}

	/**
	 * Notify to Bugsnag about Error
	 *
	 * @param e
	 *            Exception
	 * @return boolean
	 */
	public boolean notify(Exception e) {
		return bugsnag.notify(e);
	}

	public boolean notify(Exception e, String keyName, String data) {
		return bugsnag.notify(e, (report -> {
			report.addToTab("Data", keyName, data);
		}));
	}
}
