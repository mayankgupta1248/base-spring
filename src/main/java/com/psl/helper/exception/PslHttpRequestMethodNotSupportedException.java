package com.psl.helper.exception;

public final class PslHttpRequestMethodNotSupportedException extends PslBaseException {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1519197538818879080L;
	public PslHttpRequestMethodNotSupportedException() {
        super();
    }

    public PslHttpRequestMethodNotSupportedException(String message, Throwable cause) {
        super(message, cause);
    }

    public PslHttpRequestMethodNotSupportedException(String message) {
        super(message);
    }

    public PslHttpRequestMethodNotSupportedException(Throwable cause) {
        super(cause);
    }
	public PslHttpRequestMethodNotSupportedException(String message, String developerCode, Throwable cause) {
		super(message, developerCode, cause);
	}
}
