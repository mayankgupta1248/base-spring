package com.psl.helper.exception;

public class PslAlreadyProcessedEntity extends PslBaseException {

    /**
	 * 
	 */
	private static final long serialVersionUID = 4877828598726065470L;

	public PslAlreadyProcessedEntity() {
        super();
    }

    public PslAlreadyProcessedEntity(String message, Throwable cause) {
        super(message, cause);
    }

    public PslAlreadyProcessedEntity(String message) {
        super(message);
    }

    public PslAlreadyProcessedEntity(Throwable cause) {
        super(cause);
    }
    
    public PslAlreadyProcessedEntity(String message, String developerCode, Throwable cause) {
    	super(message, developerCode, cause);
    }
}
