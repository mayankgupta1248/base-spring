package com.psl.helper.exception;

public final class PslJsonParseException extends PslBaseException {
    /**
	 * 
	 */
	private static final long serialVersionUID = 2159910642246194817L;
	
	public PslJsonParseException() {
        super();
    }

    public PslJsonParseException(String message, Throwable cause) {
        super(message, cause);
    }

    public PslJsonParseException(String message) {
        super(message);
    }

    public PslJsonParseException(Throwable cause) {
        super(cause);
    }
	public PslJsonParseException(String message, String developerCode, Throwable cause) {
		super(message, developerCode, cause);
	}
}
