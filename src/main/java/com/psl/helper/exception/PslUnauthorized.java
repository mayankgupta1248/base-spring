package com.psl.helper.exception;

public final class PslUnauthorized extends PslBaseException {
    /**
	 * 
	 */
	private static final long serialVersionUID = 4418268306323812634L;

	public PslUnauthorized() {
        super();
    }

    public PslUnauthorized(String message, Throwable cause) {
        super(message, cause);
    }

    public PslUnauthorized(String message) {
        super(message);
    }

    public PslUnauthorized(Throwable cause) {
        super(cause);
    }
    
	public PslUnauthorized(String message, String developerCode, Throwable cause) {
		super(message, developerCode, cause);
	}
}
