package com.psl.helper.exception;

public class PslEmailException extends PslBaseException {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1606549081722140726L;

	public PslEmailException() {
		super();
	}

	public PslEmailException(String message, Throwable cause) {
		super(message, cause);
	}

	public PslEmailException(String message) {
		super(message);
	}

	public PslEmailException(Throwable cause) {
		super(cause);
	}

	public PslEmailException(String message, String developerCode, Throwable cause) {
		super(message, developerCode, cause);
	}
}
