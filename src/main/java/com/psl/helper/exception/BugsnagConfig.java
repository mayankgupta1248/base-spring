package com.psl.helper.exception;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.xml.sax.SAXException;

import com.bugsnag.Bugsnag;
import com.bugsnag.Report;
import com.bugsnag.callbacks.Callback;
import com.psl.helper.logger.PslLogManager;
import com.psl.helper.logger.PslLogger;
import com.psl.helper.utils.Const;
import com.psl.helper.utils.request.PslRequestContext;


@Configuration
public class BugsnagConfig {
	
	private static final PslLogger logger = PslLogManager.getLogger(BugsnagConfig.class);
	
	@Value("${psl_bugsnag_key}")
	private String bugsnagKey;

	@Bean
	public Bugsnag bugsnag() throws PslException, ParserConfigurationException, SAXException, IOException {
		logger.info("Initializing bugsnag");
		if (bugsnagKey == null) {
			throw new PslException("BUGSNAG_KEY key is necessary.");
		} else {
			Bugsnag bugsnag = new Bugsnag(bugsnagKey);
			bugsnag.addCallback(new Callback() {
				@Override
				public void beforeNotify(Report report) {
					report.addToTab("Metadata", Const.xRequestId,
							PslRequestContext.getRequestContextValueByName(Const.xRequestId));
				}
			});

			return bugsnag;
		}
	}
}
