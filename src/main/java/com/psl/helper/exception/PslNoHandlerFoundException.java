package com.psl.helper.exception;

public final class PslNoHandlerFoundException extends PslBaseException {
    /**
	 * 
	 */
	private static final long serialVersionUID = 7525310142370322990L;
	public PslNoHandlerFoundException() {
        super();
    }

    public PslNoHandlerFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public PslNoHandlerFoundException(String message) {
        super(message);
    }

    public PslNoHandlerFoundException(Throwable cause) {
        super(cause);
    }
	public PslNoHandlerFoundException(String message, String developerCode, Throwable cause) {
		super(message, developerCode, cause);
	}
}
