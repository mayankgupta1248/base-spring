package com.psl.helper.exception;

public class PslResourceDeleteException extends PslBaseException {
    /**
	 * 
	 */
	private static final long serialVersionUID = 2793891630600632707L;

	public PslResourceDeleteException() {
		super();
    }

    public PslResourceDeleteException(String s) {
        super(s);
    }

    public PslResourceDeleteException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public PslResourceDeleteException(Throwable throwable) {
        super(throwable);
    }

	public PslResourceDeleteException(String message, String developerCode, Throwable cause) {
		super(message, developerCode, cause);
	}
}
