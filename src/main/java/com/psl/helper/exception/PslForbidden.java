package com.psl.helper.exception;

public final class PslForbidden extends PslBaseException {
    /**
	 * 
	 */
	private static final long serialVersionUID = -3918111598646726514L;

	public PslForbidden() {
        super();
    }

    public PslForbidden(String message, Throwable cause) {
        super(message, cause);
    }

    public PslForbidden(String message) {
        super(message);
    }

    public PslForbidden(Throwable cause) {
        super(cause);
    }
    
	public PslForbidden(String message, String developerCode, Throwable cause) {
		super(message, developerCode, cause);
	}
}
