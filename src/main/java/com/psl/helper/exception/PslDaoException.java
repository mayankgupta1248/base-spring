package com.psl.helper.exception;

public class PslDaoException extends PslBaseException {

    /**
	 * 
	 */
	private static final long serialVersionUID = -6339916831850066285L;

    public PslDaoException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public PslDaoException(Throwable throwable) {
        super(throwable);
    }
    
    public PslDaoException(String message) {
        super(message);
    }
    
    public PslDaoException() {
    	super();
    }
    
    public PslDaoException(String message, String developerCode, Throwable cause) {
    	super(message, developerCode, cause);
    }
}
