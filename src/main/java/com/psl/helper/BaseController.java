package com.psl.helper;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;

import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.bugsnag.Bugsnag;
import com.psl.helper.exception.PslAlreadyProcessedEntity;
import com.psl.helper.exception.PslConstraintViolationException;
import com.psl.helper.exception.PslDaoException;
import com.psl.helper.exception.PslDataIntegrityViolationException;
import com.psl.helper.exception.PslDomainDTOEngineException;
import com.psl.helper.exception.PslForbidden;
import com.psl.helper.exception.PslInvalidDataAccessResourceUsageException;
import com.psl.helper.exception.PslJsonParseException;
import com.psl.helper.exception.PslMessageNotReadable;
import com.psl.helper.exception.PslRedisException;
import com.psl.helper.exception.PslResourceCreateException;
import com.psl.helper.exception.PslResourceNotFoundException;
import com.psl.helper.exception.PslSQLGrammarException;
import com.psl.helper.exception.PslUnauthorized;
import com.psl.helper.exception.PslUnprocessableEntity;
import com.psl.helper.items.PslExceptionType;
import com.psl.helper.logger.PslLogManager;
import com.psl.helper.logger.PslLogger;
import com.psl.helper.model.GenericError;
import com.psl.helper.model.GenericResponse;

@ControllerAdvice
public class BaseController {
	
	@Autowired
	Bugsnag bugsnag;

	private static final PslLogger logger = PslLogManager.getLogger(BaseController.class);

	public <E> ResponseEntity<GenericResponse<E>> pslResponse(E obj, HttpStatus httpStatus) {
		GenericResponse<E> restResponseDTO = new GenericResponse<E>(obj, httpStatus.value(), null);
		return new ResponseEntity<GenericResponse<E>>(restResponseDTO, httpStatus);
	}

	public <Any> ResponseEntity<GenericResponse<Any>> pslResponse(Any obj) {
		return pslResponse(obj, HttpStatus.OK);
	}

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(PslMessageNotReadable.class)
	public @ResponseBody GenericResponse<Object> handlePslMessageNotReadable(PslMessageNotReadable ex) {
		logger.error("Exception : ", ex);
		GenericError error = new GenericError(PslExceptionType.PslMessageNotReadable, ex.getDeveloperCode(),
				ex.getMessage(), Instant.now().toEpochMilli(), getErrors(ex.getMessage()));
//		bugsnag.notify(ex);
		return new GenericResponse<>(null, HttpStatus.BAD_REQUEST.value(), error);
	}

	@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
	@ExceptionHandler(PslUnprocessableEntity.class)
	@ResponseBody
	public GenericResponse<Object> handlePslUnprocessableEntity(PslUnprocessableEntity ex) {
		logger.error("Exception : " + ex.getMessage());
		GenericError error = new GenericError(PslExceptionType.PslUnprocessableEntity, ex.getDeveloperCode(),
				ex.getMessage(), Instant.now().toEpochMilli(), getErrors(ex.getMessage()));
//		bugsnag.notify(ex);
		return new GenericResponse<>(null, HttpStatus.UNPROCESSABLE_ENTITY.value(), error);
	}

	@ResponseStatus(HttpStatus.ALREADY_REPORTED)
	@ExceptionHandler(PslAlreadyProcessedEntity.class)
	@ResponseBody
	public GenericResponse<Object> handlePsltAlreadyProcessedEntity(PslAlreadyProcessedEntity ex) {
		logger.error("Exception : ", ex);
		GenericError error = new GenericError(PslExceptionType.PslAlreadyProcessedException, ex.getDeveloperCode(),
				ex.getMessage(), Instant.now().toEpochMilli(), getErrors(ex.getMessage()));
//		bugsnag.notify(ex);
		return new GenericResponse<>(null, HttpStatus.ALREADY_REPORTED.value(), error);
	}

	@ResponseStatus(HttpStatus.UNAUTHORIZED)
	@ExceptionHandler(PslUnauthorized.class)
	@ResponseBody
	public GenericResponse<Object> handlePslUnauthorized(PslUnauthorized ex) {
		logger.error("Exception : " + ex.getMessage());
		GenericError error = new GenericError(PslExceptionType.PslUnauthorized,ex.getDeveloperCode(), ex.getMessage(),
				Instant.now().toEpochMilli(), this.getErrors(ex.getMessage()));
//		bugsnag.notify(ex);
		return new GenericResponse<>(null, HttpStatus.UNAUTHORIZED.value(), error);
	}

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(PslJsonParseException.class)
	public @ResponseBody GenericResponse<Object> handlePslJsonParseException(PslMessageNotReadable ex, WebRequest request,
			HttpServletResponse response) {
		logger.error("Exception : ", ex);
		GenericError error = new GenericError(PslExceptionType.PslJsonParseException,ex.getDeveloperCode(), ex.getMessage(),
				Instant.now().toEpochMilli(), getErrors(ex.getMessage()));
//		bugsnag.notify(ex);
		return new GenericResponse<>(null, HttpStatus.BAD_REQUEST.value(), error);
	}

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public @ResponseBody GenericResponse<Object> handleMethodArgumentNotValidException(
			MethodArgumentNotValidException ex, WebRequest request, HttpServletResponse response) {
		logger.error("Exception : ", ex);
		ArrayList<String> errors = new ArrayList<String>();

		for (ObjectError error : ex.getBindingResult().getAllErrors()) {
			errors.add(error.getObjectName() + ": " + error.getDefaultMessage());
		}
		GenericError error = new GenericError(PslExceptionType.MethodArgumentNotValidException, null,ex.getMessage(),
				Instant.now().toEpochMilli(), errors);
//		bugsnag.notify(ex);
		return new GenericResponse<>(null, HttpStatus.BAD_REQUEST.value(), error);
	}

	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ExceptionHandler(NoHandlerFoundException.class)
	public @ResponseBody GenericResponse<Object> handleNotFound(NoHandlerFoundException ex, HttpHeaders headers,
			HttpStatus status, WebRequest request) {
		logger.error("Exception : ", ex);
		GenericError error = new GenericError(PslExceptionType.NoHandlerFoundException, null, ex.getMessage(),
				Instant.now().toEpochMilli(),
				getErrors("No handler found for " + ex.getHttpMethod() + " " + ex.getRequestURL()));
		bugsnag.notify(ex);
		return new GenericResponse<>(null, HttpStatus.NOT_FOUND.value(), error);
	}

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MissingServletRequestParameterException.class)
	public @ResponseBody GenericResponse<Object> handleMissingServletRequestParameterException(
			MissingServletRequestParameterException ex, WebRequest request, HttpServletResponse response) {
		logger.error("Exception : ", ex);
		GenericError error = new GenericError(PslExceptionType.MissingServletRequestParameterException, null,
				ex.getMessage(), Instant.now().toEpochMilli(),
				getErrors(ex.getParameterName() + " parameter is missing"));
//		bugsnag.notify(ex);
		return new GenericResponse<>(null, HttpStatus.BAD_REQUEST.value(), error);
	}

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(ConstraintViolationException.class)
	public @ResponseBody GenericResponse<Object> handleConstraintViolationExceptionCustom(
			ConstraintViolationException ex, WebRequest request, HttpServletResponse response) {
		logger.error("Exception : ", ex);
		ArrayList<String> errors = new ArrayList<>();
		for (ConstraintViolation<?> violation : ex.getConstraintViolations()) {
			errors.add(violation.getRootBeanClass().getName() + " " + violation.getPropertyPath() + ": "
					+ violation.getMessage());
		}
		GenericError error = new GenericError(PslExceptionType.ConstraintViolationException, null, ex.getMessage(),
				Instant.now().toEpochMilli(), errors);
		bugsnag.notify(ex);
		return new GenericResponse<>(null, HttpStatus.BAD_REQUEST.value(), error);
	}

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(PslConstraintViolationException.class)
	public @ResponseBody GenericResponse<Object> handlePslConstraintViolcationException(
			PslConstraintViolationException ex, WebRequest webRequest, HttpServletResponse response) {
		logger.error("Exception : ", ex);
		GenericError errorModel = new GenericError(PslExceptionType.PslConstraintViolationException, ex.getDeveloperCode(), ex.getMessage(),
				Instant.now().toEpochMilli(), getErrors(ex.getMessage()));
		bugsnag.notify(ex);
		return new GenericResponse<>(null, HttpStatus.BAD_REQUEST.value(), errorModel);
	}

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(PslDataIntegrityViolationException.class)
	public @ResponseBody GenericResponse<Object> handlePslDataIntegrityViolationExceptionCustom(
			PslDataIntegrityViolationException ex, WebRequest request, HttpServletResponse response) {
		logger.error("Exception : ", ex);
		GenericError error = new GenericError(PslExceptionType.PslDataIntegrityViolationException,ex.getDeveloperCode(), ex.getMessage(),
				Instant.now().toEpochMilli(), getErrors(ex.getMessage()));
		bugsnag.notify(ex);
		return new GenericResponse<>(null, HttpStatus.BAD_REQUEST.value(), error);
	}

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentTypeMismatchException.class)
	public @ResponseBody GenericResponse<Object> handleMethodArgumentTypeMismatchException(
			MethodArgumentTypeMismatchException ex, WebRequest request, HttpServletResponse response) {
		logger.error("Exception : ", ex);
		GenericError error = new GenericError(PslExceptionType.MethodArgumentTypeMismatchException, null, ex.getMessage(),
				Instant.now().toEpochMilli(),
				getErrors(ex.getName() + " should be of type " + ex.getRequiredType().getName()));
//		bugsnag.notify(ex);
		return new GenericResponse<>(null, HttpStatus.BAD_REQUEST.value(), error);
	}

	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(PslDomainDTOEngineException.class)
	public @ResponseBody GenericResponse<Object> handleException(PslDomainDTOEngineException ex, WebRequest request,
			HttpServletResponse response) {
		logger.error("Exception : ", ex);
		GenericError error = new GenericError(PslExceptionType.PslDomainDTOEngineException, ex.getDeveloperCode(), ex.getMessage(),
				Instant.now().toEpochMilli(), getErrors(ex.getMessage()));
		bugsnag.notify(ex);
		return new GenericResponse<>(null, HttpStatus.INTERNAL_SERVER_ERROR.value(), error);
	}

	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(PslDaoException.class)
	public @ResponseBody GenericResponse<Object> handleException(PslDaoException ex, WebRequest request,
			HttpServletResponse response) {
		logger.error("Exception : ", ex);
		GenericError error = new GenericError(PslExceptionType.PslDaoException, ex.getDeveloperCode(), ex.getMessage(),
				Instant.now().toEpochMilli(), getErrors(ex.getMessage()));
		bugsnag.notify(ex);
		return new GenericResponse<>(null, HttpStatus.INTERNAL_SERVER_ERROR.value(), error);
	}

	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ExceptionHandler(PslResourceNotFoundException.class)
	public @ResponseBody GenericResponse<Object> handlePslResourceNotFoundException(PslResourceNotFoundException ex,
			WebRequest request, HttpServletResponse response) {
		logger.error("Exception : ", ex);
		GenericError error = new GenericError(PslExceptionType.PslResourceNotFoundException,ex.getDeveloperCode(), ex.getMessage(),
				Instant.now().toEpochMilli(), getErrors(ex.getMessage()));
		bugsnag.notify(ex);
		return new GenericResponse<>(null, HttpStatus.NOT_FOUND.value(), error);
	}

	@ResponseStatus(HttpStatus.CONFLICT)
	@ExceptionHandler({ PslResourceCreateException.class })
	@ResponseBody
	public GenericResponse<Object> handlePslResourceCreateException(PslResourceCreateException ex, WebRequest request,
			HttpServletResponse response) {
		logger.error("Exception : ", ex);
		GenericError error = new GenericError(PslExceptionType.PslResourceCreateException,ex.getDeveloperCode(), ex.getMessage(),
				Instant.now().toEpochMilli(), getErrors(ex.getMessage()));
		bugsnag.notify(ex);
		return new GenericResponse<>(null, HttpStatus.CONFLICT.value(), error);
	}

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler({ PslInvalidDataAccessResourceUsageException.class })
	@ResponseBody
	public GenericResponse<Object> handlePslInvalidDataAccessResourceUsageException(
			PslInvalidDataAccessResourceUsageException ex, WebRequest request, HttpServletResponse response) {
		logger.error("Exception : ", ex);
		GenericError error = new GenericError(PslExceptionType.PslInvalidDataAccessResourceUsageException,ex.getDeveloperCode(),
				ex.getMessage(), Instant.now().toEpochMilli(), getErrors(ex.getMessage()));
		bugsnag.notify(ex);
		return new GenericResponse<>(null, HttpStatus.BAD_REQUEST.value(), error);
	}

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler({ PslSQLGrammarException.class })
	@ResponseBody
	public GenericResponse<Object> handlePslSQLGrammarException(PslSQLGrammarException ex, WebRequest request,
			HttpServletResponse response) {
		logger.error("Exception : ", ex);
		GenericError error = new GenericError(PslExceptionType.PslSQLGrammarException,ex.getDeveloperCode(), ex.getMessage(),
				Instant.now().toEpochMilli(), getErrors(ex.getMessage()));
		bugsnag.notify(ex);
		return new GenericResponse<>((Object) null, HttpStatus.BAD_REQUEST.value(), error);
	}

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler({ HttpMessageNotReadableException.class })
	@ResponseBody
	public GenericResponse<Object> handleHttpMessageNotReadableException(HttpMessageNotReadableException ex,
			WebRequest request, HttpServletResponse response) {
		logger.error("Exception : ", ex);
		GenericError error = new GenericError(PslExceptionType.HttpMessageNotReadableException, null, ex.getMessage(),
				Instant.now().toEpochMilli(), getErrors(ex.getMessage()));
		bugsnag.notify(ex);
		return new GenericResponse<>(null, HttpStatus.BAD_REQUEST.value(), error);
	}

	@ResponseStatus(HttpStatus.FORBIDDEN)
	@ExceptionHandler({ PslForbidden.class })
	@ResponseBody
	public GenericResponse<Object> handlePslForbidden(PslForbidden ex) {
		logger.error("Exception : ", ex);
		GenericError error = new GenericError(PslExceptionType.PslForbidden,ex.getDeveloperCode(), ex.getMessage(),
				Instant.now().toEpochMilli(), null);
		bugsnag.notify(ex);
		return new GenericResponse<>(null, HttpStatus.FORBIDDEN.value(), error);
	}

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler({ HttpRequestMethodNotSupportedException.class })
	@ResponseBody
	public GenericResponse<Object> handleHttpRequestMethodNotSupported(HttpRequestMethodNotSupportedException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		logger.error("Exception : ", ex);
		StringBuilder builder = new StringBuilder();
		builder.append(ex.getMethod());
		builder.append(" method is not supported for this request");
		GenericError error = new GenericError(PslExceptionType.HttpRequestMethodNotSupportedException, null,
				ex.getMessage(), Instant.now().toEpochMilli(), getErrors(builder.toString()));
//		bugsnag.notify(ex);
		return new GenericResponse<>(null, HttpStatus.BAD_REQUEST.value(), error);
	}

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler({ HttpMediaTypeNotSupportedException.class })
	@ResponseBody
	public GenericResponse<Object> handleHttpMediaTypeNotSupported(HttpMediaTypeNotSupportedException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		logger.error("Exception : ", ex);
		StringBuilder builder = new StringBuilder();
		builder.append(ex.getContentType());
		builder.append(" media type is not supported");
		GenericError error = new GenericError(PslExceptionType.HttpMediaTypeNotSupportedException, null, ex.getMessage(),
				Instant.now().toEpochMilli(), getErrors(builder.toString()));
//		bugsnag.notify(ex);
		return new GenericResponse<>(null, HttpStatus.BAD_REQUEST.value(), error);
	}

	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(Exception.class)
	public @ResponseBody GenericResponse<Object> handleException(Exception ex, WebRequest request,
			HttpServletResponse response) {
		logger.error("Exception : ", ex);
		GenericError error = new GenericError(PslExceptionType.Exception, null, ex.getMessage(),
				Instant.now().toEpochMilli(), getErrors(ex.getMessage()));
		bugsnag.notify(ex);
		return new GenericResponse<>(null, HttpStatus.INTERNAL_SERVER_ERROR.value(), error);
	}

	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(PslRedisException.class)
	public @ResponseBody GenericResponse<Object> redisException(PslRedisException ex, WebRequest request,
			HttpServletResponse response) {
		logger.error("Exception : ", ex);
		GenericError error = new GenericError(PslExceptionType.PslRedisException,ex.getDeveloperCode(), ex.getMessage(),
				Instant.now().toEpochMilli(), getErrors(ex.getMessage()));
		bugsnag.notify(ex);
		return new GenericResponse<>(null, HttpStatus.INTERNAL_SERVER_ERROR.value(), error);
	}

	private ArrayList<String> getErrors(String... errors) {
		ArrayList<String> errorMessages = new ArrayList<>();
		Collections.addAll(errorMessages, errors);
		return errorMessages;
	}
}
