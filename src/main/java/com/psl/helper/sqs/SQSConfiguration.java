package com.psl.helper.sqs;

import javax.jms.Session;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.core.JmsTemplate;

import com.amazon.sqs.javamessaging.ProviderConfiguration;
import com.amazon.sqs.javamessaging.SQSConnectionFactory;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder.EndpointConfiguration;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.psl.EnvironmentService;

@Configuration
public class SQSConfiguration {
	
	@Autowired
	private EnvironmentService environmentService;
	
	public static final String SQS_REGION = "ap-southeast-1";
	private static final String SQS_ENDPOINT = "http://sqs." + SQS_REGION + ".amazonaws.com/";
	
	@Bean(name = JmsConstant.SQS_JMS_CONTAINER_FACTORY)
	public DefaultJmsListenerContainerFactory createJmsListenerContainerFactory() {
		SQSConnectionFactory sqsConnectionFactory = getSQSConnectionFactory(SQS_REGION,
				SQS_ENDPOINT);
		DefaultJmsListenerContainerFactory dmlc = new DefaultJmsListenerContainerFactory();
		dmlc.setConnectionFactory(sqsConnectionFactory);
		dmlc.setErrorHandler(new JmsCustomExceptionHandler());
		dmlc.setSessionAcknowledgeMode(Session.CLIENT_ACKNOWLEDGE);
		return dmlc;
	}
	
	@Bean(name = JmsConstant.SQS_JMS_TEMPLATE)
	public JmsTemplate createJMSTemplate() {
		SQSConnectionFactory sqsConnectionFactory = getSQSConnectionFactory(SQS_REGION,
				SQS_ENDPOINT);
		return new JmsTemplate(sqsConnectionFactory);
	}

	private SQSConnectionFactory getSQSConnectionFactory(String region, String endpoint) {
		AmazonSQSClientBuilder amazonSQSClientBuilder = AmazonSQSClientBuilder.standard();
		amazonSQSClientBuilder.setEndpointConfiguration(new EndpointConfiguration(endpoint, region));
		amazonSQSClientBuilder.setRegion(region);

		return new SQSConnectionFactory(new ProviderConfiguration(),
				AmazonSQSClientBuilder.standard().withRegion(region).withCredentials(awsCredentialsProvider));
	}
	
	private final AWSCredentialsProvider awsCredentialsProvider = new AWSCredentialsProvider() {
		@Override
		public AWSCredentials getCredentials() {
			return new BasicAWSCredentials(environmentService.getAwsAccessKey(), environmentService.getAwsSecretKey());
		}

		@Override
		public void refresh() {

		}
	};
}
