package com.psl.helper.sqs;

import org.springframework.util.ErrorHandler;

import com.bugsnag.Bugsnag;
import com.psl.helper.logger.PslLogManager;
import com.psl.helper.logger.PslLogger;
import com.psl.helper.utils.AppContextManager;

public class JmsCustomExceptionHandler implements ErrorHandler{

	private final PslLogger logger = PslLogManager.getLogger(this.getClass());
	
	@Override
	public void handleError(Throwable throwable) {
		Bugsnag bugsnag = AppContextManager.getBean(Bugsnag.class);
		logger.error("JMS exception", throwable);
		bugsnag.notify(throwable);
	}
}
