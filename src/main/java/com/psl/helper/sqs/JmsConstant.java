package com.psl.helper.sqs;

public class JmsConstant {
	//JMS Template Names
	public static final String SQS_JMS_CONTAINER_FACTORY = "sqsJmsContainerFactory";	
	public static final String SQS_JMS_TEMPLATE = "sqsJmsTemplate";
}
