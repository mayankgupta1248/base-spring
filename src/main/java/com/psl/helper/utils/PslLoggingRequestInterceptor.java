package com.psl.helper.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

import com.psl.helper.logger.PslLogManager;
import com.psl.helper.logger.PslLogger;

public class PslLoggingRequestInterceptor implements ClientHttpRequestInterceptor {

    private static final PslLogger log = PslLogManager.getLogger(PslLoggingRequestInterceptor.class);

    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
        traceRequest(request, body);
        ClientHttpResponse response = execution.execute(request, body);
        traceResponse(response);
        return response;
    }

    private void traceRequest(HttpRequest request, byte[] body) throws IOException {
        log.debug("===========================request begin================================================");
        log.info("URI         : {}" + request.getURI());
        log.debug("Method      : {}" + request.getMethod());
        log.debug("Headers     : {}" + request.getHeaders() );
        log.info("Request body: {}" + new String(body, "UTF-8"));
        log.debug("==========================request end================================================");
    }

    private void traceResponse(ClientHttpResponse response) throws IOException {
        if (response != null && response.getBody() != null) {
            StringBuilder inputStringBuilder = new StringBuilder();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(response.getBody(), "UTF-8"));
            String line = bufferedReader.readLine();
            while (line != null) {
                inputStringBuilder.append(line);
                inputStringBuilder.append('\n');
                line = bufferedReader.readLine();
            }
            log.debug("============================response begin==========================================");
            log.info("Status code  : {}" + response.getStatusCode());
            log.debug("Status text  : {}" + response.getStatusText());
            log.debug("Headers      : {}" + response.getHeaders());
            log.info("Response body: {}" + inputStringBuilder.toString());
            log.debug("=======================response end=================================================");
        }
    }
}