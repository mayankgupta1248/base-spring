package com.psl.helper.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class RedisUtility {

	@Autowired
	private StringRedisTemplate redisTemplate;

	private ObjectMapper mapper = new ObjectMapper();
	private String namespaceSeparator = "::";

	public void setValue(String namespace, String key, Object value) throws Exception {
		String keyWithNamespace = namespace + namespaceSeparator + key;
		String serializedValue = mapper.writeValueAsString(value);
		this.redisTemplate.opsForValue().set(keyWithNamespace, serializedValue);
	}

	public void setMultiValue(String namespace, Map<String, Object> keys) throws Exception {
		Map<String, String> keyWithValues = new HashMap<>(keys.size());
		for (Map.Entry<String, Object> entry : keys.entrySet()) {
			String keyWithNamespace = namespace + namespaceSeparator + entry.getKey();
			String serializedValue = mapper.writeValueAsString(entry.getValue());
			keyWithValues.put(keyWithNamespace, serializedValue);
		}
		this.redisTemplate.opsForValue().multiSet(keyWithValues);
	}

	public <T> T getValue(String namespace, String key, Class<T> cls) throws Exception {
		String keyWithNamespace = namespace + namespaceSeparator + key;
		String value = this.redisTemplate.opsForValue().get(keyWithNamespace);
		return mapper.readValue(value, cls);
	}

	public <T> List<T> getMultiValue(String namespace, List<String> keys, Class<T> cls) throws Exception {
		List<String> keysWithNamespace = new ArrayList<>(keys.size());
		for (String key : keys) {
			String keyWithNamespace = namespace + namespaceSeparator + key;
			keysWithNamespace.add(keyWithNamespace);
		}
		List<String> values = this.redisTemplate.opsForValue().multiGet(keysWithNamespace);
		List<T> results = new ArrayList<>(values.size());
		for (String val : values) {
			results.add(mapper.readValue(val, cls));
		}
		return results;
	}

	public <T> T getValueOrNull(String namespace, String key, Class<T> cls) throws Exception {
		String keyWithNamespace = namespace + namespaceSeparator + key;
		String value = this.redisTemplate.opsForValue().get(keyWithNamespace);
		if (value == null) {
			return null;
		}
		return mapper.readValue(value, cls);
	}

	public boolean deleteKey(String namespace, String key) {
		String keyWithNamespace = namespace + namespaceSeparator + key;
		this.redisTemplate.delete(keyWithNamespace);
		return true;
	}

	public void setWithExpiry(String namespace, String key, Object value, long timeout, TimeUnit unit)
			throws Exception {
		String keyWithNamespace = namespace + namespaceSeparator + key;
		String serializedValue = mapper.writeValueAsString(value);
		this.redisTemplate.opsForValue().set(keyWithNamespace, serializedValue, timeout, unit);
	}

	public void clearNamespace(String namespace) {
		this.redisTemplate.delete(this.redisTemplate.keys(namespace + namespaceSeparator + "*"));
	}

}
