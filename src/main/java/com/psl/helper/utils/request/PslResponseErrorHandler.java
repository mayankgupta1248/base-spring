package com.psl.helper.utils.request;

import java.io.IOException;

import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.ResponseErrorHandler;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.psl.helper.exception.PslException;
import com.psl.helper.model.GenericResponse;
import com.psl.helper.utils.PslUtil;

public class PslResponseErrorHandler implements ResponseErrorHandler {

	private ObjectMapper objectMapper = new ObjectMapper();

	public PslResponseErrorHandler() {
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	}

	@Override
	public void handleError(ClientHttpResponse clienthttpresponse) throws IOException {

		GenericResponse<Object> responseDTO = objectMapper.readValue(clienthttpresponse.getBody(),
				GenericResponse.class);

		try {
			PslUtil.parseRequest(responseDTO);
		} catch (Exception e) {
			if (e instanceof RuntimeException) {
				throw (RuntimeException) e;
			} else {
				throw new PslException("Unknown Error Occurred", null, e);
			}
		}
	}

	@Override
	public boolean hasError(ClientHttpResponse clienthttpresponse) throws IOException {
		String statusCode = String.valueOf(clienthttpresponse.getRawStatusCode());
		if (statusCode.startsWith("2") || statusCode.startsWith("3")) {
			return false;

		} else {
			return true;
		}
	}
}
