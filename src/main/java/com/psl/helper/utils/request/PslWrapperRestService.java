package com.psl.helper.utils.request;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.psl.helper.exception.PslException;
import com.psl.helper.utils.PslLoggingRequestInterceptor;

@Service
public class PslWrapperRestService {


    @Autowired
    PslRequest pslRequest;

    private ObjectMapper objectMapper;

    private TypeFactory _typeFactory;

    private int connectionTimeout;

    private int readTimeout;

    private RestTemplate restTemplate = new RestTemplate();

    public PslWrapperRestService(@Value("${request.connectionTimeout : 30000}") int connectionTimeout,
                                @Value("${request.readTimeout : 30000}") int readTimeout) {
        this.connectionTimeout = connectionTimeout;
        this.readTimeout = readTimeout;
        objectMapper =  new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        _typeFactory = objectMapper.getTypeFactory();
        List<ClientHttpRequestInterceptor> interceptors = new ArrayList<>();
        interceptors.add(new PslLoggingRequestInterceptor());
        restTemplate.setInterceptors(interceptors);
        this.restTemplate.setRequestFactory(bufferingClientHttpRequestFactory());
    }

    private BufferingClientHttpRequestFactory bufferingClientHttpRequestFactory() {
        SimpleClientHttpRequestFactory simpleClientHttpRequestFactory = new SimpleClientHttpRequestFactory();
        simpleClientHttpRequestFactory.setConnectTimeout(connectionTimeout);
        simpleClientHttpRequestFactory.setReadTimeout(readTimeout);
        BufferingClientHttpRequestFactory factory = new BufferingClientHttpRequestFactory(simpleClientHttpRequestFactory);
        return factory;
    }

    public <T> T makeHttpRequest(String urlEndPoint, HttpMethod httpMethod, Object requestBody, Class<T> javaType) {
        return request(urlEndPoint, httpMethod, requestBody, null, this._typeFactory.constructType(javaType));
    }

    public <T> T makeHttpRequest(String urlEndPoint, HttpMethod httpMethod, Object requestBody, Class<T> javaType, Map<String, String> headersMap) {
        MultiValueMap<String, String> headersMultiValue = new LinkedMultiValueMap<>();
        for (Map.Entry<String, String> tuple: headersMap.entrySet())
        {
            headersMultiValue.add(tuple.getKey(), tuple.getValue());
        }
        return request(urlEndPoint, httpMethod, requestBody, headersMultiValue, this._typeFactory.constructType(javaType));
    }



    private <T> T request(String url, HttpMethod type, Object obj, MultiValueMap<String, String> headers,
                          JavaType javaType) {
        T responseDTO;
        if (type.equals(HttpMethod.POST)) {
            responseDTO = postObject(url, obj, headers);
        } else {
            throw new PslException("Not supported method.");
        }
        try {
            return parseRequestAfterReturn(responseDTO, javaType);
        } catch (Exception e) {
            throw new PslException("Unknown exception occured.");
        }

    }

    private <T> T postObject(String url, Object obj, MultiValueMap<String, String> headers) {
        PslHttpEntity rt;
        if (headers == null) {
            rt = new PslHttpEntity(obj);
        } else {
            rt = new PslHttpEntity(obj, headers);
        }
        ResponseEntity<T> restResponseDTO = restTemplate.exchange(url, HttpMethod.POST, rt,
                new ParameterizedTypeReference<T>() {
                });
        return restResponseDTO.getBody();
    }

    private <T> T parseRequestAfterReturn(T response, JavaType type) throws Exception {
        StringWriter stringWriter = new StringWriter();
        objectMapper.writeValue(stringWriter, response);
        return objectMapper.readValue(stringWriter.toString(), type);
    }
}
