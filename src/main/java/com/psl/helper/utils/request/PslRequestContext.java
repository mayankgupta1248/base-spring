package com.psl.helper.utils.request;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.psl.models.user.AccessTokenDetails;

public final class PslRequestContext {
	private static ThreadLocal<Map<String, String>> threadLocal = new ThreadLocal<Map<String, String>>() {
		@Override
		protected Map<String, String> initialValue() {
			return new HashMap<>();
		}
	};

	private static ThreadLocal<AccessTokenDetails> accessToken = new ThreadLocal<AccessTokenDetails>() {
		@Override
		protected AccessTokenDetails initialValue() {
			return null;
		}
	};

	public static MultiValueMap<String, String> getRequestContext() {
		Map<String, String> values = threadLocal.get();
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		for (Map.Entry<String, String> entry : values.entrySet()) {
			params.add(entry.getKey(), entry.getValue());
		}
		return params;
	}

	public static String getRequestContextValueByName(String name) {
		return threadLocal.get().get(name);
	}

	public static void setRequestContext(Map<String, String> headers) {
		threadLocal.set(headers);
	}

	public static AccessTokenDetails getAccessTokenDetails() {
		return accessToken.get();
	}

	public static void setAccessToken(AccessTokenDetails accessTokenDetails) {
		accessToken.set(accessTokenDetails);
	}

	public static List<ClientHttpRequestInterceptor> getHeaderRequestInterceptor() {
		List<ClientHttpRequestInterceptor> interceptors = new ArrayList<ClientHttpRequestInterceptor>();
		Map<String, String> headers = threadLocal.get();
		for (Map.Entry<String, String> entry : headers.entrySet()) {
			interceptors.add(new HeaderRequestInterceptor(entry.getKey(), entry.getValue()));
		}
		return interceptors;
	}

	public static void clearRequestContext() {
		threadLocal.remove();
		accessToken.remove();
	}

	private PslRequestContext()
	{
	}
}
