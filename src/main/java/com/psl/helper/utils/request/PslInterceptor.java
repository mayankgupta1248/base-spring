package com.psl.helper.utils.request;

import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.psl.enums.UserRoles;
import com.psl.helper.exception.PslForbidden;
import com.psl.helper.logger.PslLogManager;
import com.psl.helper.logger.PslLogger;
import com.psl.helper.utils.AuthUtility;
import com.psl.helper.utils.Const;
import com.psl.helper.utils.PslUtil;
import com.psl.helper.utils.auth.PslSecured;
import com.psl.models.user.AccessTokenDetails;

@Component
public class PslInterceptor implements HandlerInterceptor {

	@Autowired
	private AuthUtility authUtility;

	private static final PslLogger logger = PslLogManager.getLogger(PslInterceptor.class);

	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		PslUtil._setRequestContext(request);
		
		logger.info("Request URL " + request.getRequestURL().toString());

		String authToken = request.getHeader(Const.authorizationHeaderName);
		String accessToken = request.getHeader(Const.accessTokenName);

		AccessTokenDetails accessTokenDetails;

		if (null != accessToken && !accessToken.isEmpty()) {
			accessTokenDetails = this.authUtility.getAccessTokenFromJwtHmac(accessToken);
		} else {
			// create empty access token details , if security is required by api through
			// @secured then
			// call will not proceed as this object does not carry auth and access details
			accessTokenDetails = new AccessTokenDetails();
		}

		accessTokenDetails.setAuthToken(authToken);

		PslRequestContext.setAccessToken(accessTokenDetails);

		if (handler instanceof HandlerMethod) {
			HandlerMethod method = (HandlerMethod) handler;
			boolean isSecured = method.hasMethodAnnotation(PslSecured.class);
			if (isSecured) {
				if (null == accessTokenDetails.getAuthToken() || accessTokenDetails.getAuthToken().isEmpty()) {
					throw new PslForbidden("Forbidden.");
				} else {
					Set<String> apiRolesSet = new HashSet<>();
					PslSecured annotation = method.getMethodAnnotation(PslSecured.class);
					UserRoles[] apiRoles = annotation.roles();
					if (apiRoles != null) {
						for (UserRoles role : apiRoles) {
							apiRolesSet.add(role.name());
						}
					}
					
					//Check UserInfo Role and its kyc if its not admin
					String userRole = accessTokenDetails.getRole();
					if (UserRoles.USER.name().equalsIgnoreCase(userRole)) {
						//Check access
					}
					else {
						//Check access
					}

					//Check Access required for API
					if (apiRolesSet.isEmpty()) {
						// do nothing as api does not require any special access role
					} else {
						if (apiRolesSet.contains(userRole)) {
							// UserInfo have valid access
						} else {
							throw new PslForbidden("Invalid Access.");
						}
					}
				}
			}
		}

		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		PslRequestContext.clearRequestContext();
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		PslRequestContext.clearRequestContext();
	}
}
