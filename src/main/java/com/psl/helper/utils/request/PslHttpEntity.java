package com.psl.helper.utils.request;

import org.springframework.http.HttpEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

public class PslHttpEntity<T> extends HttpEntity<T> {
	protected PslHttpEntity() {
		this((T) null, (MultiValueMap<String, String>) PslRequestContext.getRequestContext());
	}

	public PslHttpEntity(T body) {
		this(body, (MultiValueMap<String, String>) PslRequestContext.getRequestContext());
	}

	public PslHttpEntity(MultiValueMap<String, String> headers) {
		this((T) null, headers);
	}

	public PslHttpEntity(T body, MultiValueMap<String, String> headers) {
		this(body, addCustomHeaders(headers), true);
	}

	public static MultiValueMap<String, String> addCustomHeaders(MultiValueMap<String, String> customHeaders) {
		MultiValueMap<String, String> tempHeaders = new LinkedMultiValueMap<>();
		tempHeaders.putAll(PslRequestContext.getRequestContext());
		tempHeaders.putAll(customHeaders);
		return tempHeaders;
	}

	public PslHttpEntity(T body, MultiValueMap<String, String> headers, boolean noDefaultHeaders) {
		super(body, headers);
	}
}
