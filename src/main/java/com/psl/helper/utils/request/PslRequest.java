package com.psl.helper.utils.request;

import java.io.StringWriter;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.psl.helper.exception.PslException;
import com.psl.helper.logger.PslLogManager;
import com.psl.helper.logger.PslLogger;
import com.psl.helper.model.GenericResponse;
import com.psl.helper.utils.PslUtil;

@EnableAspectJAutoProxy
@Service
@Configurable
public class PslRequest {
	private static final PslLogger logger = PslLogManager.getLogger(PslRequest.class);
	private static ObjectMapper objectMapper;
	protected TypeFactory _typeFactory;
	RestTemplate restTemplate;

	private int readTimeout;

	private int connectionTimeout;

	public PslRequest(@Value("${request.connectionTimeout:30000}") int connectionTimeout,
			@Value("${request.readTimeout:30000}") int readTimeout) {
		restTemplate = new RestTemplate(clientHttpRequestFactory());
		objectMapper = new ObjectMapper();
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		_typeFactory = objectMapper.getTypeFactory();
		logger.info("Initializing ltrequest..");
		this.readTimeout = readTimeout;
		this.connectionTimeout = connectionTimeout;
		restTemplate.setErrorHandler(new PslResponseErrorHandler());
		restTemplate.setInterceptors(PslRequestContext.getHeaderRequestInterceptor());
	}

	private ClientHttpRequestFactory clientHttpRequestFactory() {
		SimpleClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory();
		factory.setReadTimeout(readTimeout);
		factory.setConnectTimeout(connectionTimeout);
		factory.setOutputStreaming(false);
		return factory;
	}

	public <T> T request(String url, HttpMethod type, Object obj, Class<T> javaType) {
		return this.request(url, type, obj, null, this._typeFactory.constructType(javaType));
	}

	public <T> T request(String url, HttpMethod type, Object obj, MultiValueMap<String, String> headers,
			Class<T> javaType) {
		return this.request(url, type, obj, headers, this._typeFactory.constructType(javaType));
	}

	public <T> T request(String url, HttpMethod type, Class<T> javaType) {
		return this.request(url, type, null, null, this._typeFactory.constructType(javaType));
	}

	public <T> T request(String url, HttpMethod type, MultiValueMap<String, String> headers, Class<T> javaType) {
		return this.request(url, type, null, headers, this._typeFactory.constructType(javaType));
	}

	public <T> T request(String url, HttpMethod type, JavaType javaType) {
		return this.request(url, type, null, null, javaType);
	}

	public <T> T request(String url, HttpMethod type, Object obj, JavaType javaType) {
		return this.request(url, type, obj, null, javaType);
	}

	private <T> T request(String url, HttpMethod type, Object obj, MultiValueMap<String, String> headers,
			JavaType javaType) {
		GenericResponse<T> genericResponse;
		if (type.equals(HttpMethod.GET)) {
			genericResponse = getObject(url, headers);
		} else if (type.equals(HttpMethod.POST)) {
			genericResponse = postObject(url, obj, headers);
		} else if (type.equals(HttpMethod.PUT)) {
			genericResponse = putObject(url, obj, headers);
		} else if (type.equals(HttpMethod.PATCH)) {
			genericResponse = patchObject(url, obj, headers);
		} else if (type.equals(HttpMethod.DELETE)) {
			genericResponse = deleteObject(url, obj, headers);
		} else {
			throw new PslException("Not supported method.");
		}
		try {
			return parseRequestAfterReturn(genericResponse, javaType);
		} catch (Exception e) {
			throw new PslException("Unknown Error Occurred", null, e);
		}

	}

	private <T> GenericResponse<T> getObject(String url, MultiValueMap<String, String> headers) {
		PslHttpEntity<T> rt = null;
		if (headers == null) {
			rt = new PslHttpEntity<>();
		} else {
			rt = new PslHttpEntity<>(headers);
		}
		ResponseEntity<GenericResponse<T>> restResponseDTO = restTemplate.exchange(url, HttpMethod.GET, rt,
				new ParameterizedTypeReference<GenericResponse<T>>() {
				});
		return restResponseDTO.getBody();
	}

	private <T> GenericResponse<T> postObject(String url, Object obj, MultiValueMap<String, String> headers) {
		PslHttpEntity rt = null;
		if (headers == null) {
			rt = new PslHttpEntity(obj);
		} else {
			rt = new PslHttpEntity(obj, headers);
		}
		ResponseEntity<GenericResponse<T>> restResponseDTO = restTemplate.exchange(url, HttpMethod.POST, rt,
				new ParameterizedTypeReference<GenericResponse<T>>() {
				});
		return restResponseDTO.getBody();
	}

	private <T> GenericResponse<T> putObject(String url, Object obj, MultiValueMap<String, String> headers) {
		PslHttpEntity rt = null;
		if (headers == null) {
			rt = new PslHttpEntity(obj);
		} else {
			rt = new PslHttpEntity(obj, headers);
		}
		ResponseEntity<GenericResponse<T>> response = restTemplate.exchange(url, HttpMethod.PUT, rt,
				new ParameterizedTypeReference<GenericResponse<T>>() {
				});
		return response.getBody();
	}

	private <T> GenericResponse<T> deleteObject(String url, Object obj, MultiValueMap<String, String> headers) {
		PslHttpEntity rt = null;
		if (headers == null) {
			rt = new PslHttpEntity(obj);
		} else {
			rt = new PslHttpEntity(obj, headers);
		}
		ResponseEntity<GenericResponse<T>> restResponseDTO = restTemplate.exchange(url, HttpMethod.DELETE, rt,
				new ParameterizedTypeReference<GenericResponse<T>>() {
				});
		return restResponseDTO.getBody();
	}

	private <T> GenericResponse<T> patchObject(String url, Object obj, MultiValueMap<String, String> headers) {
		PslHttpEntity rt = null;
		if (headers == null) {
			rt = new PslHttpEntity(obj);
		} else {
			rt = new PslHttpEntity(obj, headers);
		}
		ResponseEntity<GenericResponse<T>> restResponseDTO = restTemplate.exchange(url, HttpMethod.PATCH, rt,
				new ParameterizedTypeReference<GenericResponse<T>>() {
				});
		return restResponseDTO.getBody();
	}

	private <T> T parseRequestAfterReturn(GenericResponse<T> genericResponse, JavaType type) throws Exception {
		PslUtil.parseRequest(genericResponse);
		T rt = genericResponse.getResult();
		StringWriter stringWriter = new StringWriter();
		objectMapper.writeValue(stringWriter, rt);
		return objectMapper.readValue(stringWriter.toString(), type);
	}
}
