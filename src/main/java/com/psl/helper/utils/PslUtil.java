package com.psl.helper.utils;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.UUID;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.psl.helper.exception.PslException;
import com.psl.helper.exception.PslUnprocessableEntity;
import com.psl.helper.items.PslExceptionType;
import com.psl.helper.logger.PslLogManager;
import com.psl.helper.logger.PslLogger;
import com.psl.helper.model.GenericError;
import com.psl.helper.model.GenericResponse;
import com.psl.helper.utils.request.PslRequestContext;

public class PslUtil {

	private static final PslLogger logger = PslLogManager.getLogger(PslUtil.class);

	public static String[] getValuesFromEnum(Class<? extends Enum<?>> e) {
		return Arrays.toString(e.getEnumConstants()).replaceAll("^.|.$", "").split(", ");
	}

	public static String getExcludeFields(String providedFields, Class<?> clz) {
		Field[] fields = clz.getDeclaredFields();
		String[] items = providedFields.split(",");
		List<String> excludedFields = new ArrayList<>();
		for (Field field : fields) {
			if (!Arrays.asList(items).contains(field.getName())) {
				excludedFields.add(field.getName());
			}
		}
		String output = String.join(",", excludedFields);
		return output;

	}

	public static Map<String, Integer> paginationDetails(int pageNo, int pageSize) {
		Integer startNo = (pageNo - 1) * pageSize;
		Integer endNo = (pageNo) * pageSize;
		Map<String, Integer> output = new HashMap<String, Integer>();
		output.put("startNo", startNo);
		output.put("endNo", endNo);
		output.put("maxResult", pageSize);
		return output;

	}

	public static String msgForException(String e, String deafultMsg) {
		if (e.equals("org.springframework.http.converter.HttpMessageNotReadableException")) {
			return "Message body is not readable.";
		} else if (e.equals("com.fasterxml.jackson.core.JsonParseException")) {
			return "Not able to parse json.";
		} else if (e.equals("org.springframework.web.HttpMediaTypeNotSupportedException")) {
			return "Media type not supported.";
		} else if (e.equals("org.springframework.dao.DataIntegrityViolationException")) {
			return "Data integrity voilation exception occured.";
		} else if (e.equals("org.springframework.web.bind.MethodArgumentNotValidException")) {
			return "Validation failed for provided fields";
		} else if (e.equals("org.hibernate.id.IdentifierGenerationException")) {
			return "Entity id not provided.";
		} else if (e.equals("org.springframework.web.bind.MissingServletRequestParameterException")) {
			return "Request parameters are missing.";
		} else if (e.equals("com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException")) {
			return "Same primary key already exist";
		}
		return deafultMsg;
	}

	public static String generateUniqueId() {
		UUID uniqueKey = UUID.randomUUID();
		return uniqueKey.toString();
	}

	public static boolean isNullOrBlank(String key) {
		int strLen;
		if (key == null || (strLen = key.length()) == 0) {
			return true;
		}
		for (int i = 0; i < strLen; i++) {
			if (Character.isWhitespace(key.charAt(i)) == false) {
				return false;
			}
		}
		return true;
	}

	public static String objectToJson(Object object) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			String jsonToString = mapper.writeValueAsString(object);
			return jsonToString;
		} catch (Exception e) {
			logger.error("exception", e);
			return null;
		}
	}

	public static <T> List<T> jsonToObjectList(String json, Class<?> clz) {
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			return objectMapper.readValue(json, TypeFactory.defaultInstance().constructCollectionType(List.class, clz));
		} catch (Exception e) {
			logger.error("exception in object to json", e);
			return null;
		}
	}

	public static <Any> Any jsonToObject(String json, Class<?> clz) {

		try {
			ObjectMapper objectMapper = new ObjectMapper();
			objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			Any output = (Any) objectMapper.readValue(json, clz);
			return output;
		} catch (Exception e) {
			logger.error("exception in json to object", e);
			return null;
		}

	}

	public static boolean isBetween(int a, int b, int c) {
		return b > a ? c > a && c < b : c > b && c < a;
	}

	public static void parseRequest(GenericResponse<?> genericResponse) throws Exception {
		if (PslUtil.isBetween(199, 300, genericResponse.getStatus())) {

		} else if (genericResponse.getStatus() >= 300 && genericResponse.getError() != null) {
			generateExceptionFromError(genericResponse.getError());
		} else {
			logger.error("Exception in pslRequest " + PslUtil.objectToJson(genericResponse));
			throw new PslException("Unknown exception occured. " + PslUtil.objectToJson(genericResponse));
		}
	}

	public static void generateExceptionFromError(GenericError error) throws Exception {
		PslExceptionType errorType = error.getCode();
		RuntimeException ex = null;
		String className = "com.psl.helper.exception." + errorType.name();
		Class<?> t = Class.forName(className);
		Constructor<?> c = t.getConstructor(String.class, String.class, Throwable.class);
		Object o = c.newInstance(error.getMessage(), error.getDeveloperCode(), null);
		if (o instanceof RuntimeException) {
			ex = (RuntimeException) o;
			throw ex;
		} else {
			throw new PslException("Invalid error domain");
		}

	}

	public static void _setRequestContext(HttpServletRequest request) {
		Enumeration<String> headerNames = request.getHeaderNames();
		Map<String, String> headers = new HashMap<String, String>();
		if (headerNames != null) {
			while (headerNames.hasMoreElements()) {
				String headerName = headerNames.nextElement();
				String headerValue = request.getHeader(headerName);
				headers.put(headerName, headerValue);
			}
			if (headers.get(Const.xRequestId) == null) {
				String UUID = PslUtil.generateUniqueId();
				headers.put(Const.xRequestId, UUID);
			}
			PslRequestContext.setRequestContext(headers);
		}
	}

	public static String filterURL(String uri) {
		String filteredUri = uri;
		int a = uri.indexOf("?");
		if (a > -1) {
			filteredUri = uri.substring(0, uri.indexOf("?"));
		}

		String finalURL = "";
		int len = filteredUri.length();
		char prev = 0, curr = 0;
		for (int i = 0; i < len; i++) {
			curr = filteredUri.charAt(i);
			if (curr == prev && curr == '/') {
			} else {
				finalURL = finalURL + curr;
			}
			prev = curr;
		}

		int length = finalURL.length();
		if (finalURL.charAt(length - 1) == '/') {
			finalURL = finalURL.substring(0, length - 1);
		}

		return finalURL;
	}

	public static String buildUrlWithQueryParams(String endpoint, Map<String, String> queryParams) {
		UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder.fromHttpUrl(endpoint);
		for (Map.Entry<String, String> tuple : queryParams.entrySet()) {
			uriComponentsBuilder.queryParam(tuple.getKey(), tuple.getValue());
		}
		return uriComponentsBuilder.build().encode().toUri().toString();
	}

	public static Class<?> getClassForName(String className) {
		try {
			Class<?> result = Class.forName(className);
			return result;
		} catch (ClassNotFoundException e) {
			logger.error("Class was tried to be retrieved with name - " + className, e);
		}
		return null;
	}

	public static String encrypt(String plainText, String key) throws Exception {
		Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
		SecretKeySpec secretKey = new SecretKeySpec(key.getBytes("UTF-8"), "AES");
		cipher.init(Cipher.ENCRYPT_MODE, secretKey);
		return Base64.encodeBase64URLSafeString(cipher.doFinal(plainText.getBytes("UTF-8")));
	}

	public static String decrypt(String encryptedValue, String key) throws Exception {
		Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
		SecretKeySpec secretKey = new SecretKeySpec(key.getBytes("UTF-8"), "AES");
		cipher.init(Cipher.DECRYPT_MODE, secretKey);
		return new String(cipher.doFinal(Base64.decodeBase64(encryptedValue)));
	}

	public static Long getPastEpochMillis(long currentEpoch, int days) {
		return currentEpoch - days * 24 * 60 * 60 * 1000;
	}

	public static Long getFutureEpochMillis(long currentEpoch, int days) {
		return currentEpoch + days * 24 * 60 * 60 * 1000;
	}

	public static String generateOTP() {
		int randomPin = (int) (Math.random() * 900000) + 100000;
		return String.valueOf(randomPin);
	}

	public static int getNumberOfDecimalPlaces(BigDecimal bigDecimal) {
		String string = bigDecimal.stripTrailingZeros().toPlainString();
		int index = string.indexOf(".");
		return index < 0 ? 0 : string.length() - index - 1;
	}

	public static BigDecimal increaseAmountByPrecent(BigDecimal price, Integer percent) {
		return price.add(price.multiply(new BigDecimal(percent)).divide(new BigDecimal("100")));
	}

	public static BigDecimal decreaseAmountByPrecent(BigDecimal price, Integer percent) {
		return price.subtract(price.multiply(new BigDecimal(percent)).divide(new BigDecimal("100")));
	}

	public static String getIndianTimeZoneDateFromEpoch(Long epoch) {
		Date date = new Date(epoch);
		DateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		format.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
		return format.format(date);
	}

	public static String generateUrlUsingParams(String url, Map<String, Object> paramMap) {
		UriComponentsBuilder uri = UriComponentsBuilder.fromHttpUrl(url);
		for (Map.Entry<String, Object> entry : paramMap.entrySet()) {
			uri.queryParam(entry.getKey(), entry.getValue());
		}
		return uri.build().encode().toUri().toString();
	}

	public static String generateUrlUsingPathVariable(String url, Map<String, Object> variableMap) {
		UriComponentsBuilder uri = UriComponentsBuilder.fromHttpUrl(url);
		return uri.buildAndExpand(variableMap).encode().toUri().toString();
	}

	public static BigDecimal calculatePercentageChange(BigDecimal todayPrice, BigDecimal oldPrice) {
		if (todayPrice == null || oldPrice == null || oldPrice.compareTo(new BigDecimal("0")) == 0) {
			return new BigDecimal("0");
		} else {
			return (todayPrice.subtract(oldPrice)).multiply(new BigDecimal("100")).divide(oldPrice, 2,
					RoundingMode.HALF_UP);
		}
	}

	public static BigDecimal calculateChange(BigDecimal todayPrice, BigDecimal oldPrice) {
		if (todayPrice == null || oldPrice == null) {
			return new BigDecimal("0");
		} else {
			return todayPrice.subtract(oldPrice);
		}
	}

	public static String hideHalfEmail(String email) {
		if (email == null) {
			return null;
		}
		int len = email.length();
		int index = email.lastIndexOf('@');
		if (index == -1) {
			throw new PslUnprocessableEntity("@ is missing in given email");
		}
		String prefix = email.substring(0, index);
		String suffix = email.substring(index + 1, len);
		String secondHalf = prefix.substring(prefix.length() / 2, prefix.length());
		String finalPrefix = StringUtils.leftPad(secondHalf, prefix.length(), "*");

		return finalPrefix + "@" + suffix;
	}
	
	public static String hideEmailExceptFirstAndLastLetter(String email) {
		if (email == null) {
			return null;
		}
		int len = email.length();
		int index = email.lastIndexOf('@');
		if (index == -1) {
			throw new PslUnprocessableEntity("@ is missing in given email");
		}
		String prefix = email.substring(0, index);
		String suffix = email.substring(index + 1, len);
		int prefixLength = prefix.length();
		
		StringBuilder finalPrefix = new StringBuilder();
		
		for(int i = 0 ; i < prefixLength; i++) {
			if(i == 0 || i == prefixLength -1) {
				finalPrefix.append(prefix.charAt(i));
			}
			else {
				finalPrefix.append("*");
			}
		}

		return finalPrefix.append("@" + suffix).toString();
	}
}
