package com.psl.helper.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.stereotype.Component;

@Component
public class PslContextDestroyListner implements ApplicationListener<ContextClosedEvent> {

    @Autowired
    PslDestroyListenerService pslDestroyListenerService;

    @Override
    public void onApplicationEvent(ContextClosedEvent contextClosedEvent) {
        for (DestroyListener destroyListener :
                pslDestroyListenerService.getDestroyListeners())
        {
            destroyListener.onDestroy();
        }
    }
}
