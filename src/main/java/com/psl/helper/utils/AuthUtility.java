package com.psl.helper.utils;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.jwt.Jwt;
import org.springframework.security.jwt.JwtHelper;
import org.springframework.security.jwt.crypto.sign.MacSigner;
import org.springframework.security.jwt.crypto.sign.SignerVerifier;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.psl.EnvironmentService;
import com.psl.models.user.AccessTokenDetails;

@Service
public class AuthUtility {

	@Autowired
	private EnvironmentService environmentServicel;

	private SignerVerifier signer;
	private ObjectMapper objectMapper;

	@PostConstruct
	public void afterInit() {
		signer = new MacSigner(environmentServicel.getAccessTokenHmacKey());
		objectMapper = new ObjectMapper();
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	}

	public String getHmacAccessToken(AccessTokenDetails accessToken) throws JsonProcessingException {
		Jwt jwt = JwtHelper.encode(objectMapper.writeValueAsString(accessToken), signer);
		return jwt.getEncoded();
	}

	public AccessTokenDetails getAccessTokenFromJwtHmac(String token) throws Exception {
		Jwt jwt = JwtHelper.decodeAndVerify(token, signer);
		return objectMapper.readValue(jwt.getClaims(), AccessTokenDetails.class);
	}
}
