package com.psl.helper.utils;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.springframework.stereotype.Component;

@Component
public class PslExecutorService {

    private ExecutorService sqsExecutorService;

    /**
     * No-args constructor.
     */
    PslExecutorService()
    {
        if(sqsExecutorService == null) {
            sqsExecutorService = Executors.newFixedThreadPool(10);
        }
    }

    /**
     * Returns instance of SQS Executor Service
     * @return ExecutorService
     */
    public synchronized ExecutorService getSQSExecutorService() {
        return sqsExecutorService;
    }

    //ToDO :Make this bean singleton
}
