package com.psl.helper.utils;

public class Const {
	public static final String xRequestId = "x-request-id";
	public static final String CONTENT_TYPE = "content-type";

	// Error message section
	public static final String RESOURCE_NOT_FOUND = "resource for %s with id %s does not exists";
	public static final String CONSTRAINT_VIOLATION = "unable to create resource. constraint voilation exception for %s.";
	public static final String HIBERNATE_EXCEPTION = "Hibernate Exception occurred with cause %s";

	// Pool size of executors
	public static final int PSL_NOTIFICATION_POOL_SIZE = (System.getenv("PSL_NOTIFICATION_POOL_SIZE") == null) ? 5
			: Integer.valueOf(System.getenv("PSL_NOTIFICATION_POOL_SIZE"));

	// PSL Util Executor
	public static final int PSL_EXECUTOR_POOL_SIZE = (System.getenv("PSL_EXECUTOR_POOL_SIZE") == null) ? 10
			: Integer.valueOf(System.getenv("PSL_EXECUTOR_POOL_SIZE"));

	public static final int PSL_SQS_PUBLISHER_EXECUTOR_SIZE = (System.getenv("PSL_PUBLISHER_EXECUTOR_SIZE") == null) ? 10
			: Integer.valueOf(System.getenv("PSL_PUBLISHER_EXECUTOR_SIZE"));
	
	public static final int PSL_AFTER_MATCHING_EXECUTOR_SIZE = (System.getenv("PSL_AFTER_MATCHING_EXECUTOR_SIZE") == null) ? 15
			: Integer.valueOf(System.getenv("PSL_PUBLISHER_EXECUTOR_SIZE"));

	public static final int TL_MESSAGE_ERROR_EXECUTOR_POOL_SIZE = (System
			.getenv("TL_MESSAGE_ERROR_EXECUTOR_POOL_SIZE") == null) ? 2
					: Integer.valueOf(System.getenv("TL_MESSAGE_ERROR_EXECUTOR_POOL_SIZE"));
	
	public static final int EXECUTOR_AWAIT_TERMINATION_TIMEOUT = 90;

	// Email Sender
	public static final String COMPANY_NAME = "PSL";
	
	// Email Addresses
	public static final String NO_REPLY_EMAIL = "no-reply@PSL.com";

	// Email Subjects
	public static final String EMAIL_INVITE_SUBJECT = "Welcome to PSL.";
	public static final String CONTACTUS_SUBJECT = "New Query Received";
	public static final String EMAIL_VERIFICATION_SUBJECT = " Email Verification";
	public static final String EMAIL_FORGOT_PASSWORD_SUBJECT = "Forgot Password OTP";
	public static final String EMAIL_CHANGE_PASSWORD_SUBJECT = "Change Password OTP";
	public static final String LOGIN_OTP_SUBJECT = "Login OTP";
	public static final String NEW_LOGIN_SUBJECT = "New Successful Login";
	public static final String WITHDRAW_SUBJECT = "Withdrawal Request OTP";
	public static final String DEPOSIT_SUCCESSFUL = "Welcome to PSL.";
	public static final String CHANGE_2FA_SUBJECT = "Change 2FA OTP";

	// SMS/EMAIL Messages
	public static final String CHANGE_2FA_MSG = "Your 2FA change OTP is {0}. It will expire in {1} minutes.";
	public static final String FORGOT_PASS_MSG = "Your forgot password OTP is {0}. It will expire in {1} minutes.";
	public static final String CHANGE_PASS_MSG = "Your change password OTP is {0}. It will expire in {1} minutes.";
	public static final String VERIFICATION_OTP_MSG = "Your {0} verification OTP is {1}. It will expire in {2} minutes.";
	public static final String LOGIN_OTP_MSG = "Your login OTP is {0}. It will expire in {1} minutes. This is requested from {2} at {3} via {4}. Your ISP location is {5}. If this was not you then please change your password immediately.";
	public static final String REGISTRATION_SUCCESS_MSG = "Thank You for becoming part of PSL Exchange. Please login and start trading. ";
	public static final String LOGIN_SUCCESS_EMAIL_MSG = "There is a new sign in for your PSL Account at {0}. If this was not you then please change your password immediately.";
	public static final String WITHDRAW_OTP_MESSAGE = "Your OTP for initiating withdrawal of {0} {1} is {2}.";

	public static final String authorizationHeaderName = "X-Authorization";
	public static final String accessTokenName = "X-Access-Token";

	public static final String REDIS_NAMESPACE = "PSL";
	public static final String REDIS_MARKET_DATA_KEY = "MARKET_DATA";
	public static final String REDIS_LEADERBOARD_KEY = "LEADERBOARD_";
	public static final String REDIS_TRADING_LEADERBOARD_VOLUME_KEY = "TRADING_LEADERBOARD_VOLUME_";
	public static final long OTP_EXPIRY = 5; // In Minutes
	public static final long EMAIL_OTP_EXPIRY = 5; // In Minutes

	
	public static final long MAX_IMAGE_SIZE = 3145728; // In Bytes
	
	public static final String PSL_SQS_PUBLISHER_EXECUTOR_NAME = "sqsPublisherExecutor";
	public static final String PSL_AFTER_MATCHING_PUBLISHER_EXECUTOR_NAME = "afterMatchingExecutor";
	public static final String PSL_NOTIFICATION_PUBLISHER_EXECUTOR_NAME = "notificationExecutor";
	
	
}
