package com.psl.helper.utils;

import ma.glasnost.orika.impl.DefaultMapperFactory;

public class PslMapperFactory extends DefaultMapperFactory {

    public PslMapperFactory(MapperFactoryBuilder<?, ?> builder) {
        super(builder);
    }
}
