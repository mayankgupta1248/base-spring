package com.psl.helper.utils.validators;

import java.util.Arrays;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.psl.helper.utils.validators.constraint.In;

public class InConstraintValidator implements ConstraintValidator<In, String>
{

    private String [] values;

    public final void initialize(final In annotation)
    {
        values = annotation.values();
    }

    public final boolean isValid(final String value, final ConstraintValidatorContext context)
    {
        if (value == null)
        {
            return false;
        }
        return Arrays.asList(values).contains(value);  // check if value is in this.values
    }

}
