package com.psl.helper.utils.validators;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Date;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.psl.helper.exception.PslUnprocessableEntity;
import com.psl.helper.utils.validators.constraint.Dob;

public class DobValidator implements ConstraintValidator<Dob, String> {

	@Override
	public void initialize(Dob dob) {

	}

	@Override
	public boolean isValid(String dob, ConstraintValidatorContext context) {
		Date currentDate = new Date();
		LocalDate currentLocale = currentDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		Date dobDate = null;
		try {
			dobDate = new SimpleDateFormat("dd/MM/yyyy").parse(dob);
		} catch (ParseException e) {
			throw new PslUnprocessableEntity("Invalid date formal");
		}
		LocalDate birthLocale = dobDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

		Period period = Period.between(birthLocale, currentLocale);
		int year = period.getYears();
		return year >= 18;
	}
}
