package com.psl.helper.utils.validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.psl.helper.utils.validators.constraint.Time;

public class TimeConstraintValidator implements ConstraintValidator<Time, String>
{
    private static final String TIME24HOURS_PATTERN =
            "([01]?[0-9]|2[0-3]):[0-5][0-9]";

    public final void initialize(final Time annotation)
    {
    }

    public final boolean isValid(final String value, final ConstraintValidatorContext context)
    {
        if(value==null){
            return false;
        }
        Pattern p = Pattern.compile(TIME24HOURS_PATTERN);
        Matcher m = p.matcher(value);
        return m.matches();  // check if value is in this.values
    }

}