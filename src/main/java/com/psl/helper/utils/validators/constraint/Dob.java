package com.psl.helper.utils.validators.constraint;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.psl.helper.utils.validators.DobValidator;

@Documented
@Constraint(validatedBy = DobValidator.class)
@Target({ TYPE, FIELD, ANNOTATION_TYPE })
@Retention(RUNTIME)
public @interface Dob {

    String message() default "Invalid DOB";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
