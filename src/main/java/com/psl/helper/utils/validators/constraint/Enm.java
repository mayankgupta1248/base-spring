package com.psl.helper.utils.validators.constraint;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.psl.helper.utils.validators.EnmConstraintValidator;

@java.lang.annotation.Target({ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE, ElementType.CONSTRUCTOR, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = EnmConstraintValidator.class)
public @interface Enm {
    String message() default "Provided value is not allowed";
    
    boolean allowNull() default false;

    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default {};

    Class enm();
}
