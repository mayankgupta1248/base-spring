package com.psl.helper.utils.validators;

import java.util.Arrays;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.psl.helper.utils.PslUtil;
import com.psl.helper.utils.validators.constraint.Enm;

public class EnmConstraintValidator implements ConstraintValidator<Enm, String> {

	private Class enm;
	private boolean allowNull;

	public final void initialize(final Enm annotation) {
		enm = annotation.enm();
		allowNull = annotation.allowNull();
	}

	public final boolean isValid(final String value, final ConstraintValidatorContext context) {
		if (allowNull && value == null) {
			return true;
		}

		if (!allowNull && value == null) {
			return false;
		}
		String[] valuesOfEnum = PslUtil.getValuesFromEnum(enm);
		return Arrays.asList(valuesOfEnum).contains(value); // check if value is
															// in this.values
	}
}
