package com.psl.helper.utils.validators;

import java.math.BigDecimal;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.psl.helper.utils.validators.constraint.Positive;

public class PositiveConstraintValidator implements ConstraintValidator<Positive, Object> {
	public final void initialize(final Positive annotation) {
	}

	public final boolean isValid(final Object number, final ConstraintValidatorContext context) {
		if (number == null) {
			return false;
		} else if (number instanceof Integer) {
			Integer num = (Integer) number;
			return (num > 0);
		} else if (number instanceof Double) {
			Double num = (Double) number;
			return (num > 0d);
		} else if (number instanceof Long) {
			Long num = (Long) number;
			return (num > 0L);
		} else if (number instanceof Float) {
			Float num = (Float) number;
			return (num > 0F);
		} else if (number instanceof BigDecimal) {
			BigDecimal num = (BigDecimal) number;
			return num.compareTo(BigDecimal.ZERO) > 0;
		}

		return false;
	}
}
