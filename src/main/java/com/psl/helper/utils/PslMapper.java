package com.psl.helper.utils;

import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.psl.helper.exception.PslDomainDTOEngineException;
import com.psl.helper.exception.PslException;
import com.psl.helper.logger.PslLogManager;
import com.psl.helper.logger.PslLogger;
import com.psl.helper.model.MappingModel;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import ma.glasnost.orika.metadata.ClassMapBuilder;

public class PslMapper {

	private MapperFactory mapperFactory = new DefaultMapperFactory.Builder().build();
	private MapperFacade mapperEngine;
	private static final PslLogger logger = PslLogManager.getLogger(PslMapper.class);

	public PslMapper() {
		try {
			logger.info("Initializing the PslMapper......");
			InputStreamReader dataReader;
			ObjectMapper mapper = new ObjectMapper();
			logger.info("reading mapping.config gile from classpath..");
			URL url = getClass().getResource("/mapping.config.json");
			if (url != null) {
				logger.info("mapping.config found in classpath.");
				dataReader = new InputStreamReader(url.openStream());
				ArrayList<MappingModel> mappingConfigs = mapper.readValue(dataReader,
						TypeFactory.defaultInstance().constructCollectionType(List.class, MappingModel.class));
				logger.info("setting the mapping configs to " + mappingConfigs.toString());
				for (MappingModel model : mappingConfigs) {
					ClassMapBuilder classMapBuilder = mapperFactory.classMap(Class.forName(model.getBaseClass()),
							Class.forName(model.getDestinationClass()));
					classMapBuilder.mapNulls(model.isMapNulls());
					classMapBuilder.mapNullsInReverse(model.isMapNullsReverse());
					for (Map.Entry<String, String> entry : model.getFields().entrySet()) {
						classMapBuilder.field(entry.getKey(), entry.getValue());
					}
					for (String excludeField : model.getExcludeFields()) {
						classMapBuilder.exclude(excludeField);
					}
					if (model.isDefaultMap()) {
						classMapBuilder.byDefault();
					}
					classMapBuilder.register();
				}
			} else {
				logger.info("No mapping file found. Mappings will be done by default strategy.");
			}
			this.mapperEngine = mapperFactory.getMapperFacade();
		} catch (Exception e) {
			logger.error("Exception : ", e);
			throw new PslDomainDTOEngineException(e.getMessage());
		}
	}

	public <Any> Any convertModel(Object object, Class clz) {
		Any output = (Any) mapperEngine.map(object, clz);
		return output;
	}

	public <Any, AnyOutput> AnyOutput convertModelList(List<Any> object, Class clz) {
		AnyOutput output = (AnyOutput) mapperEngine.mapAsList(object, clz);
		return output;
	}

	public void mergeModel(Object source, Object destination) {
		mapperEngine.map(source, destination);
	}

    public <Any, AnyOutput> Collection<AnyOutput> convertModelCollection(Iterable<Any> source, Class collectionClass, Class collectionElementClass)
    {
		Collection<AnyOutput> collection = null;
		try {
			collection = (Collection)collectionClass.newInstance();
		}
		catch (Exception e)
		{
			throw new PslException("Unable to convert collection");
		}
		mapperEngine.mapAsCollection(source, collection, collectionElementClass);
		return collection;
    }

	public <Any, AnyOutput> void convertModelCollection(Iterable<Any> source, Collection<AnyOutput> destination, Class collectionElementClass)
	{
		mapperEngine.mapAsCollection(source, destination, collectionElementClass);
	}
}
