package com.psl.helper.utils;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class PslDestroyListenerService {

    List<DestroyListener> destroyListeners;

    /**
     * No-args constructor.
     */
    PslDestroyListenerService()
    {
        this.destroyListeners = new ArrayList<>();
    }

    /**
     *
     * @param destroyListener
     */
    public void addDestroyListener(DestroyListener destroyListener)
    {
        this.destroyListeners.add(destroyListener);
    }

    /**
     * Returns a List of all DestroyListeners
     * @return List<DestroyListener>
     */
    public List<DestroyListener> getDestroyListeners()
    {
        return this.destroyListeners;
    }
}
