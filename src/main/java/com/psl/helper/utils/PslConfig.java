package com.psl.helper.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.psl.helper.utils.request.PslInterceptor;

@Configuration
public class PslConfig extends WebMvcConfigurerAdapter {

    @Autowired
    private PslInterceptor pslInterceptor;
    
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(pslInterceptor);
    }
}
