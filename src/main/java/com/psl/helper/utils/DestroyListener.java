package com.psl.helper.utils;

public abstract class DestroyListener {

    public abstract void onDestroy();
}
