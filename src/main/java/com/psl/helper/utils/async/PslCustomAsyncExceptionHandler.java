package com.psl.helper.utils.async;

import java.lang.reflect.Method;

import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;

import com.bugsnag.Bugsnag;
import com.psl.helper.logger.PslLogManager;
import com.psl.helper.logger.PslLogger;
import com.psl.helper.utils.AppContextManager;

public class PslCustomAsyncExceptionHandler implements AsyncUncaughtExceptionHandler {
	private final PslLogger logger = PslLogManager.getLogger(this.getClass());

	@Override
	public void handleUncaughtException(Throwable throwable, Method method, Object... obj) {
		Bugsnag bugsnag = AppContextManager.getBean(Bugsnag.class);
		logger.error("Async exception", throwable);
		bugsnag.notify(throwable);
	}
}