package com.psl.helper.utils.async;

import java.util.concurrent.Executor;

import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import com.psl.helper.utils.Const;


@Configuration
public class PslAsyncConfigurer implements AsyncConfigurer {

    @Override
    public Executor getAsyncExecutor() {
        ThreadPoolTaskExecutor ltExecuter =  new ThreadPoolTaskExecutor();
        ltExecuter.setCorePoolSize(Const.PSL_EXECUTOR_POOL_SIZE);
        ltExecuter.setMaxPoolSize(Const.PSL_EXECUTOR_POOL_SIZE * 2);
        ltExecuter.setThreadNamePrefix("ltTaskExecuter-");
        ltExecuter.setWaitForTasksToCompleteOnShutdown(true);
        ltExecuter.setAwaitTerminationSeconds(Const.EXECUTOR_AWAIT_TERMINATION_TIMEOUT);
        ltExecuter.initialize();
        return ltExecuter;
    }

    @Override
    public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
        return new PslCustomAsyncExceptionHandler();
    }
}
