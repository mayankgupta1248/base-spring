package com.psl.helper;

import java.io.IOException;
import java.time.Instant;
import java.util.HashMap;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClientBuilder;
import com.amazonaws.services.simpleemail.model.Body;
import com.amazonaws.services.simpleemail.model.Content;
import com.amazonaws.services.simpleemail.model.Destination;
import com.amazonaws.services.simpleemail.model.Message;
import com.amazonaws.services.simpleemail.model.SendEmailRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.psl.EnvironmentService;
import com.psl.helper.logger.PslLogManager;
import com.psl.helper.logger.PslLogger;
import com.psl.helper.utils.Const;
import com.psl.helper.utils.PslUtil;

@Service
public class EmailAndSmsService {

	@Autowired
	private EnvironmentService environmentService;

	@Autowired
	private TemplateEngine emailTemplateEngine;

	private AmazonSimpleEmailService sesClient = null;

	private RestTemplate restTemplate = new RestTemplate();

	private ObjectMapper objectMapper = new ObjectMapper();
	
	public static final String SQS_FIFO_REGION = "eu-west-1";

	private static PslLogger logger = PslLogManager.getLogger(EmailAndSmsService.class);

	@PostConstruct
	public void createClient() {
		AWSCredentials awsCredentials = new BasicAWSCredentials(environmentService.getAwsAccessKey(),
				environmentService.getAwsSecretKey());
		AmazonSimpleEmailService client = AmazonSimpleEmailServiceClientBuilder.standard()
				.withRegion(SQS_FIFO_REGION)
				.withCredentials(new AWSStaticCredentialsProvider(awsCredentials)).build();
		this.sesClient = client;
	}

	@Async(Const.PSL_NOTIFICATION_PUBLISHER_EXECUTOR_NAME)
	public void sendSms(String message, String mobile) throws IOException {
		try {
			HashMap<String, String> params = new HashMap<>();
			params.put("authkey", environmentService.getMsg91AuthKey());
			params.put("mobiles", mobile);
			params.put("message", message);
			params.put("sender", environmentService.getMsg91SenderId());
			params.put("route", "4");
			params.put("response", "json");
			String uri = PslUtil.buildUrlWithQueryParams(environmentService.getMsg91Api(), params);

			String resp = restTemplate.getForObject(uri, String.class);
			logger.info("msg91 sms request id : " + resp);
		} catch (HttpClientErrorException ex) {
			try {
				HashMap<String, String> errorResp = objectMapper.readValue(ex.getResponseBodyAsString(), HashMap.class);
				String errorMsg = "Error in Sending Sms to ";
				errorMsg += mobile + " due to reason and code " + errorResp.getOrDefault("message", "") + " ";
				errorMsg += errorResp.getOrDefault("code", "") + ", http status code and message ";
				errorMsg += ex.getStatusCode().value() + " " + ex.getStatusCode().getReasonPhrase();
				logger.error(errorMsg, ex);
			} catch (IOException e) {
				logger.error("Error in parsing error response", e);
				throw e;
			}
		} catch (Exception e) {
			logger.error("Error in Sending Sms to " + mobile, e);
			throw e;
		}
	}

	@Async(Const.PSL_AFTER_MATCHING_PUBLISHER_EXECUTOR_NAME)
	public void sendGenericEmail(String email, String message, String subject) throws Exception {
		email = email.trim();
		String subjectWithTimestamp = subject + " [" + PslUtil.getIndianTimeZoneDateFromEpoch(Instant.now().toEpochMilli()) + "]";
		Context ctx = new Context();
		String emailTemplate = "email";
		ctx.setVariable("subject", subjectWithTimestamp);
		ctx.setVariable("message", message);
		this.sendSingleEmail(emailTemplate, ctx, Const.NO_REPLY_EMAIL, email, subjectWithTimestamp);
	}

	private void sendSingleEmail(String template, Context ctx, String from, String to, String subject) {
		String text = this.emailTemplateEngine.process(template, ctx);

		SendEmailRequest emailRequest = new SendEmailRequest();
		emailRequest.setDestination(new Destination().withToAddresses(to));
		emailRequest.setSource(Const.COMPANY_NAME + " <" +from + ">");
		emailRequest.withMessage(new Message(new Content(subject),
				new Body().withHtml(new Content().withCharset("UTF-8").withData(text))));
		sesClient.sendEmail(emailRequest);
	}
}
