package com.psl.helper;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.stereotype.Repository;

import com.psl.helper.exception.PslDaoException;
import com.psl.helper.exception.PslResourceCreateException;
import com.psl.helper.exception.PslResourceNotFoundException;
import com.psl.helper.utils.Const;
import com.psl.helper.utils.PslUtil;

@Repository
public abstract class AbstractBaseRepository {

	private ThreadLocal<Session> currentSession = new ThreadLocal<Session>();

	@PersistenceContext
	EntityManager entityManager;

	public <T> T getById(Serializable id, Class<T> clz) {
		T output = this.getCurrentSession().get(clz, id);
		if (output == null) {
			throw new PslResourceNotFoundException(
					String.format(Const.RESOURCE_NOT_FOUND, clz.getName(), id.toString()));
		}
		return output;
	}

	public <T> List<T> getAll(int pageNo, int pageSize, Class<T> clz) {

		try {
			Criteria criteria = this.getCurrentSession().createCriteria(clz);
			Map<String, Integer> pageDetails = PslUtil.paginationDetails(pageNo, pageSize);
			criteria.setFirstResult(pageDetails.get("startNo"));
			criteria.setMaxResults(pageSize);
			List<T> output = criteria.list();
			return output;
		} catch (HibernateException e) {
			throw new PslDaoException(String.format(Const.HIBERNATE_EXCEPTION, e.getMessage()), e);
		}
	}

	public Serializable create(Object entity) {
		try {
			return this.getCurrentSession().save(entity);
		} catch (ConstraintViolationException e) {
			throw new PslResourceCreateException(String.format(Const.CONSTRAINT_VIOLATION, entity.toString()), e);
		} catch (HibernateException e) {
			throw new PslDaoException(String.format(Const.HIBERNATE_EXCEPTION, e.getMessage()), e);
		}

	}

	public void update(Object entity) {
		try {
			this.getCurrentSession().update(entity);
		} catch (HibernateException e) {
			throw new PslDaoException(String.format(Const.HIBERNATE_EXCEPTION, e.getMessage()), e);
		}

	}

	public <T> T merge(T entity, Class<T> clz) {

		try {
			return (T) this.getCurrentSession().merge(entity);
		} catch (HibernateException e) {
			throw new PslDaoException(String.format(Const.HIBERNATE_EXCEPTION, e.getMessage()), e);
		}
	}

	public <T> List<T> getResultList(int pageNo, int pageSize, Query hql, Class<T> clz) {

		try {
			Map<String, Integer> pageDetails = PslUtil.paginationDetails(pageNo, pageSize);
			List<T> output = hql.setFirstResult(pageDetails.get("startNo")).setMaxResults(pageSize).list();
			return output;
		} catch (HibernateException e) {
			throw new PslDaoException(String.format(Const.HIBERNATE_EXCEPTION, e.getMessage()), e);
		}
	}

	public <T> List<T> getResultList(int pageNo, int pageSize, Criteria cr, Class<T> clz) {
		try {
			Map<String, Integer> pageDetails = PslUtil.paginationDetails(pageNo, pageSize);
			List<T> output = cr.setFirstResult(pageDetails.get("startNo")).setMaxResults(pageSize).list();
			return output;
		} catch (HibernateException e) {
			throw new PslDaoException(String.format(Const.HIBERNATE_EXCEPTION, e.getMessage()), e);
		}
	}

	public <T> List<T> getResultList(Criteria cr, Class<T> clz) {

		try {
			List<T> output = cr.list();
			return output;
		} catch (HibernateException e) {
			throw new PslDaoException(String.format(Const.HIBERNATE_EXCEPTION, e.getMessage()), e);
		}
	}

	@SuppressWarnings("unchecked")
	public <T> T getSingleResultOrNull(Object obj, Class<T> clz) {
		try {
			T output = null;
			if (obj instanceof Criteria) {
				return (T) ((Criteria) obj).uniqueResult();
			} else if (obj instanceof Query) {
				return (T) ((Query) obj).uniqueResult();
			} else {

				return null;
			}

		} catch (HibernateException e) {
			throw new PslDaoException(String.format(Const.HIBERNATE_EXCEPTION, e.getMessage()), e);
		}
	}

	@SuppressWarnings("unchecked")
	public <T> T getSingleResultOrException(Object obj, Class<T> clz) {
		try {
			T output = null;
			if (obj instanceof Criteria) {
                output = (T)((Criteria) obj).uniqueResult();
			} else if (obj instanceof Query) {
                output = (T)((Query) obj).uniqueResult();
			} else {
				throw new PslDaoException("Invalid Query type");
			}
			if(null == output)
            {
                throw new PslResourceNotFoundException("No Object Found");
            }
            else {
			    return output;
            }

		} catch (HibernateException e) {
			throw new PslDaoException(String.format(Const.HIBERNATE_EXCEPTION, e.getMessage()), e);
		}
	}

	public int executeUpdate(Query hql) {

		try {
			return hql.executeUpdate();
		} catch (HibernateException e) {
			throw new PslDaoException(String.format(Const.HIBERNATE_EXCEPTION, e.getMessage()), e);
		}
	}

	public void delete(Object entity) {
		try {
			this.getCurrentSession().delete(entity);
		} catch (HibernateException e) {
			throw new PslDaoException(String.format(Const.HIBERNATE_EXCEPTION, e.getMessage()), e);
		}
	}

	public void deleteById(Serializable entityId, Class clz) {

		try {
			Object entity = this.getById(entityId, clz);
			this.delete(entity);
		} catch (HibernateException e) {
			throw new PslDaoException(String.format(Const.HIBERNATE_EXCEPTION, e.getMessage()), e);
		}
	}

	public Session getCurrentSession() {
		Session session = currentSession.get();
		if (null != session) {
			return session;
		} else {
			return entityManager.unwrap(Session.class);
		}
	}

	public CriteriaBuilder getCriteriaBuilder() {
		return entityManager.getCriteriaBuilder();
	}

	public EntityManager getEntityManager() {
		return entityManager;
	}
}