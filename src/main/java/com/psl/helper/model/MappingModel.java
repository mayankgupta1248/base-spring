package com.psl.helper.model;

import java.util.ArrayList;
import java.util.Map;

public class MappingModel {

    private String baseClass;

    private boolean defaultMap;

    private String destinationClass;

    private Map<String,String> fields;

    private boolean mapNulls;

    private boolean mapNullsReverse;

    private ArrayList<String> excludeFields;

    @Override
    public String toString() {
        return "MappingModel{" +
                "baseClass='" + baseClass + '\'' +
                ", defaultMap=" + defaultMap +
                ", destinationClass='" + destinationClass + '\'' +
                ", fields=" + fields +
                ", mapNulls=" + mapNulls +
                ", mapNullsReverse=" + mapNullsReverse +
                ", excludeFields=" + excludeFields +
                '}';
    }

    public String getBaseClass() {
        return baseClass;
    }

    public void setBaseClass(String baseClass) {
        this.baseClass = baseClass;
    }

    public boolean isDefaultMap() {
        return defaultMap;
    }

    public void setDefaultMap(boolean defaultMap) {
        this.defaultMap = defaultMap;
    }

    public String getDestinationClass() {
        return destinationClass;
    }

    public void setDestinationClass(String destinationClass) {
        this.destinationClass = destinationClass;
    }

    public Map<String, String> getFields() {
        return fields;
    }

    public void setFields(Map<String, String> fields) {
        this.fields = fields;
    }

    public boolean isMapNulls() {
        return mapNulls;
    }

    public void setMapNulls(boolean mapNulls) {
        this.mapNulls = mapNulls;
    }

    public boolean isMapNullsReverse() {
        return mapNullsReverse;
    }

    public void setMapNullsReverse(boolean mapNullsReverse) {
        this.mapNullsReverse = mapNullsReverse;
    }

    public ArrayList<String> getExcludeFields() {
        return excludeFields;
    }

    public void setExcludeFields(ArrayList<String> excludeFields) {
        this.excludeFields = excludeFields;
    }
}
