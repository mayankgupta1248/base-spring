package com.psl.models.db;

import java.io.Serializable;
import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;


/**
 * The persistent class for the private_leaderboard_users database table.
 * 
 */
@Entity
@Table(name="private_leaderboard_users")
@NamedQuery(name="PrivateLeaderboardUser.findAll", query="SELECT p FROM PrivateLeaderboardUser p")
public class PrivateLeaderboardUser implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="created_at")
	private Long createdAt;

	@Column(name="joining_time")
	private Long joiningTime;

	@Column(name="user_id")
	private Long userId;
	
	@Column(name="private_leaderboard_id")
	private Long privateLeaderboardId;
	
	@Column(name="updated_at")
	private Long updatedAt;

	public PrivateLeaderboardUser() {
	}
	
	@PrePersist
	protected void onCreate() {
		this.createdAt = Instant.now().toEpochMilli();
		this.updatedAt = Instant.now().toEpochMilli();
	}

	@PreUpdate
	protected void onUpdate() {
		this.updatedAt = Instant.now().toEpochMilli();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getJoiningTime() {
		return joiningTime;
	}

	public void setJoiningTime(Long joiningTime) {
		this.joiningTime = joiningTime;
	}

	public Long getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Long createdAt) {
		this.createdAt = createdAt;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Long updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Long getPrivateLeaderboardId() {
		return privateLeaderboardId;
	}

	public void setPrivateLeaderboardId(Long privateLeaderboardId) {
		this.privateLeaderboardId = privateLeaderboardId;
	}
}