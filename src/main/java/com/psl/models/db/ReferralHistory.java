package com.psl.models.db;

import java.io.Serializable;
import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Version;


/**
 * The persistent class for the referral_history database table.
 * 
 */
@Entity
@Table(name="referral_history")
@NamedQuery(name="ReferralHistory.findAll", query="SELECT r FROM ReferralHistory r")
public class ReferralHistory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	@Column(name="created_at")
	private Long createdAt;

	@Column(name="updated_at")
	private Long updatedAt;

	@Version
	private Long version;

	@Column(name="referee_user_id")
	private Long refereeUserId;

	@Column(name="referrer_user_id")
	private Long referrerUserId;

	public ReferralHistory() {
	}

	@PrePersist
	protected void onCreate() {
		this.createdAt = Instant.now().toEpochMilli();
		this.updatedAt = Instant.now().toEpochMilli();
	}

	@PreUpdate
	protected void onUpdate() {
		this.updatedAt = Instant.now().toEpochMilli();
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Long createdAt) {
		this.createdAt = createdAt;
	}

	public Long getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Long updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public Long getRefereeUserId() {
		return refereeUserId;
	}

	public void setRefereeUserId(Long refereeUserId) {
		this.refereeUserId = refereeUserId;
	}

	public Long getReferrerUserId() {
		return referrerUserId;
	}

	public void setReferrerUserId(Long referrerUserId) {
		this.referrerUserId = referrerUserId;
	}
}