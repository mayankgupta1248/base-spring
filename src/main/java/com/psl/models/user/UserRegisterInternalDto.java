package com.psl.models.user;

public class UserRegisterInternalDto {

	private Long userId;
	
    private String email;

    private String referralCode;

	public UserRegisterInternalDto() {
	}
    
	public UserRegisterInternalDto(Long userId, String email, String referralCode) {
		super();
		this.userId = userId;
		this.email = email;
		this.referralCode = referralCode;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getReferralCode() {
		return referralCode;
	}

	public void setReferralCode(String referralCode) {
		this.referralCode = referralCode;
	}

    
}
