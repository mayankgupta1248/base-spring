package com.psl.models.user;

public class NotificationInternalDto {

	private Boolean sendMail;
	
	private Boolean sendSms;
	
	private Long userId;
	
	private String notificationType;

	private String message;
	
	String referenceId;

	public NotificationInternalDto() {
		super();
	}

	public NotificationInternalDto(Boolean sendMail, Boolean sendSms, Long userId, String notificationType,
			String message, String referenceId) {
		super();
		this.sendMail = sendMail;
		this.sendSms = sendSms;
		this.userId = userId;
		this.notificationType = notificationType;
		this.message = message;
		this.referenceId = referenceId;
	}

	public Boolean getSendMail() {
		return sendMail;
	}

	public void setSendMail(Boolean sendMail) {
		this.sendMail = sendMail;
	}

	public Boolean getSendSms() {
		return sendSms;
	}

	public void setSendSms(Boolean sendSms) {
		this.sendSms = sendSms;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getNotificationType() {
		return notificationType;
	}

	public void setNotificationType(String notificationType) {
		this.notificationType = notificationType;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}
}
