package com.psl.models.user;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

public class UserMobileDto {

	@Size(min = 10, max = 10, message = "Invalid mobile.")
	@NotBlank(message = "mobile can not be blank")
	@Pattern(regexp="(^$|[0-9]{10})", message = "Invalid mobile")
	private String mobile;

	@NotBlank(message = "mobileCode can not be blank")
	private String mobileCode;

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getMobileCode() {
		return mobileCode;
	}

	public void setMobileCode(String mobileCode) {
		this.mobileCode = mobileCode;
	}
}
