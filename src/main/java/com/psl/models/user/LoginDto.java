package com.psl.models.user;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

public class LoginDto {

	@NotBlank(message = "email can not be blank")
	@Email(message = "Invalid email")
    private String email;
    
    @NotBlank(message = "password can not be blank")
    private String password;

    public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
