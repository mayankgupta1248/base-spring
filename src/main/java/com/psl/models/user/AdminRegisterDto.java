package com.psl.models.user;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import com.psl.enums.UserRoles;
import com.psl.helper.utils.validators.constraint.Enm;
import com.psl.helper.utils.validators.constraint.ValidPassword;

public class AdminRegisterDto {
	
    @NotBlank(message = "email can not be blank")
    @Email(message = "invalid email")
    private String email;
    
    @NotBlank(message = "password can not be blank")
    @ValidPassword(message = "invalid password")
    private String password;

    @NotNull(message = "countryId can not be null")
    private Integer countryId;
    
    @NotBlank(message = "confirmPassword can not be blank")
    @ValidPassword(message = "invalid confirmPassword")
    private String confirmPassword;
    
    @NotBlank(message = "role can not be blank")
    @Enm(enm = UserRoles.class, message = "invalid role")
    private String role;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getCountryId() {
		return countryId;
	}

	public void setCountryId(Integer countryId) {
		this.countryId = countryId;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
}
