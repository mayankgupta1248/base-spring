package com.psl.models.user;

import org.hibernate.validator.constraints.NotBlank;

public class UserBankDetailDto {

	@NotBlank(message = "accountHolderName can not be null")
	private String accountHolderName;

	@NotBlank(message = "accountNumber can not be null")
	private String accountNumber;

	@NotBlank(message = "accountType can not be null")
	private String accountType;

	@NotBlank(message = "bankName can not be null")
	private String bankName;

	@NotBlank(message = "branchName can not be null")
	private String branchName;

	@NotBlank(message = "ifscCode can not be null")
	private String ifscCode;

	@NotBlank(message = "mobile can not be null")
	private String mobile;

	@NotBlank(message = "repeatAccountNumber can not be null")
	private String repeatAccountNumber;
	
	public String getAccountHolderName() {
		return accountHolderName;
	}

	public void setAccountHolderName(String accountHolderName) {
		this.accountHolderName = accountHolderName;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getRepeatAccountNumber() {
		return repeatAccountNumber;
	}

	public void setRepeatAccountNumber(String repeatAccountNumber) {
		this.repeatAccountNumber = repeatAccountNumber;
	}
}
