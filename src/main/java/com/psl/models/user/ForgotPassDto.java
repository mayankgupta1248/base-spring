package com.psl.models.user;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import com.psl.helper.utils.validators.constraint.ValidPassword;

public class ForgotPassDto {

	@NotBlank(message = "email can not be null or empty")
	@Email(message = "Invalid email")
	private String email;
	
	@NotBlank(message = "password can not be null or empty")
    @ValidPassword(message = "invalid password")
	private String password;
	
	@NotBlank(message = "confirmPassword can not be null or empty")
    @ValidPassword(message = "invalid password")
	private String confirmPassword;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}
}
