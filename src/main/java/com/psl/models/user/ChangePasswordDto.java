package com.psl.models.user;

import org.hibernate.validator.constraints.NotBlank;

import com.psl.helper.utils.validators.constraint.ValidPassword;

public class ChangePasswordDto {
	@NotBlank(message = "oldPassword can not be null or empty")
	private String oldPassword;
	
	@NotBlank(message = "newPassword can not be null or empty")
    @ValidPassword(message = "invalid password")
	private String newPassword;

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
}
