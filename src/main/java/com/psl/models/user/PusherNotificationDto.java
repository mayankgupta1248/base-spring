package com.psl.models.user;

public class PusherNotificationDto {
	
	private Long userId;
	
	private String notificationType;

	private String message;

	private Boolean readStatus;
	
	String referenceId;
	
	private Long createdAt;
	
	private Long updatedAt;

	public PusherNotificationDto() {
		super();
	}

	public PusherNotificationDto(Long userId, String notificationType, String message, Boolean readStatus,
			String referenceId, Long createdAt, Long updatedAt) {
		super();
		this.userId = userId;
		this.notificationType = notificationType;
		this.message = message;
		this.readStatus = readStatus;
		this.referenceId = referenceId;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getNotificationType() {
		return notificationType;
	}

	public void setNotificationType(String notificationType) {
		this.notificationType = notificationType;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Boolean getReadStatus() {
		return readStatus;
	}

	public void setReadStatus(Boolean readStatus) {
		this.readStatus = readStatus;
	}

	public String getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}

	public Long getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Long createdAt) {
		this.createdAt = createdAt;
	}

	public Long getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Long updatedAt) {
		this.updatedAt = updatedAt;
	}
	
}
