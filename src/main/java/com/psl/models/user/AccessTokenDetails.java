package com.psl.models.user;

public class AccessTokenDetails {

	private Long userId;
	private String uniqueId;
	private String email;
	private String role;
	private String authToken;

	public AccessTokenDetails() {
	}

	public AccessTokenDetails(Long userId, String uniqueId, String email, String role) {
		this.userId = userId;
		this.uniqueId = uniqueId;
		this.email = email;
		this.role = role;
	}

	public Long getUserId() {
		return userId;
	}

	public String getEmail() {
		return email;
	}

	public String getRole() {
		return role;
	}

	public String getAuthToken() {
		return authToken;
	}

	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}

	public String getUniqueId() {
		return uniqueId;
	}
}
