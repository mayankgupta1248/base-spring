package com.psl.models.user;

import java.util.List;

public class NotificationResponseDto {

	private Long unreadNotifications;

	private List<NotificationDto> notifications;

	public NotificationResponseDto() {
		super();
	}

	public NotificationResponseDto(Long unreadNotifications, List<NotificationDto> notifications) {
		super();
		this.unreadNotifications = unreadNotifications;
		this.notifications = notifications;
	}

	public Long getUnreadNotifications() {
		return unreadNotifications;
	}

	public void setUnreadNotifications(Long unreadNotifications) {
		this.unreadNotifications = unreadNotifications;
	}

	public List<NotificationDto> getNotifications() {
		return notifications;
	}

	public void setNotifications(List<NotificationDto> notifications) {
		this.notifications = notifications;
	}
}
