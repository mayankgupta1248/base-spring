package com.psl.models.user;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import com.psl.helper.utils.validators.constraint.ValidPassword;

public class UserRegisterDto {

    @NotBlank(message = "email can not be blank")
    @Email(message = "invalid email")
    private String email;
    
    @NotBlank(message = "password can not be blank")
    @ValidPassword(message = "invalid password")
    private String password;

    private String referralCode;
    
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

	public String getReferralCode() {
		return referralCode;
	}

	public void setReferralCode(String referralCode) {
		this.referralCode = referralCode;
	}
}
