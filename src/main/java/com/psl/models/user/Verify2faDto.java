package com.psl.models.user;

import org.hibernate.validator.constraints.NotBlank;

public class Verify2faDto {
	
	@NotBlank(message = "loginId can not be blank")
	private String loginId;

	@NotBlank(message = "email can not be blank")
	private String email;
	
	@NotBlank(message = "password can not be blank")
	private String password;

	@NotBlank(message = "otp can not be blank")
	private String otp;

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
