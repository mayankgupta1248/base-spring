package com.psl.models.user;

public class NotificationDto {
	
	private String notificationType;

	private String message;

	private Boolean readStatus;
	
	String referenceId;
	
	private Long createdAt;
	
	private Long updatedAt;
	
	public NotificationDto() {
		super();
	}

	public NotificationDto(String notificationType, String message, String referenceId, Boolean readStatus) {
		super();
		this.notificationType = notificationType;
		this.message = message;
		this.referenceId = referenceId;
		this.readStatus = readStatus;
	}

	public String getNotificationType() {
		return notificationType;
	}

	public void setNotificationType(String notificationType) {
		this.notificationType = notificationType;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Boolean getReadStatus() {
		return readStatus;
	}

	public void setReadStatus(Boolean readStatus) {
		this.readStatus = readStatus;
	}

	public Long getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Long createdAt) {
		this.createdAt = createdAt;
	}

	public Long getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Long updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}
}
