package com.psl.models.match;

public class PlayerResponseDto {
	
	private Long playerId;
		
	private String playerName;
	
	private String type;
	
	public PlayerResponseDto() {
		super();
	}

	public PlayerResponseDto(Long playerId, String playerName, String type) {
		super();
		this.playerId = playerId;
		this.playerName = playerName;
		this.type = type;
	}

	public Long getPlayerId() {
		return playerId;
	}

	public void setPlayerId(Long playerId) {
		this.playerId = playerId;
	}

	public String getPlayerName() {
		return playerName;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	
}
