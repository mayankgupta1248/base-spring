package com.psl.models.match;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class MatchRequestDto {

	@NotNull(message = "matchDetail can not be null")
	@Valid
	private MatchDeatilRequestDto matchDetail;

	@NotNull(message = "teams can not be null")
	@Size(min = 2, max = 2, message = "Minimum two teams are required")
	@Valid
	private List<TeamRequestDto> teams;

	public MatchDeatilRequestDto getMatchDetail() {
		return matchDetail;
	}

	public void setMatchDetail(MatchDeatilRequestDto matchDetail) {
		this.matchDetail = matchDetail;
	}

	public List<TeamRequestDto> getTeams() {
		return teams;
	}

	public void setTeams(List<TeamRequestDto> teams) {
		this.teams = teams;
	}
}
