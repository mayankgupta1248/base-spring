package com.psl.models.match;

import java.util.List;

public class TeamResponseDto {

	private String teamId;
	
	private String teamName;
	
	private List<PlayerResponseDto> players;

	public TeamResponseDto() {
	}
	
	public TeamResponseDto(String teamId, String teamName, List<PlayerResponseDto> players) {
		super();
		this.teamId = teamId;
		this.teamName = teamName;
		this.players = players;
	}

	public String getTeamId() {
		return teamId;
	}

	public void setTeamId(String teamId) {
		this.teamId = teamId;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public List<PlayerResponseDto> getPlayers() {
		return players;
	}

	public void setPlayers(List<PlayerResponseDto> players) {
		this.players = players;
	}
}
