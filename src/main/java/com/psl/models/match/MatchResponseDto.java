package com.psl.models.match;

import java.util.List;

public class MatchResponseDto {

	private MatchDetailResponseDto matchDetail;
	
	private List<TeamResponseDto> teams;

	public MatchResponseDto() {
		super();
	}

	public MatchResponseDto(MatchDetailResponseDto matchDetail, List<TeamResponseDto> teams) {
		super();
		this.matchDetail = matchDetail;
		this.teams = teams;
	}

	public MatchDetailResponseDto getMatchDetail() {
		return matchDetail;
	}

	public void setMatchDetail(MatchDetailResponseDto matchDetail) {
		this.matchDetail = matchDetail;
	}

	public List<TeamResponseDto> getTeams() {
		return teams;
	}

	public void setTeams(List<TeamResponseDto> teams) {
		this.teams = teams;
	}	
}
