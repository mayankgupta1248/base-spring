package com.psl.models.match;

public class MatchTeamPlayerDto {
	
	private Long id;

	private Long matchId;

	private Long playerId;

	private Long price;
	
	private String playerName;

	private String teamId;

	public MatchTeamPlayerDto() {
		super();
	}

	public MatchTeamPlayerDto(Long id, Long matchId, Long playerId, Long price, String playerName, String teamId) {
		super();
		this.id = id;
		this.matchId = matchId;
		this.playerId = playerId;
		this.price = price;
		this.playerName = playerName;
		this.teamId = teamId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getMatchId() {
		return matchId;
	}

	public void setMatchId(Long matchId) {
		this.matchId = matchId;
	}

	public Long getPlayerId() {
		return playerId;
	}

	public void setPlayerId(Long playerId) {
		this.playerId = playerId;
	}

	public String getPlayerName() {
		return playerName;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}

	public String getTeamId() {
		return teamId;
	}

	public void setTeamId(String teamId) {
		this.teamId = teamId;
	}

	public Long getPrice() {
		return price;
	}

	public void setPrice(Long price) {
		this.price = price;
	}
}
