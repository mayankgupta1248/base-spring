package com.psl.models.match;

public class MatchDetailResponseDto {
	private Long id;

	private Long buyFee;

	private int freeOrders;

	private Long createdAt;

	private String format;

	private Long joiningFee;

	private String externalMatchId;

	private String matchName;

	private Long maxUsers;

	private Long sellFee;

	private String seasonId;

	private String seasonName;

	private Long startTime;

	private String status;

	private Long updatedAt;

	private String venue;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getBuyFee() {
		return buyFee;
	}

	public void setBuyFee(Long buyFee) {
		this.buyFee = buyFee;
	}

	public int getFreeOrders() {
		return freeOrders;
	}

	public void setFreeOrders(int freeOrders) {
		this.freeOrders = freeOrders;
	}

	public Long getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Long createdAt) {
		this.createdAt = createdAt;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public Long getJoiningFee() {
		return joiningFee;
	}

	public void setJoiningFee(Long joiningFee) {
		this.joiningFee = joiningFee;
	}

	public String getExternalMatchId() {
		return externalMatchId;
	}

	public void setExternalMatchId(String externalMatchId) {
		this.externalMatchId = externalMatchId;
	}

	public String getMatchName() {
		return matchName;
	}

	public void setMatchName(String matchName) {
		this.matchName = matchName;
	}

	public Long getMaxUsers() {
		return maxUsers;
	}

	public void setMaxUsers(Long maxUsers) {
		this.maxUsers = maxUsers;
	}

	public Long getSellFee() {
		return sellFee;
	}

	public void setSellFee(Long sellFee) {
		this.sellFee = sellFee;
	}

	public String getSeasonId() {
		return seasonId;
	}

	public void setSeasonId(String seasonId) {
		this.seasonId = seasonId;
	}

	public String getSeasonName() {
		return seasonName;
	}

	public void setSeasonName(String seasonName) {
		this.seasonName = seasonName;
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Long updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getVenue() {
		return venue;
	}

	public void setVenue(String venue) {
		this.venue = venue;
	}
}
