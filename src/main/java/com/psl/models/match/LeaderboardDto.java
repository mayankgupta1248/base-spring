package com.psl.models.match;

public class LeaderboardDto {

	Long userId;
	
	String name;
	
	Long score;

	public LeaderboardDto() {
		super();
	}

	public LeaderboardDto(Long userId, String name, Long score) {
		super();
		this.userId = userId;
		this.name = name;
		this.score = score;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getScore() {
		return score;
	}

	public void setScore(Long score) {
		this.score = score;
	}
	
}
