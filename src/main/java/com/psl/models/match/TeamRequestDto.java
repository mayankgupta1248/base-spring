package com.psl.models.match;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

public class TeamRequestDto {

	@NotBlank(message = "teamId can not be null")
	private String teamId;
	
	@NotBlank(message = "teamName can not be null")
	private String teamName;
	
	@NotNull(message = "teams can not be null")
	@Size(min = 11, max = 11, message = "Minimum 11 and max 11 players are required")
	@Valid
	private List<PlayerRequestDto> players;

	public String getTeamId() {
		return teamId;
	}

	public void setTeamId(String teamId) {
		this.teamId = teamId;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public List<PlayerRequestDto> getPlayers() {
		return players;
	}

	public void setPlayers(List<PlayerRequestDto> players) {
		this.players = players;
	}
}
