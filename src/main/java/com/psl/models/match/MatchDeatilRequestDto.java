package com.psl.models.match;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

public class MatchDeatilRequestDto {
	@NotNull(message = "buyFees can not be null")
	@Min(value = 0, message = "minimum buyFees can be 0")
	private Long buyFee;

	@NotNull(message = "freeOrders can not be null")
	@Min(value = 0, message = "minimum freeOrders can be 0")
	private int freeOrders;

	@NotBlank(message = "format can not be null")
	private String format;

	@NotNull(message = "joiningFee can not be null")
	@Min(value = 0, message = "minimum joiningFee can be 0")
	private Long joiningFee;

	@NotBlank(message = "externalMatchId can not be null")
	private String externalMatchId;
	
	@NotBlank(message = "matchName can not be null or blank")
	private String matchName;

	@NotNull(message = "maxUsers can not be null")
	@Min(value = 0, message = "minimum maxUsers can be 0")
	private Long maxUsers;

	@NotNull(message = "sellFee can not be null")
	@Min(value = 0, message = "minimum sellFee can be 0")
	private Long sellFee;
	
	@NotBlank(message = "seasonId can not be null")
	private String seasonId;
	
	@NotBlank(message = "seasonName can not be null")
	private String seasonName;
	
	@NotBlank(message = "venue can not be null")
	private String venue;

	@NotNull(message = "startTime can not be null")
	@Min(value = 0, message = "minimum startTime can be 0")
	private Long startTime;

	public Long getBuyFee() {
		return buyFee;
	}

	public void setBuyFee(Long buyFee) {
		this.buyFee = buyFee;
	}

	public int getFreeOrders() {
		return freeOrders;
	}

	public void setFreeOrders(int freeOrders) {
		this.freeOrders = freeOrders;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public Long getJoiningFee() {
		return joiningFee;
	}

	public void setJoiningFee(Long joiningFee) {
		this.joiningFee = joiningFee;
	}

	public String getExternalMatchId() {
		return externalMatchId;
	}

	public void setExternalMatchId(String externalMatchId) {
		this.externalMatchId = externalMatchId;
	}

	public String getMatchName() {
		return matchName;
	}

	public void setMatchName(String matchName) {
		this.matchName = matchName;
	}

	public Long getMaxUsers() {
		return maxUsers;
	}

	public void setMaxUsers(Long maxUsers) {
		this.maxUsers = maxUsers;
	}

	public Long getSellFee() {
		return sellFee;
	}

	public void setSellFee(Long sellFee) {
		this.sellFee = sellFee;
	}

	public String getSeasonId() {
		return seasonId;
	}

	public void setSeasonId(String seasonId) {
		this.seasonId = seasonId;
	}

	public String getSeasonName() {
		return seasonName;
	}

	public void setSeasonName(String seasonName) {
		this.seasonName = seasonName;
	}

	public String getVenue() {
		return venue;
	}

	public void setVenue(String venue) {
		this.venue = venue;
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}
}
