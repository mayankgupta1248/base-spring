package com.psl.models.match;

import org.hibernate.validator.constraints.NotBlank;

import com.psl.enums.PlayerType;
import com.psl.helper.utils.validators.constraint.Enm;

public class PlayerRequestDto {

	@NotBlank(message = "externalPlayerId can not be null")
	private String externalPlayerId;
	
	@NotBlank(message = "externalPlayerName can not be null")
	private String externalPlayerName;
	
	@NotBlank(message = "type can not be null")
	@Enm(enm = PlayerType.class, message = "invalid type")
	private String type;

	public String getExternalPlayerId() {
		return externalPlayerId;
	}

	public void setExternalPlayerId(String externalPlayerId) {
		this.externalPlayerId = externalPlayerId;
	}

	public String getExternalPlayerName() {
		return externalPlayerName;
	}

	public void setExternalPlayerName(String externalPlayerName) {
		this.externalPlayerName = externalPlayerName;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
