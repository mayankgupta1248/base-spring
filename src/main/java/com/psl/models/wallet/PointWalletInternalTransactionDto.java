package com.psl.models.wallet;

public class PointWalletInternalTransactionDto {

	private Long userId;

	private String referenceId;

	private Long amount;

	private Long fees;

	private Long totalAmount;

	private String gateway;

	private String initiatedBy;

	private String remark;

	private String source;

	private String status;

	private String type;

	public PointWalletInternalTransactionDto() {
		super();
	}

	public PointWalletInternalTransactionDto(Long userId, String referenceId, Long amount, Long fees, Long totalAmount,
			String gateway, String initiatedBy, String remark, String source, String status, String type) {
		super();
		this.userId = userId;
		this.referenceId = referenceId;
		this.amount = amount;
		this.fees = fees;
		this.totalAmount = totalAmount;
		this.gateway = gateway;
		this.initiatedBy = initiatedBy;
		this.remark = remark;
		this.source = source;
		this.status = status;
		this.type = type;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}

	public Long getAmount() {
		return amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

	public Long getFees() {
		return fees;
	}

	public void setFees(Long fees) {
		this.fees = fees;
	}

	public Long getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Long totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getGateway() {
		return gateway;
	}

	public void setGateway(String gateway) {
		this.gateway = gateway;
	}

	public String getInitiatedBy() {
		return initiatedBy;
	}

	public void setInitiatedBy(String initiatedBy) {
		this.initiatedBy = initiatedBy;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
