package com.psl.user.service;

import java.text.MessageFormat;
import java.time.Instant;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.psl.EnvironmentService;
import com.psl.enums.ErrorCodes;
import com.psl.enums.OtpType;
import com.psl.enums.UserRoles;
import com.psl.enums.UserStatus;
import com.psl.helper.EmailAndSmsService;
import com.psl.helper.exception.PslForbidden;
import com.psl.helper.exception.PslUnauthorized;
import com.psl.helper.exception.PslUnprocessableEntity;
import com.psl.helper.logger.PslLogManager;
import com.psl.helper.logger.PslLogger;
import com.psl.helper.utils.AuthUtility;
import com.psl.helper.utils.Const;
import com.psl.helper.utils.EncryptUtil;
import com.psl.helper.utils.PslMapper;
import com.psl.helper.utils.PslUtil;
import com.psl.helper.utils.RedisUtility;
import com.psl.helper.utils.request.PslRequestContext;
import com.psl.models.user.AccessTokenDetails;
import com.psl.models.user.AdminLoginDto;
import com.psl.models.user.AdminRegisterDto;
import com.psl.models.user.ChangePasswordDto;
import com.psl.models.user.ForgotPassDto;
import com.psl.models.user.LoginDto;
import com.psl.models.user.NotificationDto;
import com.psl.models.user.NotificationInternalDto;
import com.psl.models.user.NotificationResponseDto;
import com.psl.models.user.UserInfoDto;
import com.psl.models.user.UserMobileDto;
import com.psl.models.user.UserRegisterDto;
import com.psl.models.user.UserRegisterInternalDto;
import com.psl.user.domain.Notification;
import com.psl.user.domain.UserInfo;
import com.psl.user.repository.UserRepository;
import com.psl.user.sqs.UserSqsPublisher;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private UserSqsPublisher userSqsPublisher;

	@Autowired
	private EmailAndSmsService emailAndSmsService;

	@Autowired
	private EnvironmentService environmentService;

	@Autowired
	private AuthUtility authUtility;

	@Autowired
	private RedisUtility redisUtility;

	private String authTokenEncryptionSeparator = "---_---";

	private int authBcryptSalt = 8;

	private int authTokenExpiryInMillis = 86400000;

	private PslMapper pslMapper = new PslMapper();

	private static final PslLogger logger = PslLogManager.getLogger(UserService.class);

	@Transactional(rollbackFor = Exception.class)
	public UserInfoDto registerNewUserAccount(UserRegisterDto userRegisterDto) throws Exception {
		
		UserInfo existingUser = userRepository.getUserByEmail(userRegisterDto.getEmail());
		if (existingUser != null) {
			throw new PslUnprocessableEntity("This email is already registered.");
		}

		String salt = BCrypt.gensalt(authBcryptSalt);
		String hashedPassword = BCrypt.hashpw(userRegisterDto.getPassword(), salt);
		String authToken = this.generateToken(userRegisterDto.getEmail(), userRegisterDto.getEmail(), salt);
		UserInfo newUserInfo = new UserInfo(userRegisterDto.getEmail(), null, null, null, null,
				hashedPassword, salt, false, false, null, null, authToken, null, UserStatus.ACTIVE.name(), UserRoles.USER.name(),
				false);

		this.userRepository.create(newUserInfo);

		// Send data to referral Queue, it can be replaced by SNS if other service needs
		// same data
		userSqsPublisher.sendToSQS(environmentService.getNewUserWalletQueue(), newUserInfo.getId());
		UserRegisterInternalDto userRegisterInternalDto = new UserRegisterInternalDto(newUserInfo.getId(),
				newUserInfo.getEmail(), userRegisterDto.getReferralCode());
		userSqsPublisher.sendToSQS(environmentService.getNewUserReferralQueue(), userRegisterInternalDto);

		// Send email to user
		emailAndSmsService.sendGenericEmail(userRegisterDto.getEmail(), Const.REGISTRATION_SUCCESS_MSG,
				Const.EMAIL_INVITE_SUBJECT);
		return pslMapper.convertModel(newUserInfo, UserInfoDto.class);
	}

	@Transactional(rollbackFor = Exception.class)
	public UserInfoDto login(LoginDto loginDto) throws Exception {
		UserInfo userInfo = this.userRepository.getUserByEmail(loginDto.getEmail());
		if (userInfo == null) {
			throw new PslUnauthorized(ErrorCodes.LOGIN101.getDescription(), ErrorCodes.LOGIN101.getCode(), null);
		} else if (UserStatus.SUSPENDED.name().equalsIgnoreCase(userInfo.getStatus())) {
			throw new PslUnauthorized(ErrorCodes.LOGIN102.getDescription(), ErrorCodes.LOGIN102.getCode(), null);
		} else if (UserStatus.BLACKLISTED.name().equalsIgnoreCase(userInfo.getStatus())) {
			throw new PslUnauthorized(ErrorCodes.LOGIN104.getDescription(), ErrorCodes.LOGIN104.getCode(), null);
		} else if (!UserRoles.USER.name().equalsIgnoreCase(userInfo.getUserRole())) {
			throw new PslUnauthorized("Invalid login.");
		}

		String hashedPassword = BCrypt.hashpw(loginDto.getPassword(), userInfo.getSalt());
		if (!hashedPassword.equals(userInfo.getPassword())) {
			throw new PslUnauthorized(ErrorCodes.LOGIN103.getDescription(), ErrorCodes.LOGIN103.getCode(), null);
		} else {
			try {
				this.validateToken(
						EncryptUtil.decrypt(userInfo.getAuthToken(), environmentService.getSecretEncryptionKey()));
			} catch (Exception ex) {
				userInfo.setAuthToken(
						this.generateToken(userInfo.getEmail(), userInfo.getMobile(), userInfo.getSalt()));
				this.userRepository.update(userInfo);
			}

			String message = MessageFormat.format(Const.LOGIN_SUCCESS_EMAIL_MSG, PslUtil.getIndianTimeZoneDateFromEpoch(Instant.now().toEpochMilli()));
			this.emailAndSmsService.sendGenericEmail(userInfo.getEmail(), message, Const.NEW_LOGIN_SUBJECT);
			return pslMapper.convertModel(userInfo, UserInfoDto.class);
		}
	}

	@Transactional(rollbackFor = Exception.class)
	public String forgotPassword(ForgotPassDto forgotPassDto, String otp) throws Exception {
		if (!forgotPassDto.getPassword().equals(forgotPassDto.getConfirmPassword())) {
			throw new PslUnprocessableEntity("Password and Confirm Password mismatch");
		}

		UserInfo userInfo = userRepository.getUserByEmail(forgotPassDto.getEmail());
		if (userInfo == null) {
			throw new PslUnauthorized("Invalid email");
		}

		String key = OtpType.FORGOT_PASS.name() + "_" + userInfo.getEmail();
		this.validateOtp(key, otp);

		String oldHashedPassword = BCrypt.hashpw(forgotPassDto.getPassword(), userInfo.getSalt());

		if (oldHashedPassword.equals(userInfo.getPassword())) {
			throw new PslUnprocessableEntity("New password should be different from last password");
		}

		String salt = BCrypt.gensalt(authBcryptSalt);
		String newHashedPassword = BCrypt.hashpw(forgotPassDto.getPassword(), salt);

		if (newHashedPassword.equals(userInfo.getPassword())) {
			throw new PslUnprocessableEntity("New password should be different from last password");
		}

		userInfo.setPassword(newHashedPassword);
		userInfo.setSalt(salt);
		userInfo.setAuthToken(this.generateToken(userInfo.getEmail(), userInfo.getEmail(), salt));
		this.userRepository.update(userInfo);
		return "Password changed. Please login.";
	}

	@Transactional(rollbackFor = Exception.class)
	public String getAccessToken(String authToken) throws Exception {
		UserInfo userInfo = this.validateAndGetUserFromToken(authToken);
		if (UserStatus.SUSPENDED.name().equalsIgnoreCase(userInfo.getStatus())) {
			throw new PslUnauthorized("UserInfo Account is suspended");
		} else if (UserStatus.BLACKLISTED.name().equalsIgnoreCase(userInfo.getStatus())) {
			throw new PslUnauthorized("UserInfo Account is blacklisted");
		} else {
			if (null != userInfo.getAccessToken() && !userInfo.getAccessToken().isEmpty()) {
				return userInfo.getAccessToken();
			} else {
				AccessTokenDetails accessTokenDetails = new AccessTokenDetails(userInfo.getId(), userInfo.getUsername(),
						userInfo.getEmail(), userInfo.getUserRole());
				String encryptedAccessToken = this.authUtility.getHmacAccessToken(accessTokenDetails);
				userInfo.setAccessToken(encryptedAccessToken);
				this.userRepository.update(userInfo);
				return userInfo.getAccessToken();
			}
		}
	}

	private UserInfo validateAndGetUserFromToken(String token) throws Exception {
		String plainAuthToken = EncryptUtil.decrypt(token, environmentService.getSecretEncryptionKey());
		this.validateToken(plainAuthToken);
		String[] keys = plainAuthToken.split(authTokenEncryptionSeparator);
		UserInfo userInfo = this.userRepository.getUserByEmail(keys[0]);
		if (userInfo == null || !userInfo.getAuthToken().equals(token)) {
			throw new PslUnauthorized("Auth Token is Invalid");
		}
		return userInfo;
	}

	private void validateToken(String plainAuthToken) throws Exception {
		String[] keys = plainAuthToken.split(authTokenEncryptionSeparator);
		long currentTimeInMilliSec = Instant.now().toEpochMilli();
		if (keys.length != 4 || !(keys[3].matches("[0-9]+"))
				|| (Long.valueOf(keys[3]) + authTokenExpiryInMillis) < currentTimeInMilliSec) {
			throw new PslUnauthorized("Token is invalid");
		}
	}

	private String generateToken(String email, String phone, String salt) throws Exception {
		String concatenatedSalt = email + salt + phone;
		String hashedSalt = BCrypt.hashpw(concatenatedSalt, BCrypt.gensalt(authBcryptSalt));
		String plainAuthToken = email + authTokenEncryptionSeparator + hashedSalt + authTokenEncryptionSeparator + phone
				+ authTokenEncryptionSeparator + String.valueOf(Instant.now().toEpochMilli());
		return EncryptUtil.encrypt(plainAuthToken, environmentService.getSecretEncryptionKey());
	}

	@Transactional(readOnly = true)
	public String sendEmailVerifyOtp(String email) throws Exception {

		UserInfo userInfo = this.userRepository.getUserByEmail(email);
		if (userInfo != null) {
			throw new PslUnprocessableEntity("This email is already registered.");
		}

		if (PslUtil.isNullOrBlank(email) || !EmailValidator.getInstance().isValid(email)) {
			throw new PslUnprocessableEntity("Invalid email.");
		}
		String key = OtpType.VERIFY_EMAIL.name() + "_" + email;
		String otp = saveOtpInRedis(key);
		String message = MessageFormat.format(Const.VERIFICATION_OTP_MSG, "email", otp,
				String.valueOf(Const.EMAIL_OTP_EXPIRY));
		this.emailAndSmsService.sendGenericEmail(email, message, Const.EMAIL_VERIFICATION_SUBJECT);
		return "OTP sent to your email.";
	}

	@Transactional(readOnly = true)
	public String sendMobileVerifyOtp(UserMobileDto userMobileDto) throws Exception {
		UserInfo existingUser = this.userRepository.getUserByMobile(userMobileDto.getMobile());
		if (existingUser != null) {
			throw new PslUnprocessableEntity("This mobile is already registered.");
		}

		UserInfo userInfo = getUserFromAccessTokenDetails();

		if (userInfo.getMobile() != null) {
			throw new PslUnprocessableEntity("Some other mobile is registered with this account");
		}

		String key = OtpType.VERIFY_MOBILE.name() + "_" + userInfo.getEmail();
		String otp = saveOtpInRedis(key);
		String message = MessageFormat.format(Const.VERIFICATION_OTP_MSG, "mobile", otp,
				String.valueOf(Const.EMAIL_OTP_EXPIRY));
		this.emailAndSmsService.sendSms(message, userMobileDto.getMobile());
		return "OTP sent to your mobile.";
	}

	@Transactional(rollbackFor = Exception.class)
	public UserInfoDto verifyOtpAddMobile(UserMobileDto userMobileDto, String otp) throws Exception {

		UserInfo existingUser = this.userRepository.getUserByMobile(userMobileDto.getMobile());
		if (existingUser != null) {
			throw new PslUnprocessableEntity("Mobile is already registered.");
		}

		UserInfo userInfo = getUserFromAccessTokenDetails();

		String key = OtpType.VERIFY_MOBILE.name() + "_" + userInfo.getEmail();
		this.validateOtp(key, otp);

		userInfo.setMobileVerified(true);
		userInfo.setMobile(userMobileDto.getMobile());
		userInfo.setMobileCode(userMobileDto.getMobileCode());

		userInfo = this.userRepository.merge(userInfo, UserInfo.class);
		return pslMapper.convertModel(userInfo, UserInfoDto.class);
	}

	private String saveOtpInRedis(String key) throws Exception {
		String otp = PslUtil.generateOTP();
		redisUtility.setWithExpiry(Const.REDIS_NAMESPACE, key, otp, Const.OTP_EXPIRY, TimeUnit.MINUTES);
		return otp;
	}

	private void validateOtp(String key, String otp) throws Exception {
		String persistedOtp = redisUtility.getValueOrNull(Const.REDIS_NAMESPACE, key, String.class);
		if (persistedOtp == null) {
			throw new PslUnauthorized(ErrorCodes.OTP104.getDescription(), ErrorCodes.OTP104.getCode(), null);
		} else if (!persistedOtp.equalsIgnoreCase(otp)) {
			throw new PslUnauthorized(ErrorCodes.OTP105.getDescription(), ErrorCodes.OTP105.getCode(), null);
		} else {
			redisUtility.deleteKey(Const.REDIS_NAMESPACE, key);
		}
	}

	public String forgotPasswordOtp(ForgotPassDto forgotPassDto) throws Exception {
		UserInfo userInfo = userRepository.getUserByEmail(forgotPassDto.getEmail());
		if (userInfo == null) {
			throw new PslUnprocessableEntity("No such user");
		}
		logger.info("Forgot password request for user " + userInfo.getUsername());

		String key = OtpType.FORGOT_PASS.name() + "_" + userInfo.getEmail();
		String otp = saveOtpInRedis(key);
		String message = MessageFormat.format(Const.FORGOT_PASS_MSG, otp, String.valueOf(Const.EMAIL_OTP_EXPIRY));
		this.emailAndSmsService.sendGenericEmail(userInfo.getEmail(), message, Const.EMAIL_FORGOT_PASSWORD_SUBJECT);

		return "OTP sent to email.";
	}

	@Transactional(readOnly = true)
	public UserInfoDto getUserByUserId(Long userId) {
		return pslMapper.convertModel(this.userRepository.getById(userId, UserInfo.class), UserInfoDto.class);
	}

	public AccessTokenDetails getAccessTokenDetails() {
		AccessTokenDetails accessTokenDetails = PslRequestContext.getAccessTokenDetails();
		if (null == accessTokenDetails) {
			throw new PslForbidden("Resource is Forbidden");
		} else {
			return accessTokenDetails;
		}
	}

	public UserInfo getUserFromAccessTokenDetails() {
		AccessTokenDetails accessTokenDetails = PslRequestContext.getAccessTokenDetails();
		if (null == accessTokenDetails) {
			throw new PslForbidden("Resource is Forbidden");
		}
		UserInfo userInfo = this.userRepository.getUserByUserId(accessTokenDetails.getUserId());
		if (userInfo == null) {
			throw new PslUnauthorized("No user found");
		}
		return userInfo;
	}

	@Transactional(readOnly = true)
	public UserInfoDto getUserDtoByAccessToken() {
		UserInfo userInfo = getUserFromAccessTokenDetails();
		return pslMapper.convertModel(userInfo, UserInfoDto.class);
	}

	@Transactional(readOnly = true)
	public NotificationResponseDto getUserNotifications(int offset, int size) {
		Long userId = this.getAccessTokenDetails().getUserId();
		List<Notification> notifications = this.userRepository.getUserNotifications(userId, offset, size);
		List<NotificationDto> notificationDto = pslMapper.convertModelList(notifications, NotificationDto.class);
		Long unreadNotifications = this.userRepository.getUnreadNotificationCount(userId);
		return new NotificationResponseDto(unreadNotifications, notificationDto);
	}

	@Transactional(rollbackFor = Exception.class)
	public String markNotificationAsRead() {
		Long userId = this.getAccessTokenDetails().getUserId();
		this.userRepository.updateUserNotification(userId);
		return "Success";
	}

	@Transactional(rollbackFor = Exception.class)
	public void handleNotification(NotificationInternalDto notificationInternalDto) throws Exception {
		Notification notification = this.userRepository.getUserNotificationByRefrenceId(
				notificationInternalDto.getUserId(), notificationInternalDto.getReferenceId());
		if (notification != null) {
			return;
		} else {
			notification = new Notification(notificationInternalDto.getNotificationType(),
					notificationInternalDto.getMessage(), notificationInternalDto.getReferenceId(), false,
					notificationInternalDto.getUserId());
			this.userRepository.create(notification);
		}

		// PusherNotificationDto pusherNotificationDto = new
		// PusherNotificationDto(notification.getUserId(),
		// notification.getNotificationType(), notification.getMessage(),
		// notification.getReadStatus(),
		// notification.getReferenceId(), notification.getCreatedAt(),
		// notification.getUpdatedAt());

		// userSqsPublisher.sendToSQSAsync(environmentService.getUiUserNotificationQueue(),
		// pusherNotificationDto);

		if (notificationInternalDto.getSendMail()) {
			UserInfo userInfo = this.userRepository.getUserByUserId(notificationInternalDto.getUserId());
			this.emailAndSmsService.sendGenericEmail(userInfo.getEmail(), notificationInternalDto.getMessage(),
					notificationInternalDto.getNotificationType());
		}
	}

	@Transactional(rollbackFor = Exception.class)
	public String createNewAdmin(AdminRegisterDto adminRegisterDto) throws Exception {
		if (UserRoles.USER.name().equalsIgnoreCase(adminRegisterDto.getRole())) {
			throw new PslUnprocessableEntity("USER type role can not be created");
		}
		UserInfo existingUser = userRepository.getUserByEmail(adminRegisterDto.getEmail());
		if (existingUser != null) {
			throw new PslUnprocessableEntity("This email is already registered.");
		}
		if (!adminRegisterDto.getPassword().equals(adminRegisterDto.getConfirmPassword())) {
			throw new PslUnprocessableEntity("Password and Confirm Password mismatch");
		}
		String salt = BCrypt.gensalt(authBcryptSalt);
		String hashedPassword = BCrypt.hashpw(adminRegisterDto.getPassword(), salt);
		String authToken = this.generateToken(adminRegisterDto.getEmail(), adminRegisterDto.getEmail(), salt);

		UserInfo newAdminUser = new UserInfo(adminRegisterDto.getEmail(), null, null, null,
				null, hashedPassword, salt, true, false, null, null, authToken, null, UserStatus.ACTIVE.name(),
				UserRoles.ADMIN.name(), true);
		this.userRepository.create(newAdminUser);
		return "Account successfully created";
	}

	@Transactional(rollbackFor = Exception.class)
	public UserInfoDto adminLogin(AdminLoginDto adminLoginDto) throws Exception {
		UserInfo userInfo = this.userRepository.getUserByEmail(adminLoginDto.getEmail());

		if (userInfo == null) {
			throw new PslUnauthorized(ErrorCodes.LOGIN101.getDescription(), ErrorCodes.LOGIN101.getCode(), null);
		} else if (UserStatus.SUSPENDED.name().equalsIgnoreCase(userInfo.getStatus())) {
			throw new PslUnauthorized(ErrorCodes.LOGIN102.getDescription(), ErrorCodes.LOGIN102.getCode(), null);
		} else if (UserStatus.BLACKLISTED.name().equalsIgnoreCase(userInfo.getStatus())) {
			throw new PslUnauthorized(ErrorCodes.LOGIN104.getDescription(), ErrorCodes.LOGIN104.getCode(), null);
		} else if (UserRoles.USER.name().equalsIgnoreCase(userInfo.getUserRole())) {
			throw new PslUnauthorized("Invalid login.");
		}

		String hashedPassword = BCrypt.hashpw(adminLoginDto.getPassword(), userInfo.getSalt());
		if (!hashedPassword.equals(userInfo.getPassword())) {
			throw new PslUnauthorized(ErrorCodes.LOGIN103.getDescription(), ErrorCodes.LOGIN103.getCode(), null);
		} else {
			try {
				this.validateToken(
						EncryptUtil.decrypt(userInfo.getAuthToken(), environmentService.getSecretEncryptionKey()));
			} catch (Exception ex) {
				userInfo.setAuthToken(
						this.generateToken(userInfo.getEmail(), userInfo.getMobile(), userInfo.getSalt()));
				this.userRepository.update(userInfo);
			}
		}

		return pslMapper.convertModel(userInfo, UserInfoDto.class);
	}

	@Transactional(readOnly = true)
	public String changePasswordOtp(ChangePasswordDto changePasswordDto) throws Exception {
		UserInfo userInfo = getUserFromAccessTokenDetails();
		validateChangePasswordRequestData(userInfo, changePasswordDto);
		String key = OtpType.CHANGE_PASS.name() + "_" + userInfo.getEmail();
		String otp = saveOtpInRedis(key);
		String message = MessageFormat.format(Const.CHANGE_PASS_MSG, otp, String.valueOf(Const.EMAIL_OTP_EXPIRY));
		this.emailAndSmsService.sendGenericEmail(userInfo.getEmail(), message, Const.EMAIL_CHANGE_PASSWORD_SUBJECT);
		return "OTP sent to your email.";
	}

	@Transactional
	public String changePassword(ChangePasswordDto changePasswordDto, String otp) throws Exception {

		UserInfo userInfo = getUserFromAccessTokenDetails();

		String salt = BCrypt.gensalt(authBcryptSalt);
		String newHashedPassword = BCrypt.hashpw(changePasswordDto.getNewPassword(), salt);

		String key = OtpType.CHANGE_PASS.name() + "_" + userInfo.getEmail();
		this.validateOtp(key, otp);

		userInfo.setPassword(newHashedPassword);
		userInfo.setSalt(salt);
		userInfo.setAuthToken(this.generateToken(userInfo.getEmail(), userInfo.getEmail(), salt));
		this.userRepository.update(userInfo);
		return "Password changed successfully";
	}

	private void validateChangePasswordRequestData(UserInfo userInfo, ChangePasswordDto changePasswordDto) {

		String oldHashedPassword = BCrypt.hashpw(changePasswordDto.getOldPassword(), userInfo.getSalt());
		if (!oldHashedPassword.equals(userInfo.getPassword())) {
			throw new PslUnprocessableEntity("Wrong old password");
		}

		String newHashedPasswordWithOldSalt = BCrypt.hashpw(changePasswordDto.getNewPassword(), userInfo.getSalt());

		if (newHashedPasswordWithOldSalt.equals(userInfo.getPassword())) {
			throw new PslUnprocessableEntity("New password must be different from last password");
		}
	}

	@Transactional(readOnly = true)
	public List<UserRegisterInternalDto> getLeaderboardUserDetails(List<Long> userIds) {
		List<UserRegisterInternalDto> userList = this.userRepository.getUserByUserIds(userIds);
		if (userList != null) {
			for (UserRegisterInternalDto userRegisterInternalDto : userList) {
				userRegisterInternalDto
						.setEmail(PslUtil.hideEmailExceptFirstAndLastLetter(userRegisterInternalDto.getEmail()));
			}
		}
		return userList;
	}

	@Transactional
	public String changeUserStatus(Long userId, UserStatus userStatus) {
		if(null == userStatus) {
			throw new PslUnprocessableEntity("Invalid status");
		}
		
		UserInfo userInfo = this.userRepository.getUserByUserId(userId);
		if(userInfo.getStatus().equalsIgnoreCase(userStatus.name())) {
			throw new PslUnprocessableEntity("Status is already " + userStatus.name());
		}
		userInfo.setStatus(userStatus.name());
		this.userRepository.update(userInfo);
		
		return "Status changed to " + userStatus.name();
	}
}
