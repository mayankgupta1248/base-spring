package com.psl.user.sqs;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.psl.helper.sqs.JmsConstant;
import com.psl.helper.utils.Const;
import com.psl.helper.utils.PslUtil;

@Service
public class UserSqsPublisher {
	
	@Autowired
	@Qualifier(JmsConstant.SQS_JMS_TEMPLATE)
	private JmsTemplate jmsTemplate;

	public void sendToSQS(final String queue, final Object payload) {
		jmsTemplate.send(queue, new MessageCreator() {
			@Override
			public Message createMessage(Session session) throws JMSException {
				TextMessage textMessage = session.createTextMessage(PslUtil.objectToJson(payload));
				textMessage.setStringProperty("JMSXGroupID", queue);
				return textMessage;
			}
		});
	}
	
	@Async(Const.PSL_SQS_PUBLISHER_EXECUTOR_NAME)
	public void sendToSQSAsync(final String queue, final Object payload) {
		jmsTemplate.send(queue, new MessageCreator() {
			@Override
			public Message createMessage(Session session) throws JMSException {
				TextMessage textMessage = session.createTextMessage(PslUtil.objectToJson(payload));
				textMessage.setStringProperty("JMSXGroupID", queue);
				return textMessage;
			}
		});
	}
}
