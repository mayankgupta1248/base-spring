package com.psl.user.sqs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.psl.helper.logger.PslLogManager;
import com.psl.helper.logger.PslLogger;
import com.psl.user.service.UserService;

@Service
public class UserSqsListner {
	
	@Autowired
	private UserService userService;

	private static final PslLogger LOGGER = PslLogManager.getLogger(UserSqsListner.class);
	
//	@JmsListener(destination = "${user_notification_queue}", containerFactory = JmsConstant.SQS_JMS_CONTAINER_FACTORY)
//	public void onNewNotification(Message message) {
//
//		TextMessage textMessage = (TextMessage) message;
//		try {
//			LOGGER.info("New Notification " + textMessage.getText());
//			NotificationInternalDto notificationInternalDto = PslUtil.jsonToObject(textMessage.getText(), NotificationInternalDto.class);
//			userService.handleNotification(notificationInternalDto);
//			message.acknowledge();
//		} catch (Exception e) {
//			LOGGER.error("Error processing message ", e);
//		}
//	}
}
