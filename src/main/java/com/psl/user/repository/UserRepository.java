package com.psl.user.repository;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.AliasToBeanResultTransformer;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.psl.enums.LoginStatus;
import com.psl.helper.AbstractBaseRepository;
import com.psl.models.user.UserRegisterInternalDto;
import com.psl.user.domain.Notification;
import com.psl.user.domain.Role;
import com.psl.user.domain.UserInfo;
import com.psl.user.domain.UserLogin;

@Repository
public class UserRepository extends AbstractBaseRepository {

	public UserInfo getUserByEmail(String email) {
		Criteria criteria = this.getCurrentSession().createCriteria(UserInfo.class);
		criteria.add(Restrictions.eq("email", email));
		return this.getSingleResultOrNull(criteria, UserInfo.class);
	}

	public UserInfo getUserByMobile(String mobile) {
		Criteria criteria = this.getCurrentSession().createCriteria(UserInfo.class);
		criteria.add(Restrictions.eq("mobile", mobile));
		return this.getSingleResultOrNull(criteria, UserInfo.class);
	}

	public UserInfo getUserByUserId(Long userId) {
		Criteria criteria = this.getCurrentSession().createCriteria(UserInfo.class);
		criteria.add(Restrictions.eq("id", userId));
		return this.getSingleResultOrNull(criteria, UserInfo.class);
	}

	public List<UserLogin> getUserLogin(Long userId, int offset, int size) {
		Criteria criteria = this.getCurrentSession().createCriteria(UserLogin.class);
		criteria.add(Restrictions.eq("userId", userId));
		criteria.setFirstResult(offset);
		criteria.setMaxResults(size);
		criteria.addOrder(org.hibernate.criterion.Order.desc("id"));
		return this.getResultList(criteria, UserLogin.class);
	}

	public List<Notification> getUserNotifications(Long userId, int offset, int size) {
		Criteria criteria = this.getCurrentSession().createCriteria(Notification.class);
		criteria.add(Restrictions.eq("userId", userId));
		criteria.setFirstResult(offset);
		criteria.setMaxResults(size);
		criteria.addOrder(org.hibernate.criterion.Order.desc("id"));
		return this.getResultList(criteria, Notification.class);
	}

	public void updateUserNotification(Long userId) {
		String hql = "UPDATE Notification set readStatus = :readStatus where userId = :userId";
		Query query = this.getCurrentSession().createQuery(hql);
		query.setParameter("readStatus", true);
		query.setParameter("userId", userId);
		query.executeUpdate();
	}

	public Notification getUserNotificationByRefrenceId(Long userId, String referenceId) {
		Criteria criteria = this.getCurrentSession().createCriteria(Notification.class);
		criteria.add(Restrictions.eq("userId", userId));
		criteria.add(Restrictions.eq("referenceId", referenceId));
		return this.getSingleResultOrNull(criteria, Notification.class);
	}

	public Role getRoleByName(String roleName) {
		Criteria criteria = this.getCurrentSession().createCriteria(Role.class);
		criteria.add(Restrictions.eq("name", roleName));
		return this.getSingleResultOrException(criteria, Role.class);
	}

	public UserLogin getUserLoginByLoginId(String loginId) {
		Criteria criteria = this.getCurrentSession().createCriteria(UserLogin.class);
		criteria.add(Restrictions.eq("loginId", loginId));
		return this.getSingleResultOrNull(criteria, UserLogin.class);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void createFailedUserLogin(Long userId, UserLogin userLogin, String remark) {
		userLogin.setUserId(userId);
		userLogin.setStatus(LoginStatus.FAILED.name());
		userLogin.setRemark(remark);
		this.create(userLogin);
	}

	public Long createNewUserLogin(Long userId, UserLogin userLogin, String status, String remark) {
		userLogin.setUserId(userId);
		userLogin.setStatus(status);
		userLogin.setRemark(remark);
		this.create(userLogin);
		return userLogin.getId();
	}

	public Long getUnreadNotificationCount(Long userId) {
		Criteria criteria = this.getCurrentSession().createCriteria(Notification.class);
		criteria.add(Restrictions.eq("userId", userId));
		criteria.add(Restrictions.eq("readStatus", false));
		criteria.setProjection(Projections.rowCount());
		Long unreadNotification = this.getSingleResultOrNull(criteria, Long.class);
		if(unreadNotification == null) {
			unreadNotification = 0L;
		}
		return unreadNotification;
	}

	public List<UserRegisterInternalDto> getUserByUserIds(List<Long> userIds) {
		Criteria criteria = this.getCurrentSession().createCriteria(UserInfo.class);
		criteria.add(Restrictions.in("id", userIds));
		criteria.setProjection(Projections.projectionList().add(Projections.property("id"), "userId")
				.add(Projections.property("email"), "email"));
		criteria.setResultTransformer(new AliasToBeanResultTransformer(UserRegisterInternalDto.class));
		return this.getResultList(criteria, UserRegisterInternalDto.class);
	}
}
