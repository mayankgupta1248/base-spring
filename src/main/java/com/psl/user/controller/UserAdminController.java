package com.psl.user.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.psl.enums.UserRoles;
import com.psl.enums.UserStatus;
import com.psl.helper.BaseController;
import com.psl.helper.model.GenericResponse;
import com.psl.helper.utils.auth.PslSecured;
import com.psl.models.user.AdminLoginDto;
import com.psl.models.user.AdminRegisterDto;
import com.psl.models.user.UserInfoDto;
import com.psl.user.service.UserService;

@RestController
@RequestMapping(value = "/admin")
public class UserAdminController extends BaseController{

	@Autowired
	private UserService userService;
	
	@RequestMapping(value = "/register", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public ResponseEntity<GenericResponse<String>> createAdminUser(
			@Valid @RequestBody final AdminRegisterDto adminRegisterDto) throws Exception {
		return pslResponse(userService.createNewAdmin(adminRegisterDto));
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public ResponseEntity<GenericResponse<UserInfoDto>> adminLogin(
			@Valid @RequestBody final AdminLoginDto adminLoginDto) throws Exception {
		return pslResponse(userService.adminLogin(adminLoginDto));
	}
	
	@PslSecured(roles = {UserRoles.ADMIN})
	@RequestMapping(value = "/change-user-status", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public ResponseEntity<GenericResponse<String>> changeUserStatus(
			@RequestParam(name = "userId") Long userId,
			@RequestParam(name = "userStatus") UserStatus userStatus) throws Exception {
		return pslResponse(userService.changeUserStatus(userId, userStatus));
	}
}
