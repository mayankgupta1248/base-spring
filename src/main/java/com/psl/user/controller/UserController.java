package com.psl.user.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.psl.enums.UserRoles;
import com.psl.helper.BaseController;
import com.psl.helper.model.GenericResponse;
import com.psl.helper.utils.auth.PslSecured;
import com.psl.models.user.ChangePasswordDto;
import com.psl.models.user.ForgotPassDto;
import com.psl.models.user.LoginDto;
import com.psl.models.user.NotificationResponseDto;
import com.psl.models.user.UserInfoDto;
import com.psl.models.user.UserMobileDto;
import com.psl.models.user.UserRegisterDto;
import com.psl.models.user.UserRegisterInternalDto;
import com.psl.user.service.UserService;

@RequestMapping(value = "/user")
@RestController
public class UserController extends BaseController {

	@Autowired
	private UserService userService;

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public ResponseEntity<GenericResponse<UserInfoDto>> register(@Valid @RequestBody final UserRegisterDto userRegisterDto)
			throws Exception {
		return pslResponse(userService.registerNewUserAccount(userRegisterDto));
	}

	@RequestMapping(value = "/login", method = RequestMethod.PUT)
	public ResponseEntity<GenericResponse<UserInfoDto>> login(@Valid @RequestBody final LoginDto loginDto)
			throws Exception {
		return pslResponse(userService.login(loginDto));
	}

	@PslSecured(roles = { UserRoles.USER })
	@RequestMapping(value = "/change-password", method = RequestMethod.PUT)
	public ResponseEntity<GenericResponse<String>> changePassword(
			@Valid @RequestBody ChangePasswordDto changePasswordDto, @RequestParam String otp) throws Exception {
		return pslResponse(userService.changePassword(changePasswordDto, otp));
	}

	@PslSecured(roles = { UserRoles.USER })
	@RequestMapping(method = RequestMethod.PUT, value = "/change-password-otp", consumes = "application/json", produces = "application/json")
	public ResponseEntity<GenericResponse<String>> changePasswordOtp(
			@Valid @RequestBody ChangePasswordDto changePasswordDto) throws Exception {
		return pslResponse(userService.changePasswordOtp(changePasswordDto));
	}

	@RequestMapping(value = "/forgot-password", method = RequestMethod.PUT)
	public ResponseEntity<GenericResponse<String>> forgotPassword(@Valid @RequestBody ForgotPassDto forgotPassDto,
			@RequestParam String otp) throws Exception {
		return pslResponse(userService.forgotPassword(forgotPassDto, otp));
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/forgot-password-otp", consumes = "application/json", produces = "application/json")
	public ResponseEntity<GenericResponse<String>> forgotPasswordOtp(
			@Valid @RequestBody ForgotPassDto forgotPassDto) throws Exception {
		return pslResponse(userService.forgotPasswordOtp(forgotPassDto));
	}

	@PslSecured(roles = { UserRoles.USER })
	@RequestMapping(method = RequestMethod.POST, value = "/send-email-verify-otp", consumes = "application/json", produces = "application/json")
	public ResponseEntity<GenericResponse<String>> sendEmailVerifyOtp(
			@RequestParam(name = "email", required = true) String email) throws Exception {
		return pslResponse(userService.sendEmailVerifyOtp(email));
	}

	// authToken should not be exposed in get url
	@RequestMapping(value = "/access-token", method = RequestMethod.PUT)
	public ResponseEntity<GenericResponse<String>> getAccessToken(
			@RequestParam(name = "authToken", required = true) String authToken) throws Exception {
		return pslResponse(userService.getAccessToken(authToken));
	}

	// SECURED APIS
	@PslSecured(roles = { UserRoles.USER })
	@RequestMapping(method = RequestMethod.POST, value = "/send-mobile-verify-otp", consumes = "application/json", produces = "application/json")
	public ResponseEntity<GenericResponse<String>> sendMobileVerifyOtp(@Valid @RequestBody UserMobileDto userMobileDto)
			throws Exception {
		return pslResponse(userService.sendMobileVerifyOtp(userMobileDto));
	}

	@PslSecured(roles = { UserRoles.USER })
	@RequestMapping(method = RequestMethod.POST, value = "/verify-otp-add-mobile", consumes = "application/json", produces = "application/json")
	public ResponseEntity<GenericResponse<UserInfoDto>> verifyOtpAddMobile(@Valid @RequestBody UserMobileDto userMobileDto,
			@RequestParam(name = "otp", required = true) String otp) throws Exception {
		return pslResponse(userService.verifyOtpAddMobile(userMobileDto, otp));
	}

	@PslSecured(roles = { UserRoles.USER })
	@RequestMapping(method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<GenericResponse<UserInfoDto>> getUser() {
		return pslResponse(userService.getUserDtoByAccessToken());
	}

	@PslSecured(roles = { UserRoles.USER })
	@RequestMapping(value = "/notification", method = RequestMethod.GET)
	public ResponseEntity<GenericResponse<NotificationResponseDto>> getUserNotifications(
			@RequestParam(value = "offset", required = false, defaultValue = "0") int offset,
			@RequestParam(value = "size", required = false, defaultValue = "10") int size) {
		return pslResponse(userService.getUserNotifications(offset, size));
	}

	@PslSecured(roles = { UserRoles.USER })
	@RequestMapping(value = "/notification/read", method = RequestMethod.PUT)
	public ResponseEntity<GenericResponse<String>> markUserNotificationAsRead() {
		return pslResponse(userService.markNotificationAsRead());
	}

	// Get emails by userId, not authenticated, to be used internally
	@RequestMapping(value = "/leaderboard-user-details", method = RequestMethod.GET)
	public ResponseEntity<GenericResponse<List<UserRegisterInternalDto>>> getLeaderboardUserDetails(
			@RequestParam(name = "userIds", required = true) List<Long> userIds) {
		return pslResponse(userService.getLeaderboardUserDetails(userIds));
	}

}
