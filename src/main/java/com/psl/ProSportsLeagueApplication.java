
package com.psl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.core.task.TaskExecutor;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.spring4.SpringTemplateEngine;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.thymeleaf.templateresolver.ITemplateResolver;

import com.psl.helper.utils.Const;

@SpringBootApplication
@EnableAsync
@EnableScheduling
@EnableJms
public class ProSportsLeagueApplication {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(ProSportsLeagueApplication.class, args);
	}

	@Bean
	public ResourceBundleMessageSource emailMessageSource() {
		final ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
		messageSource.setBasename("templates");
		return messageSource;
	}

	@Bean
	public TemplateEngine emailTemplateEngine() {
		final SpringTemplateEngine templateEngine = new SpringTemplateEngine();
		// Resolver for TEXT emails
		templateEngine.addTemplateResolver(textTemplateResolver());
		// Resolver for HTML emails (except the editable one)
		templateEngine.addTemplateResolver(htmlTemplateResolver());
		// Message source, internationalization specific to emails
		templateEngine.setTemplateEngineMessageSource(emailMessageSource());
		return templateEngine;
	}

	private ITemplateResolver textTemplateResolver() {
		final ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
		templateResolver.setOrder(1);
		templateResolver.setPrefix("templates/sms/");
		templateResolver.setSuffix(".txt");
		templateResolver.setTemplateMode("HTML5");
		templateResolver.setCharacterEncoding("UTF-8");
		templateResolver.setCacheable(false);
		return templateResolver;
	}

	private ITemplateResolver htmlTemplateResolver() {
		final ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
		templateResolver.setOrder(2);
		templateResolver.setPrefix("templates/html/");
		templateResolver.setSuffix(".html");
		templateResolver.setTemplateMode("HTML5");
		templateResolver.setCharacterEncoding("UTF-8");
		templateResolver.setCacheable(false);
		return templateResolver;
	}

    @Bean(name=Const.PSL_AFTER_MATCHING_PUBLISHER_EXECUTOR_NAME)
    public TaskExecutor matchingExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setThreadNamePrefix(Const.PSL_AFTER_MATCHING_PUBLISHER_EXECUTOR_NAME + "-");
        executor.setCorePoolSize(Const.PSL_AFTER_MATCHING_EXECUTOR_SIZE);
        executor.setMaxPoolSize(Const.PSL_AFTER_MATCHING_EXECUTOR_SIZE);
        executor.setAwaitTerminationSeconds(Const.EXECUTOR_AWAIT_TERMINATION_TIMEOUT);
        executor.setWaitForTasksToCompleteOnShutdown(true);
        executor.initialize();
        return executor;
    }
    
    @Bean(name=Const.PSL_NOTIFICATION_PUBLISHER_EXECUTOR_NAME)
    public TaskExecutor notificationExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setThreadNamePrefix(Const.PSL_NOTIFICATION_PUBLISHER_EXECUTOR_NAME + "-");
        executor.setCorePoolSize(Const.PSL_NOTIFICATION_POOL_SIZE);
        executor.setMaxPoolSize(Const.PSL_NOTIFICATION_POOL_SIZE);
        executor.setAwaitTerminationSeconds(Const.EXECUTOR_AWAIT_TERMINATION_TIMEOUT);
        executor.setWaitForTasksToCompleteOnShutdown(true);
        executor.initialize();
        return executor;
    }
    
    @Bean(name=Const.PSL_SQS_PUBLISHER_EXECUTOR_NAME)
    public TaskExecutor sqsPublisherExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setThreadNamePrefix(Const.PSL_SQS_PUBLISHER_EXECUTOR_NAME + "-");
        executor.setCorePoolSize(Const.PSL_SQS_PUBLISHER_EXECUTOR_SIZE);
        executor.setMaxPoolSize(Const.PSL_SQS_PUBLISHER_EXECUTOR_SIZE);
        executor.setAwaitTerminationSeconds(Const.EXECUTOR_AWAIT_TERMINATION_TIMEOUT);
        executor.setWaitForTasksToCompleteOnShutdown(true);
        executor.initialize();
        return executor;
    }
    
}
