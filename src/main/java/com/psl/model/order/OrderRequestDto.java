package com.psl.model.order;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.psl.enums.OrderType;
import com.psl.helper.utils.validators.constraint.Enm;

public class OrderRequestDto {
	
	@NotNull(message = "matchId can not be null")
	private Long matchId;

	@NotNull(message = "playerId can not be null")
	private Long playerId;
	
	@NotNull(message = "price can not be null")
	private Long price;
	
	@NotBlank(message = "orderType can not be null")
	@Enm(enm = OrderType.class, message = "invalid orderType")
	private String orderType;
	
	public OrderRequestDto() {
	}

	public Long getMatchId() {
		return matchId;
	}

	public void setMatchId(Long matchId) {
		this.matchId = matchId;
	}

	public Long getPlayerId() {
		return playerId;
	}

	public void setPlayerId(Long playerId) {
		this.playerId = playerId;
	}

	public Long getPrice() {
		return price;
	}

	public void setPrice(Long price) {
		this.price = price;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
}
