package com.psl.match.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.psl.enums.MatchStatus;
import com.psl.enums.UserRoles;
import com.psl.helper.BaseController;
import com.psl.helper.model.GenericResponse;
import com.psl.helper.utils.auth.PslSecured;
import com.psl.match.service.MatchService;
import com.psl.models.match.LeaderboardDto;
import com.psl.models.match.MatchDeatilRequestDto;
import com.psl.models.match.MatchDetailResponseDto;
import com.psl.models.match.MatchRequestDto;
import com.psl.models.match.MatchResponseDto;

@RequestMapping(value = "/match")
@RestController
public class MatchController extends BaseController {

	@Autowired
	private MatchService matchService;

	@RequestMapping(value = "/{matchId}", method = RequestMethod.GET)
	public ResponseEntity<GenericResponse<MatchResponseDto>> getMatchDetails(
			@PathVariable(name = "matchId") Long matchId,
			@RequestParam(value = "getDisabledPlayer", required = false, defaultValue = "false") Boolean getDisabledPlayer) {
		return pslResponse(matchService.getMatchDetails(matchId, getDisabledPlayer));
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ResponseEntity<GenericResponse<List<MatchDetailResponseDto>>> getMatchList(
			@RequestParam(value = "seasonId", required = false) String seasonId,
			@RequestParam(value = "status", required = false) MatchStatus status,
			@RequestParam(value = "offset", required = false, defaultValue = "0") int offset,
			@RequestParam(value = "size", required = false, defaultValue = "10") int size) {
		return pslResponse(matchService.getMatchList(seasonId, status, offset, size));
	}
	
	@PslSecured(roles = UserRoles.USER)
	@RequestMapping(value = "/{matchId}/join", method = RequestMethod.GET)
	public ResponseEntity<GenericResponse<String>> joinMatch(
			@PathVariable(value = "matchId") Long matchId) {
		return pslResponse(matchService.joinMatch(matchId));
	}
	
	@RequestMapping(value = "/{matchId}/leaderboard", method = RequestMethod.GET)
	public ResponseEntity<GenericResponse<List<LeaderboardDto>>> getMatchLeaderboard(
			@PathVariable(value = "matchId") Long matchId) throws Exception {
		return pslResponse(matchService.getMatchLeaderboard(matchId));
	}
	
	//Admin API
	
	@PslSecured(roles = UserRoles.ADMIN)
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<GenericResponse<MatchResponseDto>> createNewMatch(
			@Valid @RequestBody final MatchRequestDto matchRequestDto) {
		return pslResponse(matchService.createNewMatch(matchRequestDto));
	}
	
	@PslSecured(roles = UserRoles.ADMIN)
	@RequestMapping(value = "/{matchId}", method = RequestMethod.PUT)
	public ResponseEntity<GenericResponse<MatchDetailResponseDto>> updateMatchDetail(
			@PathVariable(name = "matchId") Long matchId,
			@Valid @RequestBody final MatchDeatilRequestDto matchDeatilRequestDto) {
		return pslResponse(matchService.updateMatch(matchId, matchDeatilRequestDto));
	}

	@PslSecured(roles = UserRoles.ADMIN)
	@RequestMapping(value = "/{matchId}/update-team", method = RequestMethod.PUT)
	public ResponseEntity<GenericResponse<String>> updateMatchTeam(
			@PathVariable(name = "matchId") Long matchId,
			@Valid @RequestBody final MatchRequestDto matchRequestDto) {
		return pslResponse(matchService.updateTeam(matchId, matchRequestDto));
	}

	@PslSecured(roles = UserRoles.ADMIN)
	@RequestMapping(value = "/{matchId}/update-status", method = RequestMethod.PUT)
	public ResponseEntity<GenericResponse<String>> updateMatchStatus(
			@PathVariable(name = "matchId") Long matchId,
			@RequestParam(name = "status") MatchStatus status) {
		return pslResponse(matchService.updateMatchStatus(matchId, status));
	}
}
