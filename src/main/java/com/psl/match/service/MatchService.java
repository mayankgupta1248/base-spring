package com.psl.match.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.psl.EnvironmentService;
import com.psl.enums.MatchStatus;
import com.psl.enums.WalletTransactionGateway;
import com.psl.enums.WalletTransactionInitiator;
import com.psl.enums.WalletTransactionRemark;
import com.psl.enums.WalletTransactionSource;
import com.psl.enums.WalletTransactionStatus;
import com.psl.enums.WalletTransactionType;
import com.psl.helper.exception.PslForbidden;
import com.psl.helper.exception.PslUnprocessableEntity;
import com.psl.helper.logger.PslLogManager;
import com.psl.helper.logger.PslLogger;
import com.psl.helper.utils.Const;
import com.psl.helper.utils.PslMapper;
import com.psl.helper.utils.PslUtil;
import com.psl.helper.utils.RedisUtility;
import com.psl.helper.utils.request.PslRequest;
import com.psl.helper.utils.request.PslRequestContext;
import com.psl.match.domain.MatchBallData;
import com.psl.match.domain.MatchDetail;
import com.psl.match.domain.MatchTeam;
import com.psl.match.domain.MatchTeamPlayer;
import com.psl.match.domain.Player;
import com.psl.match.repository.MatchRepository;
import com.psl.models.match.LeaderboardDto;
import com.psl.models.match.MatchDeatilRequestDto;
import com.psl.models.match.MatchDetailResponseDto;
import com.psl.models.match.MatchRequestDto;
import com.psl.models.match.MatchResponseDto;
import com.psl.models.match.MatchTeamPlayerDto;
import com.psl.models.match.PlayerRequestDto;
import com.psl.models.match.PlayerResponseDto;
import com.psl.models.match.TeamRequestDto;
import com.psl.models.match.TeamResponseDto;
import com.psl.models.user.AccessTokenDetails;
import com.psl.models.wallet.PointWalletInternalTransactionDto;
import com.psl.order.domain.UserMatchWallet;
import com.psl.order.repository.UserMatchWalletRepository;

@Service
public class MatchService {

	@Autowired
	private MatchRepository matchRepository;

	@Autowired
	private UserMatchWalletRepository userMatchWalletRepository;

	@Autowired
	private EnvironmentService environmentService;

	@Autowired
	private MatchIntermediateService matchIntermediateService;

	@Autowired
	private RedisUtility redisUtility;

	@Autowired
	private PslRequest pslRequest;

	private PslMapper pslMapper = new PslMapper();

	private static final PslLogger logger = PslLogManager.getLogger(MatchService.class);

	@Transactional
	public MatchResponseDto createNewMatch(MatchRequestDto matchRequestDto) {
		MatchDetail matchDetail = this.matchRepository
				.getMatchByExternalMatchId(matchRequestDto.getMatchDetail().getExternalMatchId());
		if (matchDetail != null) {
			throw new PslUnprocessableEntity("Match already created for this matchId ");
		}

		logger.info("Create match request " + PslUtil.objectToJson(matchRequestDto));
		matchDetail = pslMapper.convertModel(matchRequestDto.getMatchDetail(), MatchDetail.class);
		matchRepository.create(matchDetail);

		for (TeamRequestDto teamRequestDto : matchRequestDto.getTeams()) {

			for (PlayerRequestDto playerRequestDto : teamRequestDto.getPlayers()) {
				Player player = this.matchRepository.getPlayerByExternalId(playerRequestDto.getExternalPlayerId());
				if (player == null) {
					// This code will be removed as master player data will be managed. This is only
					// for testing
					player = new Player(playerRequestDto.getExternalPlayerName(),
							playerRequestDto.getExternalPlayerId(), playerRequestDto.getExternalPlayerName());
					this.matchRepository.create(player);
					// throw new PslUnprocessableEntity("Player not found, " +
					// playerRequestDto.getExternalPlayerName());
				}

				MatchTeamPlayer matchTeamPlayer = new MatchTeamPlayer(true, playerRequestDto.getType(),
						matchDetail.getId(), player.getId(), new Long("0"), player.getName(),
						teamRequestDto.getTeamId());
				this.matchRepository.create(matchTeamPlayer);
			}

			MatchTeam matchTeam = new MatchTeam(matchDetail.getId(), teamRequestDto.getTeamId(),
					teamRequestDto.getTeamName());
			matchRepository.create(matchTeam);
		}
		return getMatchResponse(matchDetail, false);
	}

	@Transactional
	public MatchDetailResponseDto updateMatch(Long matchId, MatchDeatilRequestDto matchDetailRequestDto) {
		logger.info("Update match detail request " + matchId + " with details "
				+ PslUtil.objectToJson(matchDetailRequestDto));
		MatchDetail matchDetail = this.matchRepository.getMatchById(matchId);
		if (matchDetail == null) {
			throw new PslUnprocessableEntity("No match found");
		} else {
			matchDetail.setBuyFee(matchDetailRequestDto.getBuyFee());
			matchDetail.setFreeOrders(matchDetailRequestDto.getFreeOrders());
			matchDetail.setFormat(matchDetailRequestDto.getFormat());
			matchDetail.setJoiningFee(matchDetailRequestDto.getJoiningFee());
			matchDetail.setMatchName(matchDetailRequestDto.getMatchName());
			matchDetail.setMaxUsers(matchDetailRequestDto.getMaxUsers());
			matchDetail.setSeasonId(matchDetailRequestDto.getSeasonId());
			matchDetail.setSeasonName(matchDetailRequestDto.getSeasonName());
			matchDetail.setSellFee(matchDetailRequestDto.getSellFee());
			matchDetail.setStartTime(matchDetailRequestDto.getStartTime());
			matchDetail.setVenue(matchDetailRequestDto.getVenue());

			this.matchRepository.update(matchDetail);
		}
		return pslMapper.convertModel(matchDetail, MatchDetailResponseDto.class);
	}

	private MatchResponseDto getMatchResponse(MatchDetail matchDetail, Boolean getDisabledPlayer) {
		List<TeamResponseDto> teamResponseDtoList = new ArrayList<>();
		List<MatchTeam> matchTeams = this.matchRepository.getTeamsInMatch(matchDetail.getId());
		for (MatchTeam matchTeam : matchTeams) {
			List<MatchTeamPlayer> matchTeamPlayers = this.matchRepository.getMatchTeamPlayers(matchDetail.getId(),
					matchTeam.getTeamId(), getDisabledPlayer);
			List<PlayerResponseDto> playerResponseDtoList = new ArrayList<>();
			for (MatchTeamPlayer matchTeamPlayer : matchTeamPlayers) {
				PlayerResponseDto playerResponseDto = new PlayerResponseDto(matchTeamPlayer.getPlayerId(),
						matchTeamPlayer.getPlayerName(), matchTeamPlayer.getType());
				playerResponseDtoList.add(playerResponseDto);
			}
			TeamResponseDto teamResponseDto = new TeamResponseDto(matchTeam.getTeamId(), matchTeam.getTeamName(),
					playerResponseDtoList);
			teamResponseDtoList.add(teamResponseDto);
		}
		MatchDetailResponseDto matchDetailResponseDto = pslMapper.convertModel(matchDetail,
				MatchDetailResponseDto.class);
		return new MatchResponseDto(matchDetailResponseDto, teamResponseDtoList);
	}

	@Transactional(readOnly = true)
	public List<MatchDetailResponseDto> getMatchList(String seasonId, MatchStatus status, int offset, int size) {
		List<MatchDetail> matchList = this.matchRepository.getMatchList(seasonId, status, offset, size);
		return pslMapper.convertModelList(matchList, MatchDetailResponseDto.class);
	}

	@Transactional(readOnly = true)
	public MatchResponseDto getMatchDetails(Long matchId, Boolean getDisabledPlayer) {
		MatchDetail matchDetail = this.matchRepository.getMatchById(matchId);
		if (matchDetail == null) {
			throw new PslUnprocessableEntity("No such match");
		}
		return getMatchResponse(matchDetail, getDisabledPlayer);
	}

	public String updateTeam(Long matchId, MatchRequestDto matchRequestDto) {
		logger.info("Update team for match " + matchId + " with " + PslUtil.objectToJson(matchRequestDto));
		List<MatchTeamPlayerDto> matchTeamPlayerToVoid = matchIntermediateService.updateTeam(matchId, matchRequestDto);

		logger.info("Players to void " + PslUtil.objectToJson(matchTeamPlayerToVoid));
		if (matchTeamPlayerToVoid != null && !matchTeamPlayerToVoid.isEmpty()) {
			// Publish this list to reevert the trades
		}
		return "Success";
	}

	public String updateMatchStatus(Long matchId, MatchStatus status) {
		if (null == status) {
			throw new PslUnprocessableEntity("Status can not be null");
		}
		logger.info("Update status to " + status.name() + " for match " + matchId);

		MatchDetail matchDetail = matchIntermediateService.updateMatchStatus(matchId, status);
		if (MatchStatus.INITIALIZED.name().equals(matchDetail.getStatus())) {
			// Call pricing model to update players price
		}

		if (MatchStatus.COMPLETED.name().equals(matchDetail.getStatus())) {
			// Settle all user balances and finalize the final ranks
		}

		return "Status updated to " + status.name();
	}

	@Transactional
	public String joinMatch(Long matchId) {
		Long userId = this.getAccessTokenDetails().getUserId();
		MatchDetail matchDetail = this.matchRepository.getMatchById(matchId);
		if (!matchDetail.getStatus().equalsIgnoreCase(MatchStatus.STARTED.name())) {
			throw new PslUnprocessableEntity("Match status is " + matchDetail.getStatus());
		}
		UserMatchWallet userMatchWallet = this.userMatchWalletRepository.getUserMatchWallet(userId, matchId);
		if (userMatchWallet == null) {
			userMatchWallet = matchIntermediateService.createUserMatchWallet(userId, matchId,
					matchDetail.getJoiningFee());

			PointWalletInternalTransactionDto pointWalletInternalTransactionDto = new PointWalletInternalTransactionDto(
					userId, userMatchWallet.getUserMatchId(), matchDetail.getJoiningFee(), new Long("0"),
					matchDetail.getJoiningFee(), WalletTransactionGateway.EXTERNAL.name(),
					WalletTransactionInitiator.USER.name(), WalletTransactionRemark.MATCH_JOIN.getRemark(),
					WalletTransactionSource.MATCH_JOIN.name(), WalletTransactionStatus.SUCCESS.name(),
					WalletTransactionType.WITHDRAWAL.name());

			Boolean status = pslRequest.request(environmentService.getWalletInternalTransactionUrl(), HttpMethod.POST,
					pointWalletInternalTransactionDto, Boolean.class);
			logger.info("Response frpm poin wallet " + status);
			matchIntermediateService.processUserMatchWalletJoiningFees(userId, matchId);
		} else {
			throw new PslUnprocessableEntity("Match already joined");
			// Handle the case in which balance is deducted from point wallet but not
			// credited in user match wallet
		}

		return "Success";
	}

	public AccessTokenDetails getAccessTokenDetails() {
		AccessTokenDetails accessTokenDetails = PslRequestContext.getAccessTokenDetails();
		if (null == accessTokenDetails) {
			throw new PslForbidden("Resource is Forbidden");
		} else {
			return accessTokenDetails;
		}
	}

	@Transactional(readOnly = true)
	public List<LeaderboardDto> getMatchLeaderboard(Long matchId) throws Exception {
		MatchBallData matchBallData = this.matchRepository.getMatchBallDataByMatchId(matchId);
		if (matchBallData == null) {
			throw new PslUnprocessableEntity("No ball update found for match");
		}
		String key = Const.REDIS_LEADERBOARD_KEY + matchId + "_" + matchBallData.getBallNumber();
		List<LeaderboardDto> leaderboardDtoList = redisUtility.getValueOrNull(Const.REDIS_NAMESPACE, key, List.class);
		if (leaderboardDtoList == null) {
			leaderboardDtoList = this.matchRepository.getLeaderBoardByMatchId(matchId);
			List<UserMatchWallet> userMatchWalletList = this.userMatchWalletRepository
					.getAllUserMatchWalletByMatchId(matchId);
			Map<Long, Long> userMatchWalletMap = new HashMap<>();
			for (UserMatchWallet userMatchWallet : userMatchWalletList) {
				userMatchWalletMap.put(userMatchWallet.getUserId(), userMatchWallet.getMatchUserBalance());
			}

			for (LeaderboardDto leaderboardDto : leaderboardDtoList) {
				leaderboardDto.setScore(leaderboardDto.getScore() + userMatchWalletMap.get(leaderboardDto.getUserId()));
			}

			redisUtility.setWithExpiry(Const.REDIS_NAMESPACE, key, leaderboardDtoList, 1, TimeUnit.MINUTES);
		}
		return leaderboardDtoList;
	}
}
