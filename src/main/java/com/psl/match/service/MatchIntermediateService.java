package com.psl.match.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.psl.enums.MatchStatus;
import com.psl.enums.WalletTransactionSource;
import com.psl.enums.WalletTransactionStatus;
import com.psl.enums.WalletTransactionType;
import com.psl.helper.exception.PslUnprocessableEntity;
import com.psl.helper.logger.PslLogManager;
import com.psl.helper.logger.PslLogger;
import com.psl.match.domain.MatchDetail;
import com.psl.match.domain.MatchTeamPlayer;
import com.psl.match.domain.Player;
import com.psl.match.repository.MatchRepository;
import com.psl.models.match.MatchRequestDto;
import com.psl.models.match.MatchTeamPlayerDto;
import com.psl.models.match.PlayerRequestDto;
import com.psl.models.match.TeamRequestDto;
import com.psl.order.domain.UserMatchTransaction;
import com.psl.order.domain.UserMatchWallet;
import com.psl.order.repository.UserMatchWalletRepository;

@Service
public class MatchIntermediateService {

	@Autowired
	private MatchRepository matchRepository;

	@Autowired
	private UserMatchWalletRepository userMatchWalletRepository;

	private static final PslLogger logger = PslLogManager.getLogger(MatchIntermediateService.class);

	@Transactional
	public List<MatchTeamPlayerDto> updateTeam(Long matchId, MatchRequestDto matchRequestDto) {
		MatchDetail matchDetail = this.matchRepository.getMatchById(matchId);
		if (matchDetail == null) {
			throw new PslUnprocessableEntity("No such match");
		}

		List<MatchTeamPlayerDto> matchTeamPlayerToVoid = getDisabledPlayers(matchDetail.getId(), matchRequestDto);

		for (TeamRequestDto teamRequestDto : matchRequestDto.getTeams()) {

			for (PlayerRequestDto playerRequestDto : teamRequestDto.getPlayers()) {
				Player player = this.matchRepository.getPlayerByExternalId(playerRequestDto.getExternalPlayerId());
				if (player == null) {
					logger.info("Player not found " + playerRequestDto.getExternalPlayerId());
					// This code will be removed as master player data will be managed. This is only
					// for testing
					player = new Player(playerRequestDto.getExternalPlayerName(),
							playerRequestDto.getExternalPlayerId(), playerRequestDto.getExternalPlayerName());
					this.matchRepository.create(player);
					// throw new PslUnprocessableEntity("Player not found, " +
					// playerRequestDto.getExternalPlayerName());
				}
				MatchTeamPlayer matchTeamPlayer = this.matchRepository.getMatchTeamPlayer(matchId, player.getId());
				if (matchTeamPlayer == null) {
					logger.info("MatchTeamPlayer not found " + player.getId());
					matchTeamPlayer = new MatchTeamPlayer(true, playerRequestDto.getType(), matchDetail.getId(),
							player.getId(), new Long("0"), player.getName(), teamRequestDto.getTeamId());
					this.matchRepository.create(matchTeamPlayer);
				} else {
					matchTeamPlayer.setStatus(true);
					this.matchRepository.update(matchTeamPlayer);
				}
			}
		}

		if (matchTeamPlayerToVoid != null && !matchTeamPlayerToVoid.isEmpty()) {
			List<Long> matchTeamPlayerToDisable = new ArrayList<>();
			for (MatchTeamPlayerDto matchTeamPlayerDto : matchTeamPlayerToVoid) {
				matchTeamPlayerToDisable.add(matchTeamPlayerDto.getId());
			}

			this.matchRepository.disablePlayersOfMatch(matchTeamPlayerToDisable);
		}
		return matchTeamPlayerToVoid;
	}

	private List<MatchTeamPlayerDto> getDisabledPlayers(Long matchId, MatchRequestDto matchRequestDto) {
		Map<Long, MatchTeamPlayer> existingPlayerMap = new HashMap<>();

		for (TeamRequestDto teamRequestDto : matchRequestDto.getTeams()) {
			List<MatchTeamPlayer> dbMatchTeamPlayers = this.matchRepository.getMatchTeamPlayers(matchId,
					teamRequestDto.getTeamId(), false);

			// Map of existing players
			for (MatchTeamPlayer matchTeamPlayer : dbMatchTeamPlayers) {
				existingPlayerMap.put(matchTeamPlayer.getPlayerId(), matchTeamPlayer);
			}

			// Find new player ids
			List<String> externalPlayerIdList = new ArrayList<>();
			for (PlayerRequestDto playerRequestDto : teamRequestDto.getPlayers()) {
				externalPlayerIdList.add(playerRequestDto.getExternalPlayerId());
			}
			List<Player> teamPlayers = this.matchRepository.getPlayerListByExternalId(externalPlayerIdList);

			// Remove players from older map
			for (Player player : teamPlayers) {
				if (existingPlayerMap.containsKey(player.getId())) {
					existingPlayerMap.remove(player.getId());
				}
			}
		}

		List<MatchTeamPlayerDto> matchTeamPlayerDtoList = new ArrayList<>();
		for (MatchTeamPlayer matchTeamPlayer : existingPlayerMap.values()) {
			MatchTeamPlayerDto matchTeamPlayerDto = new MatchTeamPlayerDto(matchTeamPlayer.getId(),
					matchTeamPlayer.getMatchId(), matchTeamPlayer.getPlayerId(), matchTeamPlayer.getPrice(),
					matchTeamPlayer.getPlayerName(), matchTeamPlayer.getTeamId());
			matchTeamPlayerDtoList.add(matchTeamPlayerDto);
		}

		return matchTeamPlayerDtoList;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public UserMatchWallet createUserMatchWallet(Long userId, Long matchId, Long matchUserBalance) {
		String userMatchWalletId = gerateUserMatchWalletId();
		UserMatchWallet userMatchWallet = new UserMatchWallet(new Long("0"), 0, 0, userMatchWalletId, matchId, userId);
		this.userMatchWalletRepository.create(userMatchWallet);
		UserMatchTransaction userMatchTransaction = new UserMatchTransaction(userMatchWalletId, matchUserBalance,
				new Long("0"), matchUserBalance, new Long("0"), matchUserBalance,
				WalletTransactionStatus.PENDING.name(), WalletTransactionSource.MATCH_JOIN.name(),
				WalletTransactionType.DEPOSIT.name(), userMatchWallet.getId());
		this.userMatchWalletRepository.create(userMatchTransaction);

		return userMatchWallet;
	}

	private String gerateUserMatchWalletId() {
		return "UMW-" + UUID.randomUUID().toString();
	}

	@Transactional
	public MatchDetail updateMatchStatus(Long matchId, MatchStatus status) {
		MatchDetail matchDetail = this.matchRepository.getMatchById(matchId);
		if (status.name().equalsIgnoreCase(matchDetail.getStatus())) {
			throw new PslUnprocessableEntity("Match is status already " + status.name());
		}
		validateMatchStatus(matchDetail, status);

		matchDetail.setStatus(status.name());
		this.matchRepository.update(matchDetail);

		return matchDetail;
	}

	private void validateMatchStatus(MatchDetail matchDetail, MatchStatus status) {
		if (MatchStatus.CREATED.name().equalsIgnoreCase(matchDetail.getStatus())
				&& !MatchStatus.INITIALIZED.equals(status)) {
			throw new PslUnprocessableEntity(
					"Match can be " + MatchStatus.INITIALIZED.name() + " after " + MatchStatus.CREATED.name());
		}

		if (MatchStatus.INITIALIZED.name().equalsIgnoreCase(matchDetail.getStatus())
				&& !MatchStatus.STARTED.equals(status)) {
			throw new PslUnprocessableEntity(
					"Match can be " + MatchStatus.STARTED.name() + " after " + MatchStatus.INITIALIZED.name());
		}

		if (MatchStatus.STARTED.name().equalsIgnoreCase(matchDetail.getStatus())
				&& !(MatchStatus.STOPPED.equals(status) || MatchStatus.COMPLETED.equals(status))) {
			throw new PslUnprocessableEntity("Match can be " + MatchStatus.STOPPED.name() + " or  "
					+ MatchStatus.COMPLETED.name() + " after " + MatchStatus.STARTED.name());
		}

		if (MatchStatus.STOPPED.name().equalsIgnoreCase(matchDetail.getStatus())
				&& !(MatchStatus.STARTED.equals(status) || MatchStatus.COMPLETED.equals(status))) {
			throw new PslUnprocessableEntity("Match can be " + MatchStatus.STARTED.name() + " or  "
					+ MatchStatus.COMPLETED.name() + " after " + MatchStatus.STOPPED.name());
		}

		if (MatchStatus.COMPLETED.name().equalsIgnoreCase(matchDetail.getStatus())) {
			throw new PslUnprocessableEntity("Match is already " + MatchStatus.COMPLETED.name());
		}
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void processUserMatchWalletJoiningFees(Long userId, Long matchId) {
		UserMatchWallet userMatchWallet = this.userMatchWalletRepository.getUserMatchWallet(userId, matchId);

		UserMatchTransaction userMatchTransaction = this.userMatchWalletRepository.getUserMatchWalletTransaction(
				userMatchWallet.getId(), userMatchWallet.getUserMatchId(), WalletTransactionType.DEPOSIT.name());
		if (WalletTransactionStatus.PENDING.name().equalsIgnoreCase(userMatchTransaction.getStatus())) {
			userMatchWallet
					.setMatchUserBalance(userMatchWallet.getMatchUserBalance() + userMatchTransaction.getAmount());
			this.userMatchWalletRepository.update(userMatchWallet);

			userMatchTransaction.setStatus(WalletTransactionStatus.SUCCESS.name());
			this.userMatchWalletRepository.update(userMatchTransaction);
		}
	}

}
