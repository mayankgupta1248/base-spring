package com.psl.match.repository;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Repository;

import com.psl.enums.MatchStatus;
import com.psl.enums.OrderStatus;
import com.psl.helper.AbstractBaseRepository;
import com.psl.match.domain.MatchBallData;
import com.psl.match.domain.MatchDetail;
import com.psl.match.domain.MatchTeam;
import com.psl.match.domain.MatchTeamPlayer;
import com.psl.match.domain.Player;
import com.psl.models.match.LeaderboardDto;

@Repository
public class MatchRepository extends AbstractBaseRepository {

	public List<MatchDetail> getMatchList(String seasonId, MatchStatus status, int offset, int size) {
		Criteria criteria = this.getCurrentSession().createCriteria(MatchDetail.class);
		if (status != null) {
			criteria.add(Restrictions.eq("status", status.toString()));
		}
		if (seasonId != null) {
			criteria.add(Restrictions.eq("seasonId", seasonId));
		}

		criteria.setMaxResults(size);
		criteria.setFirstResult(offset);
		return this.getResultList(criteria, MatchDetail.class);
	}

	public Player getPlayerByExternalId(String externalPlayerId) {
		Criteria criteria = this.getCurrentSession().createCriteria(Player.class);
		criteria.add(Restrictions.eq("externalPlayerId", externalPlayerId));
		return this.getSingleResultOrNull(criteria, Player.class);
	}

	public List<MatchTeam> getTeamsInMatch(Long matchId) {
		Criteria criteria = this.getCurrentSession().createCriteria(MatchTeam.class);
		criteria.add(Restrictions.eq("matchId", matchId));
		return this.getResultList(criteria, MatchTeam.class);
	}

	public List<MatchTeamPlayer> getMatchTeamPlayers(Long matchId, String teamId, Boolean getDisabledPlayer) {
		Criteria criteria = this.getCurrentSession().createCriteria(MatchTeamPlayer.class);
		criteria.add(Restrictions.eq("matchId", matchId));
		criteria.add(Restrictions.eq("teamId", teamId));

		if (!getDisabledPlayer) {
			criteria.add(Restrictions.eq("status", true));
		}

		return this.getResultList(criteria, MatchTeamPlayer.class);
	}

	public MatchTeamPlayer getMatchTeamPlayer(Long matchId, Long playerId) {
		Criteria criteria = this.getCurrentSession().createCriteria(MatchTeamPlayer.class);
		criteria.add(Restrictions.eq("matchId", matchId));
		criteria.add(Restrictions.eq("playerId", playerId));
		return this.getSingleResultOrNull(criteria, MatchTeamPlayer.class);
	}

	public MatchDetail getMatchById(Long matchId) {
		Criteria criteria = this.getCurrentSession().createCriteria(MatchDetail.class);
		criteria.add(Restrictions.eq("id", matchId));
		return getSingleResultOrNull(criteria, MatchDetail.class);
	}

	public List<Player> getPlayerListByExternalId(List<String> externalPlayerIdList) {
		Criteria criteria = this.getCurrentSession().createCriteria(Player.class);
		criteria.add(Restrictions.in("externalPlayerId", externalPlayerIdList));
		return this.getResultList(criteria, Player.class);
	}

	public void disableAllPlayersForMatch(Long matchId) {
		String hql = "UPDATE MatchTeamPlayer set status = :status where matchId = :matchId";
		Query query = this.getCurrentSession().createQuery(hql).setBoolean("status", false).setLong("matchId", matchId);
		query.executeUpdate();
	}

	public MatchDetail getMatchByExternalMatchId(String externalMatchId) {
		Criteria criteria = this.getCurrentSession().createCriteria(MatchDetail.class);
		criteria.add(Restrictions.eq("externalMatchId", externalMatchId));
		return this.getSingleResultOrNull(criteria, MatchDetail.class);
	}

	public void disablePlayersOfMatch(List<Long> matchTeamPlayerToDisable) {
		String hql = "UPDATE MatchTeamPlayer set status = :status where id in (:ids)";
		Query query = this.getCurrentSession().createQuery(hql).setBoolean("status", false).setParameterList("ids",
				matchTeamPlayerToDisable);
		query.executeUpdate();
	}

	public List<LeaderboardDto> getLeaderBoardByMatchId(Long matchId) {
		String sql = "select uo.user_id as userId, ui.name as name,"
				+ " ((CASE WHEN uo.order_type = 'BUY' THEN uo.price + (mtp.price - uo.price) ELSE uo.price - (mtp.price - uo.price) END)) as score,"
				+ " from user_order uo join match_team_player mtp on uo.match_id = mtp.match_id and uo.player_id = mtp.player_id "
				+ " join user_info ui on uo.user_id = ui.id where mtp.match_id = :matchId and uo.status = :orderStatus"
				+ " and mtp.status = :matchPlayerStatus group by uo.user_id order by score desc";
		Query query = this.getCurrentSession().createSQLQuery(sql);
		query.setParameter("matchId", matchId);
		query.setParameter("orderStatus", OrderStatus.OPEN.name());
		query.setParameter("matchPlayerStatus", true);
		query.setResultTransformer(Transformers.aliasToBean(LeaderboardDto.class));
		return query.list();
	}

	public MatchBallData getMatchBallDataByMatchId(Long matchId) {
		Criteria criteria = this.getCurrentSession().createCriteria(MatchBallData.class);
		criteria.add(Restrictions.eq("matchId", matchId));
		return this.getSingleResultOrNull(criteria, MatchBallData.class);
	}
}
