package com.psl.match.domain;

import java.io.Serializable;
import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;


/**
 * The persistent class for the player database table.
 * 
 */
@Entity
@Table(name="player")
@NamedQuery(name="Player.findAll", query="SELECT p FROM Player p")
public class Player implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	@Column(name="created_at")
	private Long createdAt;

	private String name;

	@Column(name="external_player_id")
	private String externalPlayerId;
	
	@Column(name="external_player_name")
	private String externalPlayerName;

	@Column(name="updated_at")
	private Long updatedAt;

	public Player() {
	}
	
	public Player(String name, String externalPlayerId, String externalPlayerName) {
		super();
		this.name = name;
		this.externalPlayerId = externalPlayerId;
		this.externalPlayerName = externalPlayerName;
	}

	@PrePersist
	protected void onCreate() {
		this.createdAt = Instant.now().toEpochMilli();
		this.updatedAt = Instant.now().toEpochMilli();
	}

	@PreUpdate
	protected void onUpdate() {
		this.updatedAt = Instant.now().toEpochMilli();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Long createdAt) {
		this.createdAt = createdAt;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getExternalPlayerId() {
		return externalPlayerId;
	}

	public void setExternalPlayerId(String externalPlayerId) {
		this.externalPlayerId = externalPlayerId;
	}

	public String getExternalPlayerName() {
		return externalPlayerName;
	}

	public void setExternalPlayerName(String externalPlayerName) {
		this.externalPlayerName = externalPlayerName;
	}

	public Long getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Long updatedAt) {
		this.updatedAt = updatedAt;
	}
}