package com.psl.match.domain;

import java.io.Serializable;
import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Version;

import com.psl.enums.MatchStatus;


/**
 * The persistent class for the match database table.
 * 
 */
@Entity
@Table(name="match_detail")
@NamedQuery(name="MatchDetail.findAll", query="SELECT m FROM MatchDetail m")
public class MatchDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	@Column(name="buy_fee")
	private Long buyFee;

	@Column(name="free_orders")
	private int freeOrders;

	@Column(name="created_at")
	private Long createdAt;
	
	private String format;

	@Column(name="joining_fee")
	private Long joiningFee;

	@Column(name="external_match_id")
	private String externalMatchId;
	
	@Column(name="match_name")
	private String matchName;

	@Column(name="max_users")
	private Long maxUsers;

	@Column(name="sell_fee")
	private Long sellFee;
	
	@Column(name="season_id")
	private String seasonId;
	
	@Column(name="season_name")
	private String seasonName;

	@Column(name="start_time")
	private Long startTime;

	private String status;

	@Column(name="updated_at")
	private Long updatedAt;
	
	private String venue;

	@Version
	private int version;

	public MatchDetail() {
	}

	@PrePersist
	protected void onCreate() {
		this.status = MatchStatus.CREATED.name();
		this.createdAt = Instant.now().toEpochMilli();
		this.updatedAt = Instant.now().toEpochMilli();
	}

	@PreUpdate
	protected void onUpdate() {
		this.updatedAt = Instant.now().toEpochMilli();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getBuyFee() {
		return buyFee;
	}

	public void setBuyFee(Long buyFee) {
		this.buyFee = buyFee;
	}

	public int getFreeOrders() {
		return freeOrders;
	}

	public void setFreeOrders(int freeOrders) {
		this.freeOrders = freeOrders;
	}

	public Long getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Long createdAt) {
		this.createdAt = createdAt;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public Long getJoiningFee() {
		return joiningFee;
	}

	public void setJoiningFee(Long joiningFee) {
		this.joiningFee = joiningFee;
	}

	public String getExternalMatchId() {
		return externalMatchId;
	}

	public void setExternalMatchId(String externalMatchId) {
		this.externalMatchId = externalMatchId;
	}

	public String getMatchName() {
		return matchName;
	}

	public void setMatchName(String matchName) {
		this.matchName = matchName;
	}

	public Long getMaxUsers() {
		return maxUsers;
	}

	public void setMaxUsers(Long maxUsers) {
		this.maxUsers = maxUsers;
	}

	public Long getSellFee() {
		return sellFee;
	}

	public void setSellFee(Long sellFee) {
		this.sellFee = sellFee;
	}

	public String getSeasonId() {
		return seasonId;
	}

	public void setSeasonId(String seasonId) {
		this.seasonId = seasonId;
	}

	public String getSeasonName() {
		return seasonName;
	}

	public void setSeasonName(String seasonName) {
		this.seasonName = seasonName;
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Long updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getVenue() {
		return venue;
	}

	public void setVenue(String venue) {
		this.venue = venue;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}
}