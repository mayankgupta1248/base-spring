package com.psl.match.domain;

import java.io.Serializable;
import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;


/**
 * The persistent class for the match_team database table.
 * 
 */
@Entity
@Table(name="match_ball_data")
@NamedQuery(name="MatchBallData.findAll", query="SELECT mbd FROM MatchBallData mbd")
public class MatchBallData implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	@Column(name="created_at")
	private Long createdAt;

	@Column(name="match_id")
	private Long matchId;

	@Column(name="ball_id")
	private String ballId;
	
	@Column(name="ball_number")
	private Long ballNumber;

	@Column(name="score_card")
	private String scoreCard;
	
	@Column(name="prediction")
	private String prediction;

	@Column(name="updated_at")
	private Long updatedAt;
	
	@PrePersist
	protected void onCreate() {
		this.createdAt = Instant.now().toEpochMilli();
		this.updatedAt = Instant.now().toEpochMilli();
	}

	@PreUpdate
	protected void onUpdate() {
		this.updatedAt = Instant.now().toEpochMilli();
	}

	public MatchBallData() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Long createdAt) {
		this.createdAt = createdAt;
	}

	public Long getMatchId() {
		return matchId;
	}

	public void setMatchId(Long matchId) {
		this.matchId = matchId;
	}

	public String getBallId() {
		return ballId;
	}

	public void setBallId(String ballId) {
		this.ballId = ballId;
	}

	public Long getBallNumber() {
		return ballNumber;
	}

	public void setBallNumber(Long ballNumber) {
		this.ballNumber = ballNumber;
	}

	public String getScoreCard() {
		return scoreCard;
	}

	public void setScoreCard(String scoreCard) {
		this.scoreCard = scoreCard;
	}

	public String getPrediction() {
		return prediction;
	}

	public void setPrediction(String prediction) {
		this.prediction = prediction;
	}

	public Long getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Long updatedAt) {
		this.updatedAt = updatedAt;
	}
}