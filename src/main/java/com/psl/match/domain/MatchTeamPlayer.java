package com.psl.match.domain;

import java.io.Serializable;
import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

/**
 * The persistent class for the match_team_player database table.
 * 
 */
@Entity
@Table(name = "match_team_player")
@NamedQuery(name = "MatchTeamPlayer.findAll", query = "SELECT m FROM MatchTeamPlayer m")
public class MatchTeamPlayer implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "created_at")
	private Long createdAt;

	private Boolean status;

	private String type;

	@Column(name = "updated_at")
	private Long updatedAt;

	@Column(name = "match_id")
	private Long matchId;

	@Column(name = "player_id")
	private Long playerId;
	
	@Column(name = "price")
	private Long price;
	
	@Column(name = "player_name")
	private String playerName;

	@Column(name = "team_id")
	private String teamId;

	public MatchTeamPlayer() {
	}

	public MatchTeamPlayer(Boolean status, String type, Long matchId, Long playerId, Long price, String playerName,String teamId) {
		super();
		this.status = status;
		this.type = type;
		this.matchId = matchId;
		this.playerId = playerId;
		this.price = price;
		this.playerName = playerName;
		this.teamId = teamId;
	}

	@PrePersist
	protected void onCreate() {
		this.createdAt = Instant.now().toEpochMilli();
		this.updatedAt = Instant.now().toEpochMilli();
	}

	@PreUpdate
	protected void onUpdate() {
		this.updatedAt = Instant.now().toEpochMilli();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Long createdAt) {
		this.createdAt = createdAt;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Long getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Long updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Long getMatchId() {
		return matchId;
	}

	public void setMatchId(Long matchId) {
		this.matchId = matchId;
	}

	public Long getPlayerId() {
		return playerId;
	}

	public void setPlayerId(Long playerId) {
		this.playerId = playerId;
	}

	public String getTeamId() {
		return teamId;
	}

	public void setTeamId(String teamId) {
		this.teamId = teamId;
	}

	public String getPlayerName() {
		return playerName;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}

	public Long getPrice() {
		return price;
	}

	public void setPrice(Long price) {
		this.price = price;
	}
}