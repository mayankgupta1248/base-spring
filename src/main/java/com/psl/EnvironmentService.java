package com.psl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class EnvironmentService {
	//Internal urls
	private static final String SYSTEM_CONFIG_URL = "/system-config";
	private static final String WALLET_INTERNAL_TRANSACTION_URL = "/wallet/internal-transaction";
		
	@Value("${environment}")
	private String environment;

	//Message 91
	@Value("${msg91_authKey}")
	private String msg91AuthKey;
	
	@Value("${msg91_senderId}")
	private String msg91SenderId;
	
	@Value("${msg91_api}")
	private String msg91Api;
	
	//Encryption
    @Value("${accessToken_hmacKey}")
    private String accessTokenHmacKey;

    @Value("${data_encryption_key}")
    private String dataEncryptionKey;
    
    @Value("${secret_encryption_key}")
    private String secretEncryptionKey;
	
    //AWS Keys
    @Value("${aws_accessKey}")
    private String awsAccessKey;
    
    @Value("${aws_secretKey}")
    private String awsSecretKey;
	
    //API Base URL
    @Value("${psl_api_base_url}")
    private String pslApiBaseUrl;
    
   //SQS
    @Value("${new_user_wallet_queue}")
    private String newUserWalletQueue;
    
    @Value("${new_user_referral_queue}")
    private String newUserReferralQueue;
    
	public String getpslApiBaseUrl() {
		return pslApiBaseUrl;
	}

	public String getSystemConfigUrl() {
		return pslApiBaseUrl + SYSTEM_CONFIG_URL;
	}

	public String getWalletInternalTransactionUrl() {
		return pslApiBaseUrl + WALLET_INTERNAL_TRANSACTION_URL;
	}

	public String getAwsAccessKey() {
		return awsAccessKey;
	}

	public String getAwsSecretKey() {
		return awsSecretKey;
	}

	public String getDataEncryptionKey() {
		return dataEncryptionKey;
	}

	public String getAccessTokenHmacKey() {
		return accessTokenHmacKey;
	}

	public String getMsg91AuthKey() {
		return msg91AuthKey;
	}

	public String getMsg91SenderId() {
		return msg91SenderId;
	}

	public String getMsg91Api() {
		return msg91Api;
	}

	public String getEnvironment() {
		return environment;
	}

	public String getSecretEncryptionKey() {
		return secretEncryptionKey;
	}


	public String getPslApiBaseUrl() {
		return pslApiBaseUrl;
	}


	public String getNewUserWalletQueue() {
		return newUserWalletQueue;
	}


	public String getNewUserReferralQueue() {
		return newUserReferralQueue;
	}
}
