package com.psl.systemconfig.repository;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.psl.helper.AbstractBaseRepository;
import com.psl.systemconfig.domain.SystemConfig;

@Repository
public class SystemConfigRepository extends AbstractBaseRepository{

	public SystemConfig getSystemConfig(String key) {
		Criteria criteria = this.getCurrentSession().createCriteria(SystemConfig.class);
		criteria.add(Restrictions.eq("key", key));
		return this.getSingleResultOrNull(criteria, SystemConfig.class);
	}

	public List<SystemConfig> getAllSystemConfig() {
		Criteria criteria = this.getCurrentSession().createCriteria(SystemConfig.class);
		return this.getResultList(criteria, SystemConfig.class);
	}

}
