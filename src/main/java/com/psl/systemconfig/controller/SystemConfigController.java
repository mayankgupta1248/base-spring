package com.psl.systemconfig.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.psl.helper.BaseController;
import com.psl.helper.model.GenericResponse;
import com.psl.model.systemconfig.SystemConfigDto;
import com.psl.systemconfig.service.SystemConfigService;

@RequestMapping(value = "/system-config")
@RestController
public class SystemConfigController extends BaseController{
	
	@Autowired
	private SystemConfigService systemConfigService;

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<GenericResponse<SystemConfigDto>> getSystemConfig(
			@RequestParam(value = "key") String key) {
		return pslResponse(systemConfigService.getSystemConfig(key));
	}
	
	@RequestMapping(value = "list", method = RequestMethod.GET)
	public ResponseEntity<GenericResponse<List<SystemConfigDto>>> getAllSystemConfig() {
		return pslResponse(systemConfigService.getAllSystemConfig());
	}
}
