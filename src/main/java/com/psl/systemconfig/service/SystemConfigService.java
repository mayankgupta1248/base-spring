package com.psl.systemconfig.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.psl.helper.exception.PslUnprocessableEntity;
import com.psl.helper.logger.PslLogManager;
import com.psl.helper.logger.PslLogger;
import com.psl.helper.utils.PslMapper;
import com.psl.model.systemconfig.SystemConfigDto;
import com.psl.systemconfig.domain.SystemConfig;
import com.psl.systemconfig.repository.SystemConfigRepository;

@Service
public class SystemConfigService {

	@Autowired
	private SystemConfigRepository systemConfigRepository;
	
	private PslMapper pslMapper = new PslMapper();

	private static final PslLogger logger = PslLogManager.getLogger(SystemConfigService.class);
	
	public SystemConfigDto getSystemConfig(String key) {
		SystemConfig systemConfig = this.systemConfigRepository.getSystemConfig(key);
		if(systemConfig == null) {
			throw new PslUnprocessableEntity("No such key found");
		}
		return pslMapper.convertModel(systemConfig, SystemConfigDto.class);
	}

	public List<SystemConfigDto> getAllSystemConfig() {
		List<SystemConfig> systemConfigList = this.systemConfigRepository.getAllSystemConfig();
		return pslMapper.convertModelList(systemConfigList, SystemConfig.class);
	}

}
